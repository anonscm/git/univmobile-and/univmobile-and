package org.unpidf.univmobile.util;

import java.util.ArrayList;
import java.util.List;

import org.unpidf.univmobile.PoiDTO;
import org.unpidf.univmobile.R;
import org.unpidf.univmobile.geocampus.Marqueur;
import org.unpidf.univmobile.geocampus.MarqueurDetails;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class UnivmobileDBAdapter {
 
	
  private static final String TAG = "UnivmobileDBAdapter";
	
  private static final String DATABASE_NAME = "univmobile.db";
  public static final String DATABASE_TABLE_CATEGORY = "category";
  public static final String DATABASE_TABLE_POI_TYPES = "poi_types";
  public static final String DATABASE_TABLE_POIS = "pois";
  public static final String DATABASE_TABLE_UNIVERSITY = "university";
  
  private static final int DATABASE_VERSION = 1;
 
  public static final String KEY_ID = "_id";
  public static final String KEY_NAME = "name";
  public static final String KEY_DESCRIPTION = "description";
  public static final String KEY_IS_COMMON = "is_common";
  public static final String KEY_CAT_ID= "category_id";
  public static final String KEY_CREATION_DATE = "creation_date";
  public static final String KEY_IS_CLICKABLE = "is_clickable";
  public static final String KEY_ICON_PATH = "icon_path";
  public static final String KEY_POI_TYPE_ID = "poi_type_id";
  public static final String KEY_PARENT_ID = "parent_id";
  public static final String KEY_TITLE = "title";
  public static final String KEY_SELECTED = "selected";
  public static final String KEY_FLOOR = "floor";
  public static final String KEY_OPEN_HOURS = "opening_hours";
  public static final String KEY_ITINERARY ="itinerary";
  public static final String KEY_ADDRESS = "address";
  public static final String KEY_PHONE = "phone";
  public static final String KEY_FAX = "fax";
  public static final String KEY_EMAIL = "email";
  public static final String KEY_URL = "url";
  public static final String KEY_LAT ="lat";
  public static final String KEY_LNG ="lng";
  public static final String KEY_UNIV_ID = "univ_id";
  
  private SQLiteDatabase db;
  private final Context context;
  private univMobileDBOpenHelper dbHelper;

  public UnivmobileDBAdapter(Context _context) {
    this.context = _context;
    dbHelper = new univMobileDBOpenHelper(context, DATABASE_NAME, 
                                    null, DATABASE_VERSION);
    db = dbHelper.getReadableDatabase();
    
  }
  
  public void close() {
    db.close();
    dbHelper.close();
    
  }
  
  @Override
	protected void finalize() throws Throwable {
	  db.close();
	    dbHelper.close();
		super.finalize();
	}
  
  public void open() throws SQLiteException {  
    try {
      db = dbHelper.getWritableDatabase();
    } catch (SQLiteException ex) {
      db = dbHelper.getReadableDatabase();
    }
  }  
  
  
  public Cursor executeQuery(Uri uri, SQLiteQueryBuilder qb, String[] projection, 
          String selection, 
          String[] selectionArgs, 
          String sort){
	  	
	  
	  	Cursor c = qb.query(dbHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, sort);
	  	
	    return c;
  }
  
  
  public Cursor executeQuery(String sql, String[] selectionArgs){
	  Log.v(TAG,sql);
	  return db.rawQuery(sql, selectionArgs);
  }
  
  
  public void dropDataTableUniv(){
	 db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_UNIVERSITY);
	 db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_UNIVERSITY);
	 Log.v(TAG, "suppression des données dans la table "+DATABASE_TABLE_UNIVERSITY); 
  }
  public void dropDataBase(){
//	  db.execSQL("DELETE FROM " + DATABASE_TABLE_CATEGORY);
//	  db.execSQL("DELETE FROM  " + DATABASE_TABLE_POI_TYPES);
//	  db.execSQL("DELETE FROM  " + DATABASE_TABLE_POIS);
	  
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CATEGORY);
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POI_TYPES);
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POIS);
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_CATEGORY);
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_POI_TYPES);
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_POIS);
	    
		Log.v(TAG, "suppression des données dans les tables : "
				+ DATABASE_TABLE_CATEGORY + " " + DATABASE_TABLE_POI_TYPES
				+ " " + DATABASE_TABLE_POIS); 
  }
  
  
  public void dropDataBaseOnlyPOIS(){
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POIS);
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_POIS);
	    
		Log.v(TAG, "suppression des données dans la table : "
				 + DATABASE_TABLE_POIS); 
  }
  
  public void dropDataBaseExeptPOIS(){
	  
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CATEGORY);
	    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POI_TYPES);
	   
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_CATEGORY);
	    db.execSQL(univMobileDBOpenHelper.DATABASE_CREATE_POI_TYPES);
	    
		Log.v(TAG, "suppression des données dans les tables : "
				+ DATABASE_TABLE_CATEGORY + " " + DATABASE_TABLE_POI_TYPES); 
  }
  
  
	// Insère un nouveau POI
	public long insertPoi(PoiDTO _poi) {
		// Crée une nouvelle ligne à insérer.
		ContentValues newPoiValues = new ContentValues();
		// Assigne des valeurs à chaque ligne.
		newPoiValues.put(KEY_ID, _poi.getId());
		newPoiValues.put(KEY_POI_TYPE_ID, _poi.getPoiTypeId());
		newPoiValues.put(KEY_UNIV_ID, _poi.getUnivId());
		newPoiValues.put(KEY_TITLE, _poi.getTitle());
		newPoiValues.put(KEY_DESCRIPTION, _poi.getDescription());
		newPoiValues.put(KEY_FLOOR, _poi.getFloor());
		newPoiValues.put(KEY_ADDRESS, _poi.getAddress());
		newPoiValues.put(KEY_FAX, _poi.getFax());
		newPoiValues.put(KEY_EMAIL, _poi.getEmail());
		newPoiValues.put(KEY_LAT, _poi.getGeopoint().getLatitudeE6());
		newPoiValues.put(KEY_LNG, _poi.getGeopoint().getLongitudeE6());

		// newPoiValues.put(KEY_CREATION_DATE, _task.getCreated().getTime());
		// Insère la ligne.
//		Log.v(TAG, "poi " + _poi.getId() + " inserted ");
		return db.insert(DATABASE_TABLE_POIS, null, newPoiValues);

	}

	// Supprime un POI en fonction de son index
	public boolean removePoi(String _rowIndex) {
		Log.v(TAG, "poi " + _rowIndex + " deleted ");
		return db.delete(DATABASE_TABLE_POIS, KEY_ID + "=" + _rowIndex, null) > 0;
	}

	public void executeSQL(String sql){
		try{
			db.execSQL(sql);
		} catch (SQLiteException e){
			e.printStackTrace(); 
		} 
	}
	
	// Insère une nouvelle category
	public long insertCategory(String id, String name, String description,
			String is_common) {
		// Crée une nouvelle ligne à insérer.
		ContentValues newCategoryValues = new ContentValues();
		// Assigne des valeurs à chaque ligne.
		newCategoryValues.put(KEY_ID, id);
		newCategoryValues.put(KEY_NAME, name);
		newCategoryValues.put(KEY_DESCRIPTION, description);
		newCategoryValues.put(KEY_IS_COMMON, is_common);

		// newPoiValues.put(KEY_CREATION_DATE, _task.getCreated().getTime());
		// Insère la ligne.
	//	Log.v(TAG, "category " + id + " inserted ");
		return db.insert(DATABASE_TABLE_CATEGORY, null, newCategoryValues);

	}

	// Supprime une category en fonction de son index
	public boolean removeCategory(String _rowIndex) {
	//	Log.v(TAG, "category " + _rowIndex + " deleted ");
		return db.delete(DATABASE_TABLE_CATEGORY, KEY_ID + "=" + _rowIndex,
				null) > 0;
	}
	
	
	
	// Insère un nouveau type de poi
		public long insertPoiType(String id, String name, String description,
				String is_clickable, String category_id, String icon) {
			// Crée une nouvelle ligne à insérer.
			ContentValues newPoiTypeValues = new ContentValues();
			// Assigne des valeurs à chaque ligne.
			newPoiTypeValues.put(KEY_ID, id);
			newPoiTypeValues.put(KEY_NAME, name);
			newPoiTypeValues.put(KEY_DESCRIPTION, description);
			newPoiTypeValues.put(KEY_IS_CLICKABLE, is_clickable);
			newPoiTypeValues.put(KEY_CAT_ID, category_id);
			 String filenameIcon= icon.substring(icon
						.lastIndexOf("/") + 1);
			newPoiTypeValues.put(KEY_ICON_PATH, filenameIcon);
			
			// Insère la ligne.
		//	Log.v(TAG, "poiType " + id + " inserted ");
			return db.insert(DATABASE_TABLE_POI_TYPES, null, newPoiTypeValues);

		}

		// Supprime  un type de poi en fonction de son index
		public boolean removePoiType(String _rowIndex) {
			Log.v(TAG, "poiType " + _rowIndex + " deleted ");
			return db.delete(DATABASE_TABLE_POI_TYPES, KEY_ID + "=" + _rowIndex,
					null) > 0;
		}
		
	
		public String getCategoryName(String id) {
			
			String result = "";
//			SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//			qb.setTables (UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY);  
//			qb.appendWhere(UnivmobileDBAdapter.KEY_ID + "=" + id);
//			String[] columns = new String[] {UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_NAME};
//			String[] projection=null;
//			Cursor c = qb.query(dbHelper.getReadableDatabase(),columns,"",projection,"","","");
//			c.getColumnNames()
			String sql = "SELECT "+ DATABASE_TABLE_CATEGORY+"."+KEY_NAME+" from "+DATABASE_TABLE_CATEGORY+ " where _id="+id;
			//Log.v(TAG,sql);
			
			Cursor c = db.rawQuery(sql, null);
			int firstNameColumn = c.getColumnIndex(KEY_NAME);
			Log.v(TAG,""+firstNameColumn);
			if (c != null) {
				if (c.moveToFirst()) {
					result = c.getString(firstNameColumn);
				}
//				if (c.isFirst()) {
					
//				}
			}
			Log.v(TAG,result);
			c.close();
			return result;
		}
		
		
		public String getUniversitySummary(){
			return getSummary(false);
		}
		
		public String getCrousSummary(){
			return getSummary(true);
		}
	
		public String getSummary(boolean isCrous){
			String result = "";
			String sql;
			if(isCrous){
				sql = "SELECT "+KEY_TITLE+","+KEY_SELECTED+" from "+DATABASE_TABLE_UNIVERSITY + " where "+ KEY_ID+" LIKE '%crous%' order by " + KEY_TITLE;	
			} else {
				sql = "SELECT "+KEY_TITLE+","+KEY_SELECTED+" from "+DATABASE_TABLE_UNIVERSITY + " where "+ KEY_ID+" NOT LIKE '%crous%' order by " + KEY_TITLE;
			}
			 
		//	Log.v(TAG,sql);
			Cursor c = db.rawQuery(sql, null);
		
			int columnSelected = c.getColumnIndex(KEY_SELECTED);
			int columnTitle = c.getColumnIndex(KEY_TITLE);

			boolean substring =false;
			int i= 0;
			if (c != null) {
				 while(c.moveToNext()){
					 if(!(c.getInt(columnSelected)!=1)){
						 i++;
						 result+=c.getString(columnTitle) +", ";
						 substring=true;
					 } 
				 }
			}
			c.close();
			if(substring){
				if(i==1){
					return i +" séléctionné : "+result.substring(0, result.length()-2);
				}else {
					return i +" séléctionnés : "+result.substring(0, result.length()-2);
				}
			}
			else {
				return "0 séléctionné";
			}
		}
	
		public ArrayList<UniversiteDTO> getUniversityList(){
			ArrayList<UniversiteDTO> result = new ArrayList<UniversiteDTO>();
			
			String sql = "SELECT "+KEY_ID +","+ KEY_TITLE+","+KEY_SELECTED+" from "+DATABASE_TABLE_UNIVERSITY + " where "+ KEY_ID+" NOT LIKE '%crous%' order by " + KEY_TITLE; 
		//	Log.v(TAG,sql);
			Cursor c = db.rawQuery(sql, null);
			
			int columnId = c.getColumnIndex(KEY_ID);
			int columnTitle = c.getColumnIndex(KEY_TITLE);
			int columnSelected = c.getColumnIndex(KEY_SELECTED);
			UniversiteDTO univDTO;
			
			if (c != null) {
				 while(c.moveToNext()){
					 univDTO = new UniversiteDTO();
					 univDTO.setId(c.getString(columnId));
					 univDTO.setTitle(c.getString(columnTitle));
					 if(c.getInt(columnSelected)!=1){
						 univDTO.setSelected(false);
					 } else {
						 univDTO.setSelected(true);
					 }
					 result.add(univDTO);
				 }
			}
			c.close();
			
			return result;
		}
		
		public int getNbUnivSelected(){
			
			String sql = "SELECT COUNT(*) as nb  from "+DATABASE_TABLE_UNIVERSITY +" where "+ KEY_SELECTED+" <> 0"; 
		//	Log.v(TAG,sql);
			Cursor c = db.rawQuery(sql, null);
			
			int columnNb = c.getColumnIndex("nb");
			int result = 0;
			if (c != null) {
				 c.moveToFirst();
				 result = c.getInt(columnNb) ;
			}
			c.close();
			
			return result;
		}
		
		
		public void updateUnivPref(String key, boolean value){
			
			int selected = 0;
			if(value){
				selected =1;
			}
			String requete = "UPDATE "+DATABASE_TABLE_UNIVERSITY+ " SET "+KEY_SELECTED+" = "+selected+" where "+ KEY_ID + " LIKE '"+key+"'";
			db.execSQL(requete);
			
		}
		
		public ArrayList<UniversiteDTO> getCrousList(){
			ArrayList<UniversiteDTO> result = new ArrayList<UniversiteDTO>();
			
			String sql = "SELECT "+KEY_ID +","+ KEY_TITLE+","+KEY_SELECTED+" from "+DATABASE_TABLE_UNIVERSITY + " where "+ KEY_ID+" LIKE '%crous%' order by " + KEY_TITLE; 
		//	Log.v(TAG,sql);
			Cursor c = db.rawQuery(sql, null);
			
			int columnId = c.getColumnIndex(KEY_ID);
			int columnTitle = c.getColumnIndex(KEY_TITLE);
			int columnSelected = c.getColumnIndex(KEY_SELECTED);
			UniversiteDTO univDTO;
			
			if (c != null) {
				 while(c.moveToNext()){
					 univDTO = new UniversiteDTO();
					 univDTO.setId(c.getString(columnId));
					 univDTO.setTitle(c.getString(columnTitle));
					 if(c.getInt(columnSelected)!=1){
						 univDTO.setSelected(false);
					 } else {
						 univDTO.setSelected(true);
					 }
					 result.add(univDTO);
				 }
			}
			c.close();
			
			return result;
		}
		
		
		public String getUniversityName(String id){
			String title = "";
			String sql = "SELECT "+ DATABASE_TABLE_UNIVERSITY+"."+KEY_TITLE+" from "+DATABASE_TABLE_UNIVERSITY+ " where _id LIKE '"+id+"'"; 
			//Log.v(TAG,sql);
			
			Cursor c = db.rawQuery(sql, null);
			int firstNameColumn = c.getColumnIndex(KEY_TITLE);
			Log.v(TAG,""+firstNameColumn);
			if (c != null) {
				if (c.moveToFirst()) {
					title = c.getString(firstNameColumn);
				}
//				if (c.isFirst()) {
					
//				}
			}
			Log.v(TAG,title);
			c.close();
			return title;
		}
		
		
	public String getPoiName(String id) {
			
			String result = "";
//			SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
//			qb.setTables (UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY);  
//			qb.appendWhere(UnivmobileDBAdapter.KEY_ID + "=" + id);
//			String[] columns = new String[] {UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_NAME};
//			String[] projection=null;
//			Cursor c = qb.query(dbHelper.getReadableDatabase(),columns,"",projection,"","","");
//			c.getColumnNames()
			String sql = "SELECT "+ DATABASE_TABLE_POIS+"."+KEY_TITLE+" from "+DATABASE_TABLE_POIS+ " where _id="+id;
		//	Log.v(TAG,sql);
			
			Cursor c = db.rawQuery(sql, null);
			int firstNameColumn = c.getColumnIndex(KEY_TITLE);
			Log.v(TAG,""+firstNameColumn);
			if (c != null) {
				if (c.moveToFirst()) {
					result = c.getString(firstNameColumn);
				}
//				if (c.isFirst()) {
					
//				}
			}
			Log.v(TAG,result);
			c.close();
			return result;
		}
		
		
	  public Marqueur getPoi(String id, Context ctx){
		  Marqueur result = null;
		  Bitmap imageMarqueur;
		  String titre;
		  String sousTitre;
		  String address;
		  String address2;
		  String openHours;
		  String itinerary;
		  String universiteTitle;
		  String emplacement;
		  
		  String phone;
		  String fax;
		  String email;
		  String url;
		  double latitude;
		  double longitude;
		  String sql = univMobileDBOpenHelper.SELECT_POI + " where pois._id = "+id;
		/*  String sql = "select "+DATABASE_TABLE_POIS+"."+KEY_TITLE+", " +
					"POI_EXT."+KEY_TITLE+" AS sous_titre, "+
			  		DATABASE_TABLE_POIS+"."+KEY_LAT+", "+
			  		DATABASE_TABLE_POIS+"."+KEY_LNG+", "+
			  		DATABASE_TABLE_POIS+"."+KEY_FLOOR+", "+
			  		DATABASE_TABLE_POIS+"."+KEY_OPEN_HOURS+", "+
			  		DATABASE_TABLE_POIS+"."+KEY_ITINERARY+", "+
			  		DATABASE_TABLE_POIS+"."+KEY_ADDRESS+", "+
			  		"POI_EXT."+KEY_ADDRESS+" AS address2, "+
			  		DATABASE_TABLE_UNIVERSITY+"."+KEY_TITLE+" AS UNIV_TITLE, "+
			  		KEY_ICON_PATH+" from "+ DATABASE_TABLE_POIS +" " + 
				       " inner join " + DATABASE_TABLE_POI_TYPES  +
			  	       " on " + DATABASE_TABLE_POI_TYPES + "." + KEY_ID +
			  	       "=" +  DATABASE_TABLE_POIS + "." + KEY_POI_TYPE_ID +
			  	        " inner join " + DATABASE_TABLE_POIS  + " AS POI_EXT"+
			  	       " on POI_EXT." + KEY_ID +
			  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_PARENT_ID +
			  	       " inner join " + DATABASE_TABLE_UNIVERSITY  + 
			  	       " on "+DATABASE_TABLE_UNIVERSITY+"." + KEY_ID +
			  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_UNIV_ID +
			  	       " where pois._id = "+id;*/
		 // Log.v(TAG,sql);
		  Cursor c = db.rawQuery(sql, null);
		  int columnTitle = c.getColumnIndex(KEY_TITLE);
		  int columnSubTitle = c.getColumnIndex("sous_titre");
		  int columnLat = c.getColumnIndex(KEY_LAT);
		  int columnLng = c.getColumnIndex(KEY_LNG);
		  int columnFloor = c.getColumnIndex(KEY_FLOOR);
		  int columnAddress = c.getColumnIndex(KEY_ADDRESS);
		  int columnAddress2 = c.getColumnIndex("address2"); 
		  int columnOpenHours = c.getColumnIndex(KEY_OPEN_HOURS);
		  int columnItinerary = c.getColumnIndex(KEY_ITINERARY); 
		  int columnUniv = c.getColumnIndex("UNIV_TITLE");
		  int columnIcon = c.getColumnIndex(KEY_ICON_PATH);

		  int columnPhone = c.getColumnIndex(KEY_PHONE);
		  int columnFax = c.getColumnIndex(KEY_FAX);
		  int columnEmail = c.getColumnIndex(KEY_EMAIL);
		  int columnUrl = c.getColumnIndex(KEY_URL);

		  if (c != null) {
				if (c.moveToFirst()) {
					titre = c.getString(columnTitle);
					sousTitre = c.getString(columnSubTitle);
					latitude = c.getDouble(columnLat);
					longitude = c.getDouble(columnLng);
					address = c.getString (columnAddress);
					address2 = c.getString (columnAddress2);
					openHours = c.getString (columnOpenHours);
					itinerary = c.getString (columnItinerary);
					String uri = c.getString(columnIcon);
					universiteTitle = c.getString(columnUniv);
					emplacement = c.getString (columnFloor);
					
					phone = c.getString (columnPhone);
					fax = c.getString (columnFax);
					email = c.getString (columnEmail);
					url = c.getString (columnUrl);
//					Uri imageUri = 
//					MediaStore.Images.Media.getBitmap(context.getContentResolver(), new URI(uri));
					imageMarqueur = BitmapFactory.decodeFile(uri.substring(7));
					if(imageMarqueur==null){
						imageMarqueur =BitmapFactory.decodeResource(ctx.getResources(), R.drawable.marker_default);
					}
					GeoPoint g = new GeoPoint((int)latitude, (int)longitude);
					MarqueurDetails details = new MarqueurDetails();
					details.setTitre(titre);
					details.setUnivTitle(universiteTitle);
					details.setSousTitre(sousTitre);
					details.setAddress(address.trim());
					details.setAddress2(address2.trim());

					details.setOpenHours(openHours.trim());
					details.setItinerary(itinerary.trim());
					
					details.setPhone(phone);
					details.setFax(fax);
					details.setEmail(email);
					details.setUrl(url);
					
					details.setEmplacement(emplacement);
					result = new  Marqueur(imageMarqueur, g, details);
				}

			}
		  c.close();
		  return result;
	  }
	  public List<Marqueur> getListPoiFromCategory(String id, Context ctx){
		  List<Marqueur> result = new ArrayList<Marqueur>();
		  Marqueur marqueur = null;
		  Bitmap imageMarqueur;
		  String titre;
		  String soustitre;
		  String universiteTitle;
		  String emplacement;
		  String address;
		  String address2;
		  String phone;
		  String fax;
		  String email;
		  String url;
		  
		  String openHours;
		  String itinerary;
		  double latitude;
		  double longitude;
		  int minLatitude = (int)(+81 * 1E6);
		  int maxLatitude = (int)(-81 * 1E6);
		  int minLongitude  = (int)(+181 * 1E6);;
		  int maxLongitude  = (int)(-181 * 1E6);;
		  
		  String sql = univMobileDBOpenHelper.SELECT_POI +  " where "+DATABASE_TABLE_CATEGORY+"."+KEY_ID+" = "+id;
		  
		/*   String sql = "select "+DATABASE_TABLE_POIS+"."+KEY_TITLE+", " +
						"POI_EXT."+KEY_TITLE+" AS sous_titre, "+
				  		DATABASE_TABLE_POIS+"."+KEY_LAT+", "+
				  		DATABASE_TABLE_POIS+"."+KEY_LNG+", "+
				  		DATABASE_TABLE_POIS+"."+KEY_FLOOR+", "+
				  		DATABASE_TABLE_POIS+"."+KEY_ADDRESS+", "+
				  		"POI_EXT."+KEY_ADDRESS+" AS address2, "+
				  		DATABASE_TABLE_UNIVERSITY+"."+KEY_TITLE+" AS UNIV_TITLE, "+
				  		KEY_ICON_PATH+
				  	   " from "+ DATABASE_TABLE_POIS +" " +
				  	   " inner join " + DATABASE_TABLE_POI_TYPES  +
			  	       " on " + DATABASE_TABLE_POI_TYPES + "." + KEY_ID +
			  	       "=" +  DATABASE_TABLE_POIS+"."+KEY_POI_TYPE_ID +
			  	       " inner join " + DATABASE_TABLE_CATEGORY  +
			  	       " on " + DATABASE_TABLE_POI_TYPES + "." + KEY_CAT_ID +
			  	       "=" +  DATABASE_TABLE_CATEGORY + "." + KEY_ID +
			    " inner join " + DATABASE_TABLE_POIS  + " AS POI_EXT"+
			  	       " on POI_EXT." + KEY_ID +
			  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_PARENT_ID +
			  	  " inner join " + DATABASE_TABLE_UNIVERSITY  + 
			  	       " on "+DATABASE_TABLE_UNIVERSITY+"." + KEY_ID +
			  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_UNIV_ID +
			  	       " where "+DATABASE_TABLE_CATEGORY+"."+KEY_ID+" = "+id;
			  	*/
		 // Log.v(TAG,sql);
		  Cursor c = db.rawQuery(sql, null);
		  int columnTitle = c.getColumnIndex(KEY_TITLE);
		  int columnSubTitle = c.getColumnIndex("sous_titre");
		  int columnLat = c.getColumnIndex(KEY_LAT);
		  int columnLng = c.getColumnIndex(KEY_LNG);
		  int columnFloor = c.getColumnIndex(KEY_FLOOR);
		  int columnPhone = c.getColumnIndex(KEY_PHONE);
		  int columnFax = c.getColumnIndex(KEY_FAX);
		  int columnEmail = c.getColumnIndex(KEY_EMAIL);
		  int columnUrl = c.getColumnIndex(KEY_URL);
		  int columnAddress = c.getColumnIndex(KEY_ADDRESS);
		  int columnAddress2 = c.getColumnIndex("address2"); 
		  int columnOpenHours = c.getColumnIndex(KEY_OPEN_HOURS);
		  int columnItinerary = c.getColumnIndex(KEY_ITINERARY); 
		  int columnUniv = c.getColumnIndex("UNIV_TITLE");
		  int columnIcon = c.getColumnIndex(KEY_ICON_PATH);
//		  double latmoy = 0;
//		  double lngmoy = 0;
		  if (c != null) {
			  while(c.moveToNext()){
				
				  	titre = c.getString(columnTitle);
					soustitre = c.getString(columnSubTitle);
					latitude = c.getDouble(columnLat);
					longitude = c.getDouble(columnLng);
					universiteTitle = c.getString(columnUniv);
				//	universite = map
					emplacement = c.getString (columnFloor);
					address = c.getString (columnAddress);
					address2 = c.getString (columnAddress2);
					openHours = c.getString (columnOpenHours);
					phone = c.getString (columnPhone);
					fax = c.getString (columnFax);
					email = c.getString (columnEmail);
					url = c.getString (columnUrl);
					itinerary = c.getString (columnItinerary);
					//Log.v(TAG,"  lat : "+(int)latitude + " lng : " +(int)longitude);
//					latmoy+=latitude;
//					lngmoy+=longitude;
					
					// Sets the minimum and maximum latitude so we can span and zoom
                    minLatitude = (minLatitude > latitude) ? (int)latitude : minLatitude;
                    maxLatitude = (maxLatitude < latitude) ? (int)latitude : maxLatitude;               
                    // Sets the minimum and maximum latitude so we can span and zoom
                    minLongitude = (minLongitude > longitude) ? (int)longitude : minLongitude;
                    maxLongitude = (maxLongitude < longitude) ? (int)longitude : maxLongitude;

					
					String uri = c.getString(columnIcon);
//					Uri imageUri = 
//					MediaStore.Images.Media.getBitmap(context.getContentResolver(), new URI(uri));
					imageMarqueur = BitmapFactory.decodeFile(uri.substring(7));
					//Log.v(TAG, imageMarqueur.toString());
					
					if(imageMarqueur==null){
						imageMarqueur =BitmapFactory.decodeResource(ctx.getResources(), R.drawable.marker_default);
					}
					
					GeoPoint g = new GeoPoint((int)latitude, (int)longitude);
					MarqueurDetails details = new MarqueurDetails();
					details.setUnivTitle(universiteTitle);
					details.setSousTitre(soustitre);
					details.setTitre(titre);
					details.setEmplacement(emplacement);
					details.setAddress(address.trim());
					details.setAddress2(address2.trim());
					details.setPhone(phone);
					details.setFax(fax);
					details.setEmail(email);
					details.setUrl(url);
					
					details.setOpenHours(openHours.trim());
					details.setItinerary(itinerary.trim());
					marqueur = new  Marqueur(imageMarqueur,g, details);
					
					result.add(marqueur);			
					
					
				}

			}
		  c.close();
		  
		 // latmoy = (int) ((Double.valueOf(latmoy)/(Double.valueOf(result.size()))));
		  //lngmoy = lngmoy/result.size();
		  //Log.v(TAG,"latmoy : "+latmoy + " lngmoy : " +lngmoy);
		  
		//  GeoPoint g = new GeoPoint((int)latmoy, (int)lngmoy);
		  GeoPoint span = new GeoPoint((maxLatitude - minLatitude), (maxLongitude - minLongitude));
		  marqueur = new  Marqueur(null, "", "", "", span, "");
		  result.add(marqueur);
		  GeoPoint g = new GeoPoint((maxLatitude + minLatitude)/2, (maxLongitude + minLongitude)/2);
		  marqueur = new  Marqueur(null, "", "", "", g, "");
		  result.add(marqueur);
		  
		
		  return result;
	  }
  
  private static class univMobileDBOpenHelper extends SQLiteOpenHelper {

	  public univMobileDBOpenHelper(Context context, String name,
	                          CursorFactory factory, int version) {
	    super(context, name, factory, version);
	  }

	 // private SQLiteDatabase mDatabase;

	  
	  // Crée une nouvelle base.
	  // création de la table category
	  private static final String DATABASE_CREATE_CATEGORY = "create table " + 
	    DATABASE_TABLE_CATEGORY + " (" + KEY_ID + " integer primary key, " +
	    KEY_NAME + " text not null, " + KEY_DESCRIPTION  + " text, "+ KEY_IS_COMMON  + " text, " + KEY_CREATION_DATE + " long);";
	  // creation de la table poitypes 
	  private static final String DATABASE_CREATE_POI_TYPES = "create table " + 
			  DATABASE_TABLE_POI_TYPES + 
			  " (" + KEY_ID + " integer primary key, " +
			  	KEY_CAT_ID + " integer ," +
 			    KEY_NAME + " text not null, " + 
			    KEY_DESCRIPTION  + " text, " + 
 			    KEY_IS_CLICKABLE +  " integer, " + 
			    KEY_ICON_PATH + " text, " +
			    KEY_CREATION_DATE + " long);";
	// creation de la table pois  
	  private static final String DATABASE_CREATE_POIS= "create table " + 
			  DATABASE_TABLE_POIS+ 
			  " (" + KEY_ID + " integer primary key, " +
			  	KEY_POI_TYPE_ID + " integer, " +
				KEY_PARENT_ID + " integer, " +
			  	KEY_UNIV_ID + " text, " +
 			    KEY_TITLE + " text, " + 
			    KEY_DESCRIPTION  + " text, " +
			    KEY_FLOOR  + " text, " +
			    KEY_OPEN_HOURS  + " text, " +
			    KEY_ITINERARY  + " text, " +
			    KEY_ADDRESS  + " text, " +
			    KEY_PHONE +  " text, " +
			    KEY_FAX  + " text, " +
			    KEY_EMAIL +  " text, " +
			    KEY_URL+ " text, " +
			    KEY_LAT + " int, " +
			    KEY_LNG  + " int, " +
			    KEY_CREATION_DATE + " long);";
	  
	  private static final String SELECT_POI= "select "+DATABASE_TABLE_POIS+"."+KEY_TITLE+", " +
				"POI_EXT."+KEY_TITLE+" AS sous_titre, "+
		  		DATABASE_TABLE_POIS+"."+KEY_LAT+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_LNG+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_FLOOR+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_ADDRESS+", "+
		  		"POI_EXT."+KEY_ADDRESS+" AS address2, "+
		  		DATABASE_TABLE_POIS+"."+KEY_OPEN_HOURS+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_ITINERARY+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_PHONE+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_FAX+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_URL+", "+
		  		DATABASE_TABLE_POIS+"."+KEY_EMAIL+", "+
		  		DATABASE_TABLE_UNIVERSITY+"."+KEY_TITLE+" AS UNIV_TITLE, "+
		  		KEY_ICON_PATH+
		  	   " from "+ DATABASE_TABLE_POIS +" " +
		  	   " inner join " + DATABASE_TABLE_POI_TYPES  +
	  	       " on " + DATABASE_TABLE_POI_TYPES + "." + KEY_ID +
	  	       "=" +  DATABASE_TABLE_POIS+"."+KEY_POI_TYPE_ID +
	  	       " inner join " + DATABASE_TABLE_CATEGORY  +
	  	       " on " + DATABASE_TABLE_POI_TYPES + "." + KEY_CAT_ID +
	  	       "=" +  DATABASE_TABLE_CATEGORY + "." + KEY_ID +
	    " inner join " + DATABASE_TABLE_POIS  + " AS POI_EXT"+
	  	       " on POI_EXT." + KEY_ID +
	  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_PARENT_ID +
	  	  " inner join " + DATABASE_TABLE_UNIVERSITY  + 
	  	       " on "+DATABASE_TABLE_UNIVERSITY+"." + KEY_ID +
	  	       "= "+DATABASE_TABLE_POIS+"." +  KEY_UNIV_ID;
	  	       
	  
	  
	  private static final String DATABASE_CREATE_UNIVERSITY= "create table " + 
			  DATABASE_TABLE_UNIVERSITY+ 
			  " ( id_num integer primary key, " +
			    KEY_ID + " text, " +
			    KEY_TITLE + " text," +
			    KEY_SELECTED + " integer default 0);";
	  
	  @Override
	  public void onCreate(SQLiteDatabase _db) {
		//  mDatabase = _db;
		  
	    _db.execSQL(DATABASE_CREATE_CATEGORY);
	    _db.execSQL(DATABASE_CREATE_POI_TYPES);
	    _db.execSQL(DATABASE_CREATE_POIS);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
	    Log.w("TaskDBAdapter", "Mise à jour de la version " + 
	                           _oldVersion + " vers la version " +
	                           _newVersion + ", les anciennes données seront détruites");

	    // Détruit l’ancienne table.
	    _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CATEGORY);
	    _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POI_TYPES);
	    _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POIS);
	    
	    
	    // Crée la nouvelle.
	    onCreate(_db);
	  }
	  
	  
		
	}
}