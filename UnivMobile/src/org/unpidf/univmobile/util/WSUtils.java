package org.unpidf.univmobile.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.unpidf.univmobile.PoiDTO;
import org.unpidf.univmobile.R;
import org.unpidf.univmobile.activity.ChoixUnivActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class WSUtils {

	public static final String PREFS_NAME = "UnivMobilePrefsFile";
	public static final String KEY_UNIV = "university";
	public static final String KEY_REGION_ID = "region";
	public static final String KEY_REGION_URL = "regionurl";
	public static final String KEY_UNIV_JSON = "jsonuniversity";
	public static final String KEY_REGION_JSON = "jsonregion";
	public static final String KEY_NOT_DEFINE = "notdefine";
	public static final String KEY_HOME = "home";
	public static final String KEY_HELP = "help";
	public static final String KEY_ITEMS = "items";
	public static final String KEY_TITLE = "title";
	public static final String KEY_PUSH = "push";
	public static final String KEY_PUSH_URL = "url_callback";
	public static final String KEY_PUSH_TITLE = "title_callback";
	public static final String KEY_LINK = "link";
	public static final String KEY_ICON = "icon";
	public static final String KEY_STYLE = "style";
	public static final String KEY_NAME = "name";
	public static final String KEY_DESCRIPTION = "description";
	public static final String KEY_IS_COMMON = "is_common";
	public static final String KEY_IS_CLICKABLE = "is_clickable";
	public static final String KEY_FLOOR = "floor";
	public static final String KEY_OPEN_HOURS = "opening_hours";
	public static final String KEY_ITINERARY = "itinerary";
	public static final String KEY_ADDRESS = "address";
	public static final String KEY_PHONE = "phone";
	public static final String KEY_FAX = "fax";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_URL = "url";
	public static final String KEY_ID = "id";
	public static final String KEY_LIST_MENU = "listmenu";
	public static final String KEY_ICON_ACTU = "iconActu";
	public static final String KEY_NEWS = "news";
	public static final String KEY_NEWS_SERVICE = "newsService";
	public static final String KEY_NEWS_ITEMS = "newsItems";
	public static final String KEY_POIS = "pois";
	public static final String KEY_POITYPES = "poitypes";
	public static final String KEY_CATEGORY_ID = "category_id";
	public static final String KEY_CATEGORIES = "categories";
	public static final String KEY_POI_TYPE_ID = "poi_type_id";
	public static final String KEY_PARENT_ID = "parent_id";
	public static final String KEY_REGISTRATION_ID = "registration_id";
	private static final String TAG = "Univmobile - WSUtils";
	public static final String FILE_NAME_POIS = "pois.json";
	public static final String FLAG_DOWNLOAD_JSON = "download_json";
	public static final String FLAG_OK = "OK";
	public static final String FLAG_KO = "KO";
	public static final String FLAG_RUNNING = "RUNNING";
	public static final String KEY_CAT_SELECT = "10001";
	public static final String KEY_POI_SELECT = "10002";
	public static final int NB_SOURCES_MAX = 5;
	public static final String KEY_LANG = "fr";
	public static final String KEY_PREFS_SNOOZE = "10003";
	public static final String KEY_PREFS_LAST_DATA_REFRESH = "10004";
	public static final Long TIMEOUT_30_DAYS = 2592000000l;
	//public static final Long TIMEOUT_30_DAYS = 60000l;
	public static final Long TIMEOUT_15_DAYS = 1296000000l;
	public static final String KEY_PREFS_VERSION = "10005";
	public static final int VERSION = 3;
	public static String IPHONE_JSON_UNIV= "/iphone/listUniversities.json"; 
	public static String PUSH_PREF="push_silence";
	public static String KEY_GEOCAMPUS = "geocampus";
	
	//public static final Long TIMEOUT_15_DAYS = 30000l;
	// permet de sauver un fichier sauvegarde pour l'application
	public static void save(String filename, String contenu, Context ctx) {

		FileOutputStream fos = null;
		try {
			fos = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
			fos.write(contenu.getBytes());
		} catch (FileNotFoundException e) {
			Log.v(TAG, "filename=" + filename + " FileNotFound");
			e.printStackTrace();
		} catch (IOException e) {
			Log.v(TAG, "filename=" + filename + " IO");
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				Log.v(TAG, "filename=" + filename);
				e.printStackTrace();
			}
		}
	}

	public static void saveObject(String filename, Object object, Context ctx) {

		FileOutputStream fos = null;
		try {
			fos = ctx.openFileOutput(filename, Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			try {
				// sérialisation : écriture de l'objet dans le flux de sortie
				oos.writeObject(object);
				// on vide le tampon
				oos.flush();
				Log.v(TAG, "Object serialized");

			} finally {
				// fermeture des flux
				try {
					oos.close();
				} finally {
					fos.close();
				}
			}

		} catch (FileNotFoundException e) {
			Log.v(TAG, "filename=" + filename + " FileNotFound");
			e.printStackTrace();
		} catch (IOException e) {
			Log.v(TAG, "filename=" + filename + " IO");
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				Log.v(TAG, "filename=" + filename);
				e.printStackTrace();
			}
		}
	}

	// verifie l'existance d'une sauvegarde
	public static boolean existFile(String filename, Context ctx) {
		String[] list = ctx.fileList();
		for (int i = 0; i < list.length; i++) {
			if (list[i] != null && list[i].equals(filename)) {
				return true;

			}
		}
		return false;
	}

	// verifie l'existance d'une sauvegarde
	public static boolean remove(String filename, Context ctx) {
		String[] list = ctx.fileList();
		Log.v(TAG, "tentative de suppression du fichier " + filename);
		for (int i = 0; i < list.length; i++) {
			if (list[i] != null && list[i].equals(filename)) {
				Log.v(TAG, "suppression du fichier " + filename);
				ctx.deleteFile(filename);
				return true;

			}
		}
		return false;
	}
	
	
	
	
	public static void updateUnivPref(String key, boolean value, Context ctx){
		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);

		adapter.open();
		adapter.updateUnivPref(key, value);
		adapter.close();
		
	}
	
	
	public static int getUnivSelected(Context ctx){
		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);
		adapter.open();
		int i = adapter.getNbUnivSelected();
		adapter.close();
		return i;
	}
	
	public static boolean updateUnivInDB(List<String[]> listUniversite, Context ctx){
		
		
		
		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);

		adapter.open();	
		ArrayList<UniversiteDTO> listTotal = adapter.getUniversityList();
		ArrayList<UniversiteDTO> listCrous = adapter.getCrousList();
		listTotal.addAll(listCrous);
		
		
		HashMap<String, Boolean> keySelected= new HashMap<String, Boolean>();
		for (UniversiteDTO universiteDTO : listTotal) {
			keySelected.put(universiteDTO.getId(), universiteDTO.isSelected());
		}
		adapter.close();
		storeUnivInDb(listUniversite, ctx);
		adapter.open();	
		listTotal = adapter.getUniversityList();
		listCrous = adapter.getCrousList();
		listTotal.addAll(listCrous);
		SharedPreferences pref = ctx.getSharedPreferences(PREFS_NAME,0);
		Editor editor = pref.edit();
		for (UniversiteDTO universiteDTO : listTotal) {
			
			String key = universiteDTO.getId();
			if(keySelected.containsKey(key)){
				adapter.updateUnivPref(key, keySelected.get(key).booleanValue());
				editor.putBoolean(key, keySelected.get(key).booleanValue());
			}
		}
		editor.commit();
		adapter.close();
		return true;
	}
	
	
	public static boolean storeUnivInDb(List<String[]> listUniversite, Context ctx) {
		
		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);

		adapter.open();

		// suppression des anciens enregistrements
		// TODO
		
		adapter.dropDataTableUniv();
		String sql = "INSERT INTO "
				+ UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY + " ("
				+ UnivmobileDBAdapter.KEY_ID + ","
				+ UnivmobileDBAdapter.KEY_TITLE + ")";
		StringBuilder sb = new StringBuilder();
		sb.append(sql);

		for (int i = 0; i < listUniversite.size(); i++) {
			String[] str = listUniversite.get(i);
			sql = " SELECT " + "'" + str[0] + "'," + "'" + str[1].replace("'", "''") + "' ";

			sb.append(sql);

			if (i != listUniversite.size() - 1) {
				sb.append("UNION");
			}
			// insertion en base

		}
		Log.v(TAG,sb.toString());
		adapter.executeSQL(sb.toString());
		
		ArrayList<UniversiteDTO> crousList= adapter.getCrousList();
		ArrayList<UniversiteDTO> univList = adapter.getUniversityList();
		SharedPreferences pref = ctx.getSharedPreferences(PREFS_NAME,0);
		Editor editor = pref.edit();
		for (UniversiteDTO universiteDTO : univList) {
				editor.putBoolean(universiteDTO.getId(), false);			
			
		}
		for (UniversiteDTO universiteDTO : crousList) {
				editor.putBoolean(universiteDTO.getId(), false);
		}
		editor.commit();
		
		adapter.close();
		
		return true;
	}

	public static void storeAllInDbExeptPois(String jsonAndroid,Context ctx){
		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);
		try{
			
			adapter.dropDataBaseExeptPOIS(); 
			JSONObject jsonRoot = new JSONObject(jsonAndroid);
			JSONArray categories = jsonRoot.getJSONArray(KEY_CATEGORIES);
			
			StringBuilder sb = null;
			String sql = null;
			
			// CATEGORIES
			for (int i = 0; i < categories.length(); i++) {
				JSONObject categorie = categories.getJSONObject(i);
				String id = categorie.getString(KEY_ID).replace("'", "''");
				String name = categorie.getString(KEY_NAME).replace("'",
						"''");
				String description = categorie.getString(KEY_DESCRIPTION)
						.replace("'", "''");
				String is_common = categorie.getString(KEY_IS_COMMON)
						.replace("'", "''");
				// insertion en base
				adapter.insertCategory(id, name, description, is_common);

			}
			// POI TYPES
			JSONArray poitypes = jsonRoot.getJSONArray(KEY_POITYPES);

			sb = new StringBuilder();
			sql = "INSERT INTO "
					+ UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES + "("
					+ UnivmobileDBAdapter.KEY_ID + ","
					+ UnivmobileDBAdapter.KEY_NAME + ","
					+ UnivmobileDBAdapter.KEY_DESCRIPTION + ","
					+ UnivmobileDBAdapter.KEY_IS_CLICKABLE + ","
					+ UnivmobileDBAdapter.KEY_CAT_ID + ","
					+ UnivmobileDBAdapter.KEY_ICON_PATH + ")";
			sb.append(sql);
			for (int i = 0; i < poitypes.length(); i++) {
				JSONObject poitype = poitypes.getJSONObject(i);
				String id = poitype.getString(KEY_ID).replace("'", "''");
				String name = poitype.getString(KEY_NAME)
						.replace("'", "''");
				String description = poitype.getString(KEY_DESCRIPTION)
						.replace("'", "''");
				String is_clickable = poitype.getString(KEY_IS_CLICKABLE);
				String category_id = poitype.getString(KEY_CATEGORY_ID)
						.replace("'", "''");
				JSONObject style = poitype.getJSONObject(KEY_STYLE);
				String icon = style.getString(KEY_ICON).replace("'", "''");

				String filenameIcon = icon
						.substring(icon.lastIndexOf("/") + 1);

				downloadImage(icon, filenameIcon, ctx);

				filenameIcon = "file://" + ctx.getFilesDir().getPath()
						+ "/" + filenameIcon;
			
				sql = " SELECT " + "'" + id + "'," + "'" + name + "',"
						+ "'" + description + "'," + "'" + is_clickable
						+ "'," + "'" + category_id + "'," + "'"
						+ filenameIcon + "' ";
				sb.append(sql);

				if (i != poitypes.length() - 1) {
					sb.append("UNION");
				}
				// insertion en base

			}
			adapter.executeSQL(sb.toString());
			Log.v(TAG, sb.toString());
			
			// POIS
		}  catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			 adapter.close();
		}
		
			
	
		
	}
	
	
	
	public static boolean storeInDb(String jsonUniversity, Context ctx, String universityId, boolean init) {

		UnivmobileDBAdapter adapter = new UnivmobileDBAdapter(ctx);
		try {
			
			adapter.open();
			
			// suppression des anciens enregistrements
			if(init){
				adapter.dropDataBase();
			}
			JSONObject jsonRoot = new JSONObject(jsonUniversity);
			//cas aucune donnée geocampus
			JSONArray pois = jsonRoot.getJSONArray(KEY_POIS);
			if(pois.length()==0) {
				return true;
			}
			
			StringBuilder sb = null;
			String sql = null;
			// dans le cas d'un update on ne renseingne pas les categories et les poi_types à nouveau
			if (init) {

				JSONArray categories = jsonRoot.getJSONArray(KEY_CATEGORIES);
				// CATEGORIES
				for (int i = 0; i < categories.length(); i++) {
					JSONObject categorie = categories.getJSONObject(i);
					String id = categorie.getString(KEY_ID).replace("'", "''");
					String name = categorie.getString(KEY_NAME).replace("'",
							"''");
					String description = categorie.getString(KEY_DESCRIPTION)
							.replace("'", "''");
					String is_common = categorie.getString(KEY_IS_COMMON)
							.replace("'", "''");
					// insertion en base
					adapter.insertCategory(id, name, description, is_common);

				}
				// POI TYPES
				JSONArray poitypes = jsonRoot.getJSONArray(KEY_POITYPES);

				sb = new StringBuilder();
				sql = "INSERT INTO "
						+ UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES + "("
						+ UnivmobileDBAdapter.KEY_ID + ","
						+ UnivmobileDBAdapter.KEY_NAME + ","
						+ UnivmobileDBAdapter.KEY_DESCRIPTION + ","
						+ UnivmobileDBAdapter.KEY_IS_CLICKABLE + ","
						+ UnivmobileDBAdapter.KEY_CAT_ID + ","
						+ UnivmobileDBAdapter.KEY_ICON_PATH + ")";
				sb.append(sql);
				for (int i = 0; i < poitypes.length(); i++) {
					JSONObject poitype = poitypes.getJSONObject(i);
					String id = poitype.getString(KEY_ID).replace("'", "''");
					String name = poitype.getString(KEY_NAME)
							.replace("'", "''");
					String description = poitype.getString(KEY_DESCRIPTION)
							.replace("'", "''");
					String is_clickable = poitype.getString(KEY_IS_CLICKABLE);
					String category_id = poitype.getString(KEY_CATEGORY_ID)
							.replace("'", "''");
					JSONObject style = poitype.getJSONObject(KEY_STYLE);
					String icon = style.getString(KEY_ICON).replace("'", "''");
					
				
					String filenameIcon = icon
							.substring(icon.lastIndexOf("/") + 1);

					downloadImage(icon, filenameIcon, ctx);

					filenameIcon = "file://" + ctx.getFilesDir().getPath()
							+ "/" + filenameIcon;
				
					sql = " SELECT " + "'" + id + "'," + "'" + name + "',"
							+ "'" + description + "'," + "'" + is_clickable
							+ "'," + "'" + category_id + "'," + "'"
							+ filenameIcon + "' ";
					sb.append(sql);

					if (i != poitypes.length() - 1) {
						sb.append("UNION");
					}
					// insertion en base

				}
				adapter.executeSQL(sb.toString());
				// POIS
			}
			sb = new StringBuilder();
			sql = "INSERT INTO "+ UnivmobileDBAdapter.DATABASE_TABLE_POIS + 
					  "(" +UnivmobileDBAdapter.KEY_ID + "," +
					  UnivmobileDBAdapter.KEY_POI_TYPE_ID +  "," +
					  UnivmobileDBAdapter.KEY_PARENT_ID +  "," +
					  UnivmobileDBAdapter.KEY_UNIV_ID +  "," +
					  UnivmobileDBAdapter.KEY_TITLE +  "," +
					  UnivmobileDBAdapter.KEY_DESCRIPTION +  "," +
					  UnivmobileDBAdapter.KEY_FLOOR +  "," +
					  UnivmobileDBAdapter.KEY_OPEN_HOURS +  "," +
					  UnivmobileDBAdapter.KEY_ITINERARY +  "," +
					  UnivmobileDBAdapter.KEY_ADDRESS +  "," +
					  UnivmobileDBAdapter.KEY_PHONE +  "," +
					  UnivmobileDBAdapter.KEY_FAX +  "," +
					  UnivmobileDBAdapter.KEY_URL +  "," +
					  UnivmobileDBAdapter.KEY_EMAIL +  "," +
					  UnivmobileDBAdapter.KEY_LAT +  "," +
					  UnivmobileDBAdapter.KEY_LNG +  ")";
			sb.append(sql);
			
			for (int i = 0; i < pois.length(); i++) {
				JSONObject poi = pois.getJSONObject(i);
				PoiDTO poiDTO = new PoiDTO();
				poiDTO.setId(poi.getString(KEY_ID).replace("'", "''"));
				poiDTO.setPoiTypeId(poi.getString(KEY_POI_TYPE_ID).replace("'", "''"));
				poiDTO.setParentId(poi.getString(KEY_PARENT_ID.replace("'", "''")));
				poiDTO.setTitle(poi.getString(KEY_TITLE).replace("'", "''"));
				poiDTO.setDescription(poi.getString(KEY_DESCRIPTION).replace("'", "''"));
				poiDTO.setFloor(poi.getString(KEY_FLOOR).replace("'", "''"));
				poiDTO.setOpeningHours(poi.getString(KEY_OPEN_HOURS).replace("'", "''"));
				poiDTO.setItinerary(poi.getString(KEY_ITINERARY).replace("'", "''"));
				poiDTO.setUrl(poi.getString(KEY_URL).replace("'", "''"));
				poiDTO.setAddress(poi.getString(KEY_ADDRESS).replace("'", "''"));
				poiDTO.setPhone(poi.getString(KEY_PHONE).replace("'", "''"));
				poiDTO.setFax(poi.getString(KEY_FAX).replace("'", "''"));
				poiDTO.setEmail(poi.getString(KEY_EMAIL).replace("'", "''"));
			
				JSONArray coordinates = poi.getJSONArray("coordinates");

				int latInt = 0;
				int lngInt = 0;
				
				if (coordinates != null && !coordinates.isNull(0)) {
					String lat = ((JSONObject) coordinates.get(0))
							.getString("lat");
					String lng = ((JSONObject) coordinates.get(0))
							.getString("lng");
					try {
						Float latFloat = new Float(lat) * 1000000;
						Float lngFloat = new Float(lng) * 1000000;
						latInt = latFloat.intValue();
						lngInt = lngFloat.intValue();
					} catch (NumberFormatException e) {
						Log.v(TAG, "NumberFormatException avec le poi d'id : "
								+ poiDTO.getId() + "  lat :" + lat + "  lng : " + lng);
						e.printStackTrace();
					}
				}
				
				poiDTO.setGeopoint(new GeoPoint(latInt, lngInt));

				sql =  " SELECT " + 
						 "'" + poiDTO.getId() + "'," +
						 "'" + poiDTO.getPoiTypeId() +  "'," +
						 "'" + poiDTO.getParentId() +  "'," +
						 "'" + universityId +  "'," +
						 "'" + poiDTO.getTitle() +  "'," +
						 "'" + poiDTO.getDescription() +  "'," +
						 "'" + poiDTO.getFloor() +  "'," +
						 "'" + poiDTO.getOpeningHours() +  "'," +
						 "'" + poiDTO.getItinerary() +  "'," +
						 "'" + poiDTO.getAddress() +  "'," +
						 "'" + poiDTO.getPhone() +  "'," +
						 "'" + poiDTO.getFax() +  "'," +
						 "'" + poiDTO.getUrl() +  "'," +
						 "'" + poiDTO.getEmail() +  "'," +
						 "'" + poiDTO.getGeopoint().getLatitudeE6() +  "'," +
						 "'" + poiDTO.getGeopoint().getLongitudeE6() +  "' " ;
				sb.append(sql);
				
				if(i!=pois.length()-1){
					sb.append("UNION");
				}
				
			
			}
			adapter.executeSQL(sb.toString());
			Log.v(TAG, universityId + " : ajout des " +pois.length()+" POI en bases OK");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			adapter.close();
		}

		return true;
	}

	// lit le fichier l'enregistre et renvoi le string (contenu)
	public static String readAndSaveJsonInDb(String url, String filename,
			Context ctx, String universityId, boolean deleteOld) {

		StringBuilder sb = readAndSaveJson(url, filename, ctx);
		if(sb ==null){
			return "";
		}
		storeInDb(sb.toString(),ctx, universityId, deleteOld);
		return sb.toString();
	}

	
	public static String readAndSaveAllExeptPoisInDb(String url, String filename,
			Context ctx) {

		//suppression de l'ancien fichier
		remove(filename, ctx);
		StringBuilder sb = readAndSaveJson(url, filename, ctx);
		if(sb ==null){
			return "";
		}
		storeAllInDbExeptPois(sb.toString(), ctx);
		return sb.toString();
	}
	 
	private static StringBuilder readAndSaveJson(String url, String filename,
			Context ctx) {
		Log.v(TAG, "lecture du fichier : filename = " + filename +" url=" + url);

		InputStream inputStream = null;
		BufferedReader reader = null;
		InputStreamReader inputStreamReader = null;
		StringBuilder sb = null;
		if (!existFile(filename, ctx)) {

			try {

				URL m_url = new URL(url);
				HttpURLConnection urlConnection = (HttpURLConnection)m_url.openConnection();
			//	urlConnection.setInstanceFollowRedirects(true);
//				urlConnection.setFollowRedirects(true);
				//urlConnection.
				Date time1 = new Date();
				
				inputStream = openConnectionCheckRedirects(urlConnection);
			//inputStream = urlConnection.getInputStream();
				Log.v(TAG, "time get inputStream  : "
						+ (new Date().getTime() - time1.getTime()) + "ms");
				inputStreamReader = new InputStreamReader(inputStream);
				// ///////
				reader = new BufferedReader(inputStreamReader);
				sb = new StringBuilder();
				String line = null;
				// 13360ms
				time1 = new Date();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
					sb.append("\n");
				}
				Log.v(TAG, "time line Reading : "
						+ (new Date().getTime() - time1.getTime()) + "ms");
				// ///////

				Log.v(TAG, "json=" + sb.toString());
				time1 = new Date();
				save(filename, sb.toString(), ctx);
				Log.v(TAG,
						"time file saving in context : "
								+ (new Date().getTime() - time1.getTime())
								+ "ms");
				time1 = new Date();
				
				
				Log.v(TAG,
						"time store date in db : "
								+ (new Date().getTime() - time1.getTime())
								+ "ms");
				
			
			} catch (IOException ex) {
				Logger.getLogger(WSUtils.class.getName()).log(Level.SEVERE,
						null, ex);
				return null;
			} finally {
				try {
					if (inputStream != null) {
						inputStream.close();
					}
					if (inputStreamReader != null) {
						inputStreamReader.close();
					}
					if (reader != null) {
						reader.close();
					}
				} catch (IOException ex) {
					Logger.getLogger(ChoixUnivActivity.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
		} else {
			sb = new StringBuilder();
			try {
				FileInputStream fis = ctx.openFileInput(filename);

				try {
					byte[] t = new byte[100];
					for (int a = fis.read(t); a != -1; a = fis.read(t)) {
						for (int i = 0; i < a; ++i) {
							sb.append((char) t[i]);
						}
					}

				} finally {
					fis.close();
				}

			} catch (FileNotFoundException e) {
				Logger.getLogger(WSUtils.class.getName()).log(Level.SEVERE,
						null, e);
				return null;
			} catch (IOException e) {
				Logger.getLogger(WSUtils.class.getName()).log(Level.SEVERE,
						null, e);
				return null;
			}
		

		}
		return sb;
	}

	
	private static InputStream openConnectionCheckRedirects(URLConnection c) throws IOException
	{
	   boolean redir;
	   int redirects = 0;
	   InputStream in = null;
	   do
	   {
	      if (c instanceof HttpURLConnection)
	      {
	         ((HttpURLConnection) c).setInstanceFollowRedirects(false);
	      }
	      // We want to open the input stream before getting headers
	      // because getHeaderField() et al swallow IOExceptions.
	      in = c.getInputStream();
	      redir = false;
	      if (c instanceof HttpURLConnection)
	      {
	         HttpURLConnection http = (HttpURLConnection) c;
	         int stat = http.getResponseCode();
	         if (stat >= 300 && stat <= 307 && stat != 306 &&
	            stat != HttpURLConnection.HTTP_NOT_MODIFIED)
	         {
	            URL base = http.getURL();
	            String loc = http.getHeaderField("Location");
	            URL target = null;
	            if (loc != null)
	            {
	               target = new URL(base, loc);
	            }
	            http.disconnect();
	            // Redirection should be allowed only for HTTP and HTTPS
	            // and should be limited to 5 redirections at most.
	            if (target == null || !(target.getProtocol().equals("http")
	               || target.getProtocol().equals("https"))
	               || redirects >= 5)
	            {
	               throw new SecurityException("illegal URL redirect");
	            }
	            redir = true;
	            c = target.openConnection();
	            redirects++;
	         }
	      }
	   }
	   while (redir && redirects<5);
	   return in;
	}
	
	/**
	 * Open a url and read text data, for example html file
	 * 
	 * @param _url
	 *            url
	 * @return String file content
	 */
	public static String getTextFile(String _url) {
		Log.v(TAG, "getTextFile url=" + _url);

		InputStream inputStream = null;
		BufferedReader reader = null;
		InputStreamReader inputStreamReader = null;
		StringBuilder sb = new StringBuilder();
		try {
			URL url = new URL(_url);
			URLConnection urlConnection = url.openConnection();
			inputStream = urlConnection.getInputStream();
			//inputStream= openConnectionCheckRedirects(urlConnection);
			inputStreamReader = new InputStreamReader(inputStream);
			reader = new BufferedReader(inputStreamReader);

			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
			//Log.v(TAG, "getTextFile json="+ sb.toString());
			return sb.toString();
		} catch (IOException ex) {
		
			Log.v(TAG, "Erreur de connexion/écriture dans WSUtils.getTextFile", ex);
			return sb.toString();
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
				if (inputStreamReader != null) {
					inputStreamReader.close();
				}
				if (reader != null) {
					reader.close();
				}
			} catch (IOException ex) {
				Log.v(TAG, "Erreur de fermeture des inputsStream dans WSUtils.getTextFile", ex);
//				Logger.getLogger(ChoixUnivActivity.class.getName()).log(
//						Level.SEVERE, null, ex);
				return sb.toString();
			}
		}
	}

	
	public static String getRegionUrl(String regionJSON) {
		try {
			JSONObject jsonObject = new JSONObject(regionJSON);
			JSONObject region = jsonObject.getJSONObject(KEY_REGION_ID);
			return region.getString(KEY_URL);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return KEY_NOT_DEFINE;
		
	}
	
	
	public static byte[] getDataFromUrl(String _url) throws IOException {

		// StringBuffer b = new StringBuffer();
		byte imageData[];
		// HttpConnection c = null;

		
		HttpURLConnection urlConnection = null;
		InputStream is = null;
		DataInputStream dataInputStream = null;
		ByteArrayOutputStream bStrm = null;
		try {

			URL url = new URL(_url);
			urlConnection = (HttpURLConnection)url.openConnection();
			//is = urlConnection.getInputStream();
			is = openConnectionCheckRedirects(urlConnection);
		//	len = urlConnection.getContentLength();
			//urlConnection
			dataInputStream = new DataInputStream(is);
			bStrm = null;

//			if (len != -1) {
//				imageData = new byte[len];
//
//				// Read the png into an array
//				// iStrm.read(imageData);
//				dataInputStream.readFully(imageData);
//			} else // Length not available...
//			{
				bStrm = new ByteArrayOutputStream();
				int ch;
				while ((ch = dataInputStream.read()) != -1)
					bStrm.write(ch);
				imageData = bStrm.toByteArray();
				bStrm.close();
		//	}

		} finally {
			// Clean up
			if (is != null)
				is.close();
			if (bStrm != null)
				bStrm.close();
			if (dataInputStream != null)
				dataInputStream.close();
		}

		return imageData;
	}

	public static void storeBannerFromJSON(String jsonUniversity,
			String universityId, Context context) {

		// String filename2 = bannerUrl.substring(bannerUrl.lastIndexOf("/")+1);
		// Log.v(TAG, filename2);

		try {

			JSONObject jsonObject = new JSONObject(jsonUniversity);
			JSONObject university = jsonObject.getJSONObject("university");
			String bannerUrl = university.getString("iphoneBannerUrl");
			String filename = universityId + ".gif";
			downloadImage(bannerUrl, filename, context);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static boolean isExistantInInternalStorage(String path,
			Context context) {
		FileInputStream fis = null;
		boolean existe = true;
		try {
			fis = context.openFileInput(path);
		} catch (FileNotFoundException e) {
			existe = false;
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					Log.v(TAG, "failed to close fis : " + path);
				}
			}
		}
		return existe;

	}

	/****
	 * Télécharge l'image et la stock dans le repertoire privé de l'application
	 * 
	 * @param url
	 * @param fileName
	 * @param context
	 */
	public static void downloadImage(String url, String fileName,
			Context context) {
		FileOutputStream fos = null;
	//	Log.v(TAG, "url=" + url + " saved in " +fileName);
		try {
		//	Log.v(TAG, "url=" + url + " saved in " +fileName);
			byte[] fileContent = null;
			try {
				fileContent = getDataFromUrl(url);
			} catch (IOException e) {
				Log.v(TAG, "downloadImage url=" + e.getMessage());
				e.printStackTrace();
				
			}

			if (fileContent != null) {
				fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
				fos.write(fileContent);
			
			} else {
				Log.v(TAG, "url=" + url + " fileContent null");
			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
			Log.w(TAG, e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
			Log.w(TAG, e);
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Log.w(TAG, e);
				
				}
			}

		}

	}

	public static Map<String, Object> chargerElementMenu(String jsonUniversity,
			String universityId, Context context, String urlUniversity) {

		// String filename2 = bannerUrl.substring(bannerUrl.lastIndexOf("/")+1);
		// Log.v(TAG, filename2);

		HashMap<String, Object> map = new HashMap<String, Object>();
		try {

			JSONObject jsonObject = new JSONObject(jsonUniversity);
			JSONObject university = jsonObject.getJSONObject(KEY_UNIV);
			JSONObject home = university.getJSONObject(KEY_HOME);
			// lien vers l'aide
			String help;
			try {
				help = home.getString(KEY_HELP);
				
			} catch (JSONException jsonException) {
				// TODO: handle exception
				help =getRegionUrl(context)+"/help.jsp?iphone=true";
			}
			
			map.put(KEY_HELP, help);
			
			//ajout de l'HELP dans les preferences 
			SharedPreferences settings = context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString(KEY_HELP, help);
			editor.commit();
			JSONArray items = home.getJSONArray(KEY_ITEMS);
			ArrayList<HashMap<String, String>> listMenu = new ArrayList<HashMap<String, String>>();
		
//			ArrayList<HashMap<String, Object>> listNews = new ArrayList<HashMap<String,Object>>();
			
//			// item actualité (cas spécial)
//			int compteur = 0;
//			JSONObject itemActu = (JSONObject) items.get(compteur);
//			
//			while (itemActu.has(KEY_NEWS)){
//				
//			// verification du fait qu'il s'agit du champs actualité
//				HashMap<String, Object> newsmap = new HashMap<String, Object>();
//				// titre actualités
//				String titleActu = itemActu.getString(KEY_TITLE);
//				// icone
//				JSONObject iconActu = itemActu.getJSONObject(KEY_ICON);
//				String urlIconNews = iconActu.getString(KEY_URL);
//				// news
//				JSONObject news = itemActu.getJSONObject(KEY_NEWS);
//				String url_news = news.getString(KEY_NEWS_SERVICE);
//				String filenameIconNews = urlIconNews.substring(urlIconNews
//						.lastIndexOf("/") + 1);
//				newsmap.put(KEY_ICON_ACTU, filenameIconNews);
//				newsmap.put(KEY_NEWS_SERVICE, url_news);
//				newsmap.put(KEY_TITLE, titleActu);
//
//				if (!isExistantInInternalStorage(filenameIconNews, context)) {
//					downloadImage(urlIconNews, filenameIconNews, context);
//				}
//				JSONArray newsItems = news.getJSONArray(KEY_NEWS_ITEMS);
//				for (int i = 0; i < newsItems.length(); i++) {
//
//					HashMap<String, String> menu = new HashMap<String, String>();
//					menu.put(KEY_TITLE, ((JSONObject) newsItems.get(i))
//							.getString(KEY_TITLE));
//					menu.put(KEY_LINK,
//							((JSONObject) newsItems.get(i)).getString(KEY_URL));
//					listMenu.add(menu);
//				}
//				newsmap.put(KEY_TITLE, titleActu);
//				listNews.add(newsmap);
//				compteur++;
//				itemActu = (JSONObject) items.get(compteur);
//			}
//			if(!listNews.isEmpty()) {
//				map.put(KEY_NEWS,listNews);	
//			}
//			
			
			for (int i = 0; i < items.length(); i++) {

				HashMap<String, String> menu = new HashMap<String, String>();

				JSONObject item = (JSONObject) items.get(i);

				String title = item.getString(KEY_TITLE);
			
				if (title!=null && !title.contains("Podcast")) {

					menu.put(KEY_TITLE, title);
					JSONObject iconActu = item.getJSONObject(KEY_ICON);
					String urlIcon = iconActu.getString(KEY_URL);

					String filenameIcon = urlIcon.substring(urlIcon
							.lastIndexOf("/") + 1);

					if (!isExistantInInternalStorage(filenameIcon, context)) {
						downloadImage(urlIcon, filenameIcon, context);
					}
					
				  menu.put(KEY_URL, filenameIcon);
				  if (!item.has(KEY_GEOCAMPUS)) {
						try{
							String link = item.getString(KEY_LINK);
							menu.put(KEY_LINK, link);
						} catch (JSONException jsonException) {
							Log.v(TAG,title +" "+ jsonException.getMessage());
						}
					}
				  if(item.has(KEY_NEWS)){
					  JSONObject news = item.getJSONObject(KEY_NEWS);
					  String url_news = news.getString(KEY_NEWS_SERVICE);
					  menu.put(KEY_LINK, url_news);
					  
				  }
				  listMenu.add(menu);
				  
				  if(item.has(KEY_NEWS)){
						JSONObject news = item.getJSONObject(KEY_NEWS);
						JSONArray newsItems = news.getJSONArray(KEY_NEWS_ITEMS);
						for (int j = 0; j < newsItems.length(); j++) {

							HashMap<String, String> menunews = new HashMap<String, String>();
							menunews.put(KEY_TITLE, ((JSONObject) newsItems.get(j))
									.getString(KEY_TITLE));
							menunews.put(KEY_LINK,
									((JSONObject) newsItems.get(j)).getString(KEY_URL));
							listMenu.add(menunews);
						}
					}
					
					
				}

			}
			map.put(KEY_LIST_MENU, listMenu);
			

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v(TAG, jsonUniversity);
		}
		return map;
	}

//	public static JSONObject getJSONObjectPOIS(String urlRegion,
//			String universityId, String lang, Context ctx) {
//		JSONObject result = null;
//		Long time1 = new Date().getTime();
//		String url = constructUrl(urlRegion, universityId, lang);
//
//		String jsonPOIS = WSUtils.readAndSaveJson(url, FILE_NAME_POIS, ctx, universityId, true);
//		Long time2 = new Date().getTime();
//		try {
//			result = new JSONObject(jsonPOIS);
//		} catch (JSONException e) {
//			e.printStackTrace();
//			Log.v(TAG, jsonPOIS);
//		}
//		Long time3 = new Date().getTime();
//
//		Log.v(TAG, "Téléchargement pois : " + (time2 - time1) + " ms");
//		Log.v(TAG, "creation JSONObject : " + (time3 - time2) + " ms");
//
//		return result;
//	}

	/**
	 * @param urlRegion
	 * @param universityId
	 * @param lang
	 * @return
	 */
	public static String constructUrl(String urlRegion, String universityId,
			String lang) {
		String url = urlRegion + "/ws/" + lang + "/universities/"
				+ universityId + "/" + FILE_NAME_POIS;
		return url;
	}

	/**
	 * Création d'une map pour lier poi_type_id et category_id
	 * 
	 * @param jsonPOIS
	 * @return Map<poiType,category_id>
	 */
	public static Map<String, String> getMapPoiTypeCategory(JSONObject jsonPOIS) {
		Map<String, String> result = new HashMap<String, String>();
		try {
			JSONArray poistype = jsonPOIS.getJSONArray(KEY_POITYPES);
			for (int i = 0; i < poistype.length(); i++) {

				JSONObject jsonTmp = poistype.getJSONObject(i);
				String poi_type_id = jsonTmp.getString("id");
				String category_id = jsonTmp.getString("category_id");
				result.put(poi_type_id, category_id);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.v(TAG, jsonPOIS.toString());
		}

		return result;
	}

	/**
	 * Création d'une Map avec la structure suivante :
	 * 
	 * <Map> * <string poi_type_id, List<GeoPoint>>
	 * 
	 * @param jsonUniversity
	 * @param universityId
	 * @param context
	 * @param urlUniversity
	 * @return
	 */
	public static Map<String, List<PoiDTO>> chargerListePois(
			JSONObject jsonPOIS, Context context) {

		Map<String, List<PoiDTO>> result = new HashMap<String, List<PoiDTO>>();
		PoiDTO poiTmp;
		Date time1 = new Date();

		Map<String, String> mapTypeCat = getMapPoiTypeCategory(jsonPOIS);

		try {
			JSONArray pois = jsonPOIS.getJSONArray(KEY_POIS);
			for (int i = 0; i < pois.length(); i++) {

				poiTmp = new PoiDTO();

				JSONObject jsonTmp = pois.getJSONObject(i);
				// TODO faire des key
				String poiTypeID = jsonTmp.getString("poi_type_id");
				poiTmp.setPoiTypeId(poiTypeID);
				String id = jsonTmp.getString("id");
				poiTmp.setId(id);
				String title = jsonTmp.getString("title");
				poiTmp.setTitle(title);
				if (!"".equals(poiTypeID)) {
					JSONObject style = jsonTmp.getJSONObject("style");
					String icon = style.getString("icon");
					poiTmp.setUrlIcon(icon);
				}

				JSONArray coordinates = jsonTmp.getJSONArray("coordinates");

				int latInt = 0;
				int lngInt = 0;
				if (coordinates != null && coordinates.get(0) != null) {
					String lat = ((JSONObject) coordinates.get(0))
							.getString("lat");
					String lng = ((JSONObject) coordinates.get(0))
							.getString("lng");
					try {
						Float latFloat = new Float(lat) * 1000000;
						Float lngFloat = new Float(lng) * 1000000;
						latInt = latFloat.intValue();
						lngInt = lngFloat.intValue();
					} catch (NumberFormatException e) {
						Log.v(TAG, "NumberFormatException avec le poi d'id : "
								+ id + "  lat :" + lat + "  lng : " + lng);
						e.printStackTrace();
					}
				}
				// Log.v(TAG, title + "  ->  lat:" +latInt+ "  lng:"+ lngInt);

				GeoPoint geoTmp = new GeoPoint(latInt, lngInt);
				poiTmp.setGeopoint(geoTmp);

				String categoryId = mapTypeCat.get(poiTypeID);
				if (categoryId != null) {
					if (result.containsKey(categoryId)) {
						result.get(categoryId).add(poiTmp);
					} else {
						Log.v(TAG, "add category " + categoryId);
						List<PoiDTO> listPoi = new ArrayList<PoiDTO>();
						listPoi.add(poiTmp);
						result.put(categoryId, listPoi);
					}
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
			Log.v(TAG, jsonPOIS.toString());
		}

		Log.v(TAG, "Durée du traitement des pois : "
				+ (new Date().getTime() - time1.getTime()) + "ms");
		return result;
	}

	
	public static String getRegionUrl(Context ctx){
		SharedPreferences settings = ctx.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		
		String urlRegion = settings.getString(WSUtils.KEY_REGION_URL,
				WSUtils.KEY_NOT_DEFINE);
		int pointInt = urlRegion.indexOf("?");
		if (pointInt != -1) {
			urlRegion = urlRegion.substring(0, pointInt);
		}
		return urlRegion;
	}
	
	public static boolean chargerUniversite(Context context){
		SharedPreferences settings = context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		// recuperation de la liste des universités
    	String universitejson = settings.getString(WSUtils.KEY_UNIV_JSON, WSUtils.KEY_NOT_DEFINE);
    	boolean isNetworkOK= true;
    	if(WSUtils.KEY_NOT_DEFINE.equals(universitejson)){
    		String region = settings.getString(WSUtils.KEY_REGION_URL, WSUtils.KEY_NOT_DEFINE);
    		if(region.equals(WSUtils.KEY_NOT_DEFINE)){
    			isNetworkOK= false;
    		}
    		
    		if(region.contains("?")){
    			int index = region.indexOf("?");
    			Log.v("SplashScreen", "universiteJson = " + region.substring(0, index) + context.getText(R.string.url_univ_liste).toString() + region.substring(index, region.length()));
    			universitejson = WSUtils.getTextFile(region.substring(0, index) + context.getText(R.string.url_univ_liste).toString() + region.substring(index, region.length()));	
    		}  else if(WSUtils.KEY_NOT_DEFINE.equals(region)){
    			universitejson = WSUtils.getTextFile(context.getText(R.string.server_region).toString() + context.getText(R.string.url_univ_liste).toString());	
    		} else {
    			universitejson = WSUtils.getTextFile(region + context.getText(R.string.url_univ_liste).toString());
    		}
    		if("".equals(universitejson)){
    			Log.v(TAG,"erreur reseau");
    			isNetworkOK= false;
    			
    		} else {
    			
    			editor.putString(WSUtils.KEY_UNIV_JSON, universitejson);
    			editor.commit();
    		}
    	}
		return isNetworkOK;
	}
	
	
	public static void chargerRegions(Context mContext){
		SharedPreferences settings = mContext.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		String json = WSUtils.getTextFile(mContext.getText(R.string.server_region).toString() + mContext.getText(R.string.url_region).toString());
        editor.putString(WSUtils.KEY_REGION_JSON, json);
        editor.commit();
         
	}
	
	public static String getJsonUniv(SharedPreferences settings) {
		String retour="";
		
        	  String urlUniversite = settings.getString(WSUtils.KEY_REGION_URL, WSUtils.KEY_NOT_DEFINE);
        	  if(WSUtils.KEY_NOT_DEFINE.equals(urlUniversite)){
        		  //TODO GERER
        	  } else {
        		  
        		  
        	
        		if(urlUniversite.contains("?")){
          			int index = urlUniversite.indexOf("?");
          			retour = WSUtils.getTextFile(urlUniversite.substring(0, index) + IPHONE_JSON_UNIV + urlUniversite.substring(index, urlUniversite.length()));
          			Log.v(TAG,"text file = "+urlUniversite.substring(0, index) + IPHONE_JSON_UNIV+ urlUniversite.substring(index, urlUniversite.length()));
          		} else {
          			Log.v(TAG,"urlUniversite="+urlUniversite);
          			retour =  WSUtils.getTextFile(urlUniversite + IPHONE_JSON_UNIV);
          		}
        	  }
        	  
        	  
  	    
         return retour;
	}
	
	public static void register(Context context) {
		//SharedPreferences settings = context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		//String registrationId = settings.getString(WSUtils.KEY_REGISTRATION_ID, WSUtils.KEY_NOT_DEFINE);
		//if(registrationId.equals(WSUtils.KEY_NOT_DEFINE)){
			Log.v(TAG,"inscription sur le serveur c2dm");
			// inscription sur le serveur c2dm
			
			

//			Intent unregIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
//			unregIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
//			context.startService(unregIntent);
//			
			Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
			registrationIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0)); // boilerplate
		//	registrationIntent.putExtra("sender", "cyrus.rezvani@unpidf.fr");
			registrationIntent.putExtra("sender", context.getText(R.string.senderId));
			
			
			if(context.startService(registrationIntent)==null){
				Log.v(TAG," C2DMBroadcastReceiver startService Failed");
			}
		//	context.startService(registrationIntent);
			
			
			//registrationId = settings.getString(WSUtils.KEY_REGISTRATION_ID, WSUtils.KEY_NOT_DEFINE);
			
			
//		} else {
//			Log.v(TAG,"déja inscrit sur c2dm : registrationId :"+registrationId);
//		}
	}
	
}
