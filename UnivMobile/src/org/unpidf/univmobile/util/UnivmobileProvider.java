package org.unpidf.univmobile.util;


import org.unpidf.univmobile.R;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;

public class UnivmobileProvider extends ContentProvider {
	//private static final String TAG = "Univmobile - UnivmobileProvider";
	 public static String AUTHORITY = "org.unpidf.univmobile.provider";
	 public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/geocampus");
	 public static final Uri CONTENT_URI_SEARCH = Uri.parse("content://" + AUTHORITY + "/geocampusall");
	 public static final Uri CONTENT_URI_CAT = Uri.parse("content://" + AUTHORITY + "/geocampuscat");
	 private static final int SEARCH = 0;
	 private static final int GETITEM = 1;
	 private static final int SEARCH_SUGGEST =2;
	 private static final int CAT = 3;
	 public static final String RESOURCE_CONTEXT  = "android.resource://org.unpidf.univmobile/";
	 
	 
	 public static final String KEY_CATEGORY = SearchManager.SUGGEST_COLUMN_TEXT_1;
	 public static final String KEY_DESCRIPTION = SearchManager.SUGGEST_COLUMN_TEXT_2;
	 
	 UnivmobileDBAdapter mAdapter; 
	  private static final UriMatcher uriMatcher;
	  static {
		   uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		   uriMatcher.addURI(AUTHORITY, "geocampus", SEARCH_SUGGEST);
		   uriMatcher.addURI(AUTHORITY, "geocampusall", SEARCH);
		   uriMatcher.addURI(AUTHORITY, "geocampuscat", CAT);
		   uriMatcher.addURI(AUTHORITY, "geocampus/#", GETITEM);
		   uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH_SUGGEST); 
		   uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH_SUGGEST); 
		   uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT, SEARCH_SUGGEST); 
		   uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT + "/*", SEARCH_SUGGEST); 
		  }
	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean onCreate() {
	    Context context = getContext();
	    mAdapter = new UnivmobileDBAdapter(context);
	    return true;
	}

	
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {

		
		

		  //  qb.setTables(UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY);
		
		Cursor c = null;

		    // S'il s'agit d'une requ�te sur une ligne, on limite le r�sultat.  
		    switch (uriMatcher.match(uri)) {
		    	case SEARCH_SUGGEST : 
		    		 
		    		
		    		if(uri.getPathSegments().size()>1 ){
		    			c = getSearchAll(uri.getPathSegments().get(1), uri, projection, selection, selectionArgs, sortOrder);

		    		} else {
		    			c = getSearchAll("", uri, projection, selection, selectionArgs, sortOrder);
		    		} 
		    		
		    		  c.setNotificationUri(getContext().getContentResolver(), uri);
		    	break;
		    		
		    	case SEARCH : 
		    			
		    		c = getSearchAll(selectionArgs[0], uri, projection, selection, null , sortOrder);
		    	break;
		    	case CAT : 
	    			
		    		c = getSearchCat(selectionArgs[0]);
		    	
		    	break;
		    }
		    // Applique la requ�te � la base.
		    //mAdapter.open();
		    
		   
		    // Enregistre le ContextResolver pour qu'il soit averti
		    // si le r�sultat change.
		  
		    
		    return c;
		    
	}

	

	 private Cursor getSearchAll(String query, Uri uri, String[] columns, String selection, String[] selectionArgs, String sortOrder) {

		 
		 String sql = "select distinct "+UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY+"._id as "+ BaseColumns._ID+ " , " +
		 			  ""+UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY+"." + UnivmobileDBAdapter.KEY_NAME + " as "+SearchManager.SUGGEST_COLUMN_TEXT_1+", " +
		 			  ""+UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY+"." + UnivmobileDBAdapter.KEY_DESCRIPTION + " as "+SearchManager.SUGGEST_COLUMN_TEXT_2+" , " +
		 			 UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_ID + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID+" , " +
		 			  "'category' as type,  " +
		 			 "'"+RESOURCE_CONTEXT+R.drawable.ic_action_category+"' as "+SearchManager.SUGGEST_COLUMN_ICON_1+" " +
		 			  "from "+
		 			  UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY+" "+ 
		 			 " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES  
				  	  + " on " + UnivmobileDBAdapter.KEY_CAT_ID + "=" 
				  	 + UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_ID 
				  	 + " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_POIS  
				  	 + " on " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES + "." + UnivmobileDBAdapter.KEY_ID
				  	 + "=" +  UnivmobileDBAdapter.KEY_POI_TYPE_ID +
				 	  " where (lower("+UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." +UnivmobileDBAdapter.KEY_DESCRIPTION + ") LIKE '%" +
				 	  query + "%' OR " + 
			           "lower("+UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_NAME+ ") LIKE '%" +
			           query + "%') and category._id <> 1 "+
			           "UNION " + 
			           " select distinct '_'||"+ UnivmobileDBAdapter.DATABASE_TABLE_POIS +"._id as _id, "+ 
			           UnivmobileDBAdapter.DATABASE_TABLE_POIS +".title as "+SearchManager.SUGGEST_COLUMN_TEXT_1+", " +
			           UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY + "." +UnivmobileDBAdapter.KEY_TITLE +  "||' - '||"+ UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES+"."+UnivmobileDBAdapter.KEY_NAME +" as "+SearchManager.SUGGEST_COLUMN_TEXT_2+", " +
			           "'_'||" +UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." + UnivmobileDBAdapter.KEY_ID + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID+" , " +
			           UnivmobileDBAdapter.DATABASE_TABLE_POIS + ".poi_type_id as type, " +
			           UnivmobileDBAdapter.KEY_ICON_PATH+" as "+SearchManager.SUGGEST_COLUMN_ICON_1+" " +
			           		"from "+ 
			           UnivmobileDBAdapter.DATABASE_TABLE_POIS +
			            " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES  
				  	    + " on " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES + "." + UnivmobileDBAdapter.KEY_ID
				  	    + "=" +  UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." +UnivmobileDBAdapter.KEY_POI_TYPE_ID +
				  	      " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY  
				  	    + " on " + UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_ID
				  	    + "=" +  UnivmobileDBAdapter.KEY_CAT_ID +
				  	    " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY  + 
				  	    " on "+UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY+"." + UnivmobileDBAdapter.KEY_ID +
				  	    "= "+UnivmobileDBAdapter.DATABASE_TABLE_POIS+"." +  UnivmobileDBAdapter.KEY_UNIV_ID+
				  	     " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_POIS  + " AS POI_EXT"+ " on POI_EXT." + UnivmobileDBAdapter.KEY_ID +
				  	     "= "+UnivmobileDBAdapter.DATABASE_TABLE_POIS+"." +  UnivmobileDBAdapter.KEY_PARENT_ID +
				 	   " where "+UnivmobileDBAdapter.DATABASE_TABLE_POIS + ".poi_type_id <>'' " +
				 	   	"and "+UnivmobileDBAdapter.DATABASE_TABLE_POIS + ".poi_type_id <>  1 and category._id <> 1"+
			           " and (lower("+UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." +UnivmobileDBAdapter.KEY_TITLE + ") LIKE '%"+
			           query + "%'" +
			           " or lower("+UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." +UnivmobileDBAdapter.KEY_ADDRESS + ") LIKE '%"+
			           query + "%'" +
			           " or lower(POI_EXT." +UnivmobileDBAdapter.KEY_ADDRESS + ") LIKE '%"+
			           query + "%'" +
			           " or lower("+UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." +UnivmobileDBAdapter.KEY_DESCRIPTION + ") LIKE '%"+
			           query + "%' )";
		  
		 
		 
 		return  mAdapter.executeQuery(sql, null);
	 }
	 
	 
	 private Cursor getSearchCat(String cat) {

		 
		 String sql = "select distinct '_'||"+ UnivmobileDBAdapter.DATABASE_TABLE_POIS +"._id as _id, "+ 
			           UnivmobileDBAdapter.DATABASE_TABLE_POIS +".title as "+SearchManager.SUGGEST_COLUMN_TEXT_1+", " +
			           UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY + "." +UnivmobileDBAdapter.KEY_TITLE +  "||' - '||"+ UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES+"."+UnivmobileDBAdapter.KEY_NAME +" as "+SearchManager.SUGGEST_COLUMN_TEXT_2+", " +
			           "'_'||" +UnivmobileDBAdapter.DATABASE_TABLE_POIS + "." + UnivmobileDBAdapter.KEY_ID + " AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID+" , " +
			           "poi_type_id as type, " +
			           UnivmobileDBAdapter.KEY_ICON_PATH+" as "+SearchManager.SUGGEST_COLUMN_ICON_1+" " +
			           		"from "+ 
			           UnivmobileDBAdapter.DATABASE_TABLE_POIS +
			            " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES  
				  	    + " on " + UnivmobileDBAdapter.DATABASE_TABLE_POI_TYPES + "." + UnivmobileDBAdapter.KEY_ID
				  	    + "=" +  UnivmobileDBAdapter.KEY_POI_TYPE_ID +
				  	      " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY  
				  	    + " on " + UnivmobileDBAdapter.DATABASE_TABLE_CATEGORY + "." + UnivmobileDBAdapter.KEY_ID
				  	    + "=" +  UnivmobileDBAdapter.KEY_CAT_ID +
				  	    " inner join " + UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY  + 
				  	    " on "+UnivmobileDBAdapter.DATABASE_TABLE_UNIVERSITY+"." + UnivmobileDBAdapter.KEY_ID +
				  	    "= "+UnivmobileDBAdapter.DATABASE_TABLE_POIS+"." +  UnivmobileDBAdapter.KEY_UNIV_ID+
				 	   " where poi_type_id <>'' and poi_type_id <>  1 and category._id <> 1"+
			           " and category._id  LIKE "+ cat + "";
		  
		 
		 
 		return  mAdapter.executeQuery(sql, null);
	 }
	
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		 throw new UnsupportedOperationException();
	}
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		  throw new UnsupportedOperationException();
	}
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		 throw new UnsupportedOperationException();
	}
	
	@Override
	protected void finalize() throws Throwable {
		mAdapter.close();
		super.finalize();
	}

}
