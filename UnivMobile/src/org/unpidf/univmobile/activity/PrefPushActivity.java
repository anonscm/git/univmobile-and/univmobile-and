package org.unpidf.univmobile.activity;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.WSUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

public class PrefPushActivity extends PreferenceActivity {

	
	private Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.push_prefs);
		CheckBoxPreference pushSilence= (CheckBoxPreference)findPreference("push_silence");
		mContext = this;

		
		pushSilence.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			public boolean onPreferenceClick(Preference preference) {
				CheckBoxPreference pushSilence = (CheckBoxPreference)preference;
			
				SharedPreferences settings = mContext.getSharedPreferences(WSUtils.PREFS_NAME, 0);
				SharedPreferences.Editor editor = settings.edit();
				
				editor.putBoolean(WSUtils.PUSH_PREF, pushSilence.isChecked());
				editor.commit();
				
				return false;
			}
		});
			
		
	}
	
}
