package org.unpidf.univmobile.activity;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.UnivMobileWebView;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;

public class MainActivity extends Activity {

	
//	private TextView mTextView;
//	private ImageButton mButtonChoixUniv;
	private ImageView mBannerUniv;
	private LinearLayout mLinearLayout;
	private Map<String, Object> mapMenu;

	private AlertDialog.Builder mAlertDialog;
	private ProgressDialog mProgressDialog;
	private Context mContext;
	private String lienWebView;
	private WebView mWebView;
	private SlidingDrawer mSlidingDrawer;
	final Handler uiThreadCallback = new Handler();
	  final Activity activity = this;
	private static final String TAG = "Univmobile - MainActivity";
	
	
	
	@Override
		public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage(getResources().getString(R.string.confirm_quit))
		       .setCancelable(false)
		       .setPositiveButton(getResources().getString(R.string.confirm_yes), new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   finish();
		           }
		       })
		       .setNegativeButton(getResources().getString(R.string.confirm_no), new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		builder.show();
		//AlertDialog alert = builder.create();	
		
		
		}
	
	@Override
		protected void onNewIntent(Intent intent) {
			// TODO Auto-generated method stub
			super.onNewIntent(intent);
			Log.v(TAG, "newIntent");
			afficherNotif(intent);
		}
	
	public void onCreate(Bundle savedInstanceState) {
		 
		  super.onCreate(savedInstanceState);
		  
		  
		  setContentView(R.layout.main);
		 	 mContext = this;
		 	 
		 	//loadGeocampus();
		 

	
			//pour les notifications (on n'afficher rien par defaut)
		 	mSlidingDrawer = (SlidingDrawer) findViewById(R.id.drawer);
		 	mSlidingDrawer.setVisibility(View.INVISIBLE);
		 
		    
		    
		    
		 	mProgressDialog = ProgressDialog.show(MainActivity.this,
                    "", "Chargement", true);   
         
			mBannerUniv = (ImageView) findViewById(R.id.mBannerUniv);
			mAlertDialog = creerAlertReseau(mContext);
			
			
			final Runnable runInUIThread = new Runnable() {
	    	    public void run() {
	    	    	 
	      	          chargerImageUniversite(); 
	      	          
	      	    	mLinearLayout = (LinearLayout) findViewById(R.id.mLinearLayout);
	      	    	
	      	    	// on creer le menu actualite si le service existe
	      	    	if(mapMenu.get(WSUtils.KEY_NEWS_SERVICE)!=null){
	      	    		creerMenu((String) mapMenu.get(WSUtils.KEY_ICON_ACTU),
						(String) mapMenu.get(WSUtils.KEY_TITLE), 
						(String) mapMenu.get(WSUtils.KEY_NEWS_SERVICE));
	      	    	}
	      	    	
	      	    	@SuppressWarnings("unchecked")
					ArrayList<HashMap<String,String>> listMenu = (ArrayList<HashMap<String,String>>)mapMenu.get(WSUtils.KEY_LIST_MENU);
	      	    	
	      	    	if(listMenu==null){
	      	    		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
	      	    		Editor e = settings.edit();
	      	    		e.remove(WSUtils.KEY_UNIV);
	      				e.remove(WSUtils.KEY_REGION_URL);
	      				e.remove(WSUtils.KEY_UNIV_JSON);
	      				e.remove(WSUtils.KEY_REGION_JSON);
	      				e.clear();
	      				e.commit();
	      				Log.v(TAG, "preferences supprimées");
	      	    		mAlertDialog.show();
	      	    		

	      	    	} else { 
	      	    	
	      	    	for (int i = 0; i < listMenu.size(); i++) {
	      	    		HashMap<String,String> menu = (HashMap<String,String>)listMenu.get(i);
	      	    		
	      	    		//if(!"Géocampus".equals((String) menu.get(WSUtils.KEY_TITLE))){
	      	    		creerMenu((String) menu.get(WSUtils.KEY_URL),
	    						(String) menu.get(WSUtils.KEY_TITLE), 
	    						(String) menu.get(WSUtils.KEY_LINK));
	      	    		//}
					} }
	      	    	
	    	    }

			
	    	  };
			
			
      	  new Thread() {
      		    @Override public void run() {
      		    	
      		    	
      		      SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
      			
      			  String idUniv = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
      			  String urlUniversite = settings.getString(WSUtils.KEY_REGION_URL, WSUtils.KEY_NOT_DEFINE);
      			  
      			 String jsonUniversity ="";
      			  if(!WSUtils.KEY_NOT_DEFINE.equals(urlUniversite)){
      				int index = urlUniversite.indexOf("?");
      				
      				String finUrl =  "/iphone/getUniversityConfig.json?universityId="+ idUniv +"&langId=0";
      				if(index!=-1){
      					String url = urlUniversite.substring(0, index) + finUrl;
      					Log.v(TAG,"url modifie="+url);
      					jsonUniversity = WSUtils.getTextFile(url);
      					
      				} else {
      					jsonUniversity = WSUtils.getTextFile(urlUniversite + finUrl);
      				}
      				
      			  } else {
      				  // erreur idUniv Not define -> reinit 
      				creerAlert(mContext, "Erreur", "Veuillez relancer l'application", "OK");
      				Editor e = settings.edit();
      		  		e.remove(WSUtils.KEY_UNIV);
      				e.remove(WSUtils.KEY_REGION_URL);
      				e.remove(WSUtils.KEY_UNIV_JSON);
      				e.remove(WSUtils.KEY_REGION_JSON);
      				e.commit();
      				finish();
      			  }
      			
      			
      			//Log.v(TAG,"jsonUniversity="+  jsonUniversity);
      			  //recuperation des données spécifiques
      			  // banner
      	          if(!WSUtils.isExistantInInternalStorage(idUniv+".gif",mContext)){
                        WSUtils.storeBannerFromJSON(jsonUniversity,idUniv,mContext);
                  }
      	          //chargement des elements du menu 
      	          mapMenu = WSUtils.chargerElementMenu(jsonUniversity, idUniv, mContext, urlUniversite);
      	          	
      	         
      	
      		      mProgressDialog.dismiss();
      		      uiThreadCallback.post(runInUIThread);
      		    }
      		  }.start();
   
		  
      	afficherNotif(this.getIntent());
      	  
	 }
	
	

	private void afficherNotif(Intent intent) {
		Log.v(TAG, "afficherNotif");
		
		Bundle bundle = intent.getExtras();
		
		
		if (bundle != null) {
			String message = bundle.getString(WSUtils.KEY_PUSH);
			String callback_url = bundle.getString(WSUtils.KEY_PUSH_URL);
			
			if(callback_url==null){
			
				if (message != null) {
					if (!message.equals("")) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								mContext);
						builder.setTitle("Notification");
						builder.setMessage(message)
								.setCancelable(false)
								.setPositiveButton(
										getResources().getString(
												R.string.bouton_ok),
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog, int id) {
												dialog.cancel();
											
											}
										})
	//							.setNegativeButton(
	//									getResources().getString(
	//											R.string.close),
	//									new DialogInterface.OnClickListener() {
	//										public void onClick(
	//												DialogInterface dialog, int id) {
	//											finish();
	//										}
	//									})
										;
						builder.show();

	
					}
				}				
			} else {
				
				String callback_title = bundle.getString(WSUtils.KEY_PUSH_TITLE);
				
				if(callback_title!=null){
					TextView mTextTitle = (TextView) findViewById(R.id.mTextTitle);
					mTextTitle.setText(callback_title);
				}
				mWebView = (WebView) findViewById(R.id.notifWebView);
			 	mWebView.setWebViewClient(new WebViewClient());
			 	
			 	if(callback_url != null && callback_url.contains("udid=")){
				 	TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
					String udid = tm.getDeviceId();
					callback_url= callback_url.replace("udid=", "udid="+udid);
			 	}
			 	mWebView.loadUrl(callback_url);
		        mWebView.getSettings().setJavaScriptEnabled(true);
		        mWebView.getSettings().setBuiltInZoomControls(true);
		        mSlidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
					
					public void onDrawerClosed() {
						// TODO Auto-generated method stub
						mSlidingDrawer.setVisibility(View.INVISIBLE);
					}
				});
		        mSlidingDrawer.setVisibility(View.VISIBLE);
		        try {
		        	Thread.sleep(1000);
		        } catch (InterruptedException e) {
					Log.v(TAG, "erreur lors du sleep : " + e.getMessage());
				}
		       
		        mSlidingDrawer.close();
		        mSlidingDrawer.animateOpen();
			}
		}
	}


	
	/**
	 * 
	 */
//	private void loadGeocampus() {
//		// 	  chargement des données géocampus en tache de fond.
//		  new Thread() {
//			    @Override public void run() {
//					SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
//					
//					SharedPreferences.Editor editor = settings.edit();
//					String flagDownload = settings.getString(WSUtils.FLAG_DOWNLOAD_JSON,
//							WSUtils.KEY_NOT_DEFINE);
//					Log.v(TAG,"download JSON = "+ flagDownload);
//					if(flagDownload.equals(WSUtils.FLAG_KO)){
//						editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_RUNNING);
//						editor.commit();
//						String univId = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
//		  				String lang = WSUtils.KEY_LANG;
//		  				
//		  				String urlRegion =WSUtils.getRegionUrl(mContext);
//		  				
//		  				//TODO serveur regional
//		  				//String url = urlRegion+"/ws/"+lang+"/universities/"+univId+"/"+WSUtils.FILE_NAME_POIS;
//		  				String url = WSUtils.constructUrl(urlRegion, univId, lang);
//		  	        	WSUtils.readAndSaveJson(url,WSUtils.FILE_NAME_POIS,mContext, univId, true);
//		  	        	editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_OK);
//		  	        	editor.commit();
//					}
//					Log.v(TAG,"download JSON2 = "+ flagDownload);
//				
//			    }
//			    
//		  }.start();
//	}
	
	


	private void chargerImageUniversite(){
		
		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
		
		String idUniv = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
		
		if(!idUniv.equals(WSUtils.KEY_NOT_DEFINE)){
			Bitmap bmp = getBMapImage(idUniv+".gif");
		if(bmp == null){
			Editor e = settings.edit();
	  		e.remove(WSUtils.KEY_UNIV);
			e.remove(WSUtils.KEY_REGION_URL);
			e.remove(WSUtils.KEY_UNIV_JSON);
			e.remove(WSUtils.KEY_REGION_JSON);
			e.commit();
			Log.v(TAG, "preferences supprimées not region");
			creerAlertReseau(mContext).show();
		} else{
			Display display = getWindowManager().getDefaultDisplay(); 
			float ratio = (float)display.getWidth() / (float)bmp.getWidth();
			int heightImg = (int) (ratio* (float)bmp.getHeight());
			if(mBannerUniv!=null){
				mBannerUniv.setMinimumHeight(heightImg);
				mBannerUniv.setImageBitmap(bmp);
			} 
		}} else {
			 Intent intent;
			 SharedPreferences.Editor editor = settings.edit();
			 editor.remove(WSUtils.KEY_UNIV_JSON);
	    	 editor.commit();
	    	 intent = new Intent(MainActivity.this, ChoixUnivActivity.class);
               startActivity(intent);
               finish();
	    	
		}
		
			
	}
	
	
	/**
	 *  permet de recuperer le Bitmap dans le stockage interne de l'application
	 * @param name nom de l'image
	 * @return
	 */
	private Bitmap getBMapImage(String name){
		FileInputStream fis = null;
		Bitmap bMap = null;
		try {
			
			if(name==null){
				 Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE,"fichier image banner universite introuvable");
			}else{
				fis = openFileInput(name);
				bMap = BitmapFactory.decodeStream(fis);
			}
		} catch (FileNotFoundException e) {
		    Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE,"fichier " + name + " introuvable", e);
		} finally{
			if (fis != null){
				try {
					fis.close();
				} catch (IOException e) {
					 Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE,"Erreur E/S fichier " + name, e);
				}
			}
		}
		return bMap;
	}
	
	
	
	/**
	 * 
	 */
	private void creerMenu(String image, final String titre, final String lien) {
		
		
		LinearLayout ll = new LinearLayout(mContext);

		if(image!=null && !"".equals(image)) {
			ll.setBackgroundResource(R.drawable.menu_layout);

			ll.setPadding(6, 6, 6, 6);
			mLinearLayout.addView(ll);
			ImageView icon = new ImageView(mContext);
			icon.setImageBitmap(getBMapImage(image));
			icon.setLayoutParams(new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.WRAP_CONTENT,
					LinearLayout.LayoutParams.WRAP_CONTENT));
			icon.setPadding(6, 6, 6, 6);
			ll.setVerticalGravity(Gravity.CENTER_VERTICAL);
			ll.addView(icon);
			TextView textView = new TextView(mContext);
			textView.setEnabled(true);
			textView.setText(titre);
			textView.setPadding(6, 2, 6, 6);
			textView.setTextSize(16);
			textView.setTextColor(getResources().getColor(R.color.black));
			ll.addView(textView);
  		
  		
  		ll.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if("Géocampus".equals(titre)){

					
					mProgressDialog = ProgressDialog.show(MainActivity.this,
			                "", "Chargement des données géographiques", true);
					
					 new Thread() {
			   		    @Override public void run() {
			   		    	
			   		    	SharedPreferences settings = getSharedPreferences(
									WSUtils.PREFS_NAME, 0);
			   		    	String flagDownload = settings.getString(WSUtils.FLAG_DOWNLOAD_JSON,
									WSUtils.KEY_NOT_DEFINE);
			   		 
			   		    	while (flagDownload.equals(WSUtils.FLAG_RUNNING)){
			   		    		try {
									sleep(500);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								flagDownload = settings.getString(WSUtils.FLAG_DOWNLOAD_JSON,
										WSUtils.KEY_NOT_DEFINE);
			   		    	}
			   		    	
			   		    	
			   		    	mProgressDialog.dismiss();
			   		    
			   		    	Intent newIntent = new Intent(mContext, GeocampusActivity.class);
			   		    	startActivity(newIntent);
//			   		 	Intent newIntent = new Intent(mContext,
//								GeocampusPrefsActivity.class);
						startActivity(newIntent);
			   		    }
					 }.start();
					
					
					
				} else {
				
					
					if(lien != null && lien.contains("dailymotion")){
						// cas dailymotion
						lienWebView= lien;
						mAlertDialog = creerAlertDailyMotion(mContext);
						mAlertDialog.show();
						
					} else {
						// cas normal 
						Bundle bundle = new Bundle();
						if(lien != null && lien.contains("udid=")){
							//equivalent UDID android
							TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
							String udid = tm.getDeviceId();
							String newLien= lien.replace("udid=", "udid="+udid);
							Log.v("TEST LIEN modifié = ", newLien);
							bundle.putString(WSUtils.KEY_URL, newLien);
						} else {
							bundle.putString(WSUtils.KEY_URL, lien);
						}
							
							
		
						bundle.putString(WSUtils.KEY_HELP,
								(String) mapMenu.get(WSUtils.KEY_HELP));
						bundle.putString(WSUtils.KEY_TITLE, titre);
						Intent newIntent = new Intent(mContext,
								UnivMobileWebView.class);
						newIntent.putExtras(bundle);
						startActivity(newIntent);
					} 
					
				}
			}
			
			
		});
  	//cas newsItems
		
		} else {
		 ll.setBackgroundResource(R.drawable.news_layout);

  	    ll.setPadding(30, 2, 2, 2);
  	  		
  			
  	  	TextView textView = new TextView(mContext);
  		textView.setText(titre);
     	textView.setPadding(6, 2, 2, 2);
  		textView.setTextSize(12);
  	
  		textView.setTextColor(getResources().getColor(R.color.white));
  		ll.addView(textView);
  		mLinearLayout.addView(ll);
  		
  		ll.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if("Géocampus".equals(titre)){
					
//					Intent newIntent = new Intent(mContext, GeocampusActivity.class);
//					startActivity(newIntent);
//					
					
					Intent newIntent = new Intent(mContext,
							GeocampusPrefsActivity.class);
					startActivity(newIntent);
						
					
				} else {
				

					
					Bundle bundle = new Bundle();
					bundle.putString(WSUtils.KEY_URL, lien + "&backurl=./");
					bundle.putString(WSUtils.KEY_HELP,
							(String) mapMenu.get(WSUtils.KEY_HELP));
					bundle.putString(WSUtils.KEY_TITLE, "Actualités");
					Intent newIntent = new Intent(mContext,
							UnivMobileWebView.class);
					newIntent.putExtras(bundle);
				
					startActivity(newIntent);
				}
			}
			
			
		});
		}
  	
  		
  		
  	  		
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.univmobilemenu, menu);
	    return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	
		 SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
    	 SharedPreferences.Editor editor = settings.edit();
    	
	    switch (item.getItemId()) {
	    case R.id.mItemRegion:
	    	 editor.remove(WSUtils.KEY_REGION_URL);
	    	 editor.remove(WSUtils.KEY_REGION_ID);
	    	 editor.remove(WSUtils.KEY_REGION_JSON);
	    	 editor.remove(WSUtils.KEY_UNIV);
	    	 editor.remove(WSUtils.KEY_UNIV_JSON);
	    	 editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_KO);
	    	 editor.commit();
	    	 
	    	 mProgressDialog = ProgressDialog.show(this, "Patientez svp",
	     	          "Chargement des régions...", true);
	         new Thread(new Runnable() {
	 			
	 			public void run() {
	 				 WSUtils.chargerRegions(mContext);
	 				 mProgressDialog.dismiss();
	 				 Intent intent =   new Intent(MainActivity.this, ChoixRegionActivity.class);
	 				  startActivity(intent);
	 			     finish();   
	 				
	 			}
	 			}).start();
	    	 
//	    	 intent = new Intent(MainActivity.this, ChoixRegionActivity.class);
//             startActivity(intent);
//             finish();
	        return true;
	    case R.id.mItemUniv:
	    	// editor.remove(WSUtils.KEY_UNIV);
	    	 editor.remove(WSUtils.KEY_UNIV_JSON);
	    	 editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_KO);
	    	 editor.commit();
	    	 

	         mProgressDialog = ProgressDialog.show(this, "Patientez svp",
	     	          "Chargement des universités...", true);
	         new Thread(new Runnable() {
	 			
	 			public void run() {
	 				 WSUtils.chargerUniversite(mContext);
	 				 mProgressDialog.dismiss();
	 				 Intent intent =  new Intent(MainActivity.this, ChoixUnivActivity.class);
	 				  startActivity(intent);
	 			     finish();   
	 				
	 			}
	 			}).start();
	         
	    	 
//	    	 intent = new Intent(MainActivity.this, ChoixUnivActivity.class);
//             startActivity(intent);
//             finish();
	    	
	        return true;
	    case R.id.mItemClose:
		     finish();
	        return true;
	    case R.id.mItemPref:

	    	Intent pref =  new Intent(MainActivity.this, PrefPushActivity.class);
	    	startActivity(pref);
	    	
	    	return true;
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	private AlertDialog.Builder creerAlertReseau(Context context){
		

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		String title = "Alerte";
		String message = "Aucune connexion réseau";
		String buttonOK = "Quitter";
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton(buttonOK, new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				finish();
				
			}
		});
		
		return alertDialog;
	}
	
	private AlertDialog.Builder creerAlert(Context context, String title, String msg, String buttonOK){
		

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(title);
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton(buttonOK, new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				finish();
				
			}
		});
		
		return alertDialog;
	}
	
	
	private AlertDialog.Builder creerAlertDailyMotion(Context context){
		

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		String title = "Information";
		String message = "Redirection vers le navigateur internet pour un meilleur rendu vidéo";
		String buttonOK = "OK";
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton(buttonOK, new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				Intent i = new Intent(Intent.ACTION_VIEW);
		    	String urltmp = lienWebView;
		    	if (lienWebView!=null && lienWebView.contains("iphone=true")){
		    		urltmp = urltmp.replace("iphone=true", "iphone=false");
		    	}
		    	Log.v(TAG,"url de redirection : "+urltmp);
		    	Uri u = Uri.parse(urltmp);
		    	i.setData(u);
				startActivity(i);
				
			}
		});
		alertDialog.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
			
				//mLinearLayoutClicked.setBackgroundResource(R.drawable.bgmainmenu);
			}
		});
		return alertDialog;
		
	}
	
}
