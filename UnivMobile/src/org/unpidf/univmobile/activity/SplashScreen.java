package org.unpidf.univmobile.activity;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

public class SplashScreen extends Activity {
    
	private static final String TAG = "Univmobile - SplashScreen";
    protected boolean _active = true;
    protected int _splashTime = 3500; // time to display the splash screen in ms
    private boolean isNetworkOK = true; 
    private Context mContext;
    
   // public static final String URL_REGION = "http://univmobile-prod.univ-paris1.fr/iphone/regions.json";
   // public static final String URL_REGION ="http://univmobile-prod.univ-paris1.fr/iphone/regions.json?udid=dacc532e76634d628171c7b50d3fee3210e967e9";
   // public static final String URL_REGION = R.string.url_region;

    
    
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);
        mContext=this;
       // loadGeocampusData();
        if(isOnline()){
        	
        	
        	String key_univ = getSharedPreferences(WSUtils.PREFS_NAME, 0).getString(WSUtils.KEY_UNIV, "");
			if(!"".equals(key_univ)){
				WSUtils.register(getApplicationContext());
			}
        	
        	
        // thread for displaying the SplashScreen
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
//                	int waited = 0;
                	checkUpdate();
                	
                	Long time = (new Date()).getTime();
                	SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
                	
//                	Intent unregIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
//                	unregIntent.putExtra("app", PendingIntent.getBroadcast(SplashScreen.this, 0, new Intent(), 0));
//                	startService(unregIntent);

				
                	
                	
                	
                
					
                	
                	SharedPreferences.Editor editor = settings.edit();
            		editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_KO);
            		editor.commit();
                	
                	//recuperation de la liste des régions
                	String regionjson = settings.getString(WSUtils.KEY_REGION_JSON, WSUtils.KEY_NOT_DEFINE);
                	
                	if(WSUtils.KEY_NOT_DEFINE.equals(regionjson)){
                		regionjson = WSUtils.getTextFile(mContext.getText(R.string.server_region).toString() + mContext.getText(R.string.url_region).toString());
                		
                		
                		if("".equals(regionjson)){
                			Log.v(TAG,"erreur reseau");
                			isNetworkOK= false;
                			
                		}else {                		
                			
                			editor.putString(WSUtils.KEY_REGION_JSON, regionjson);
                			editor.commit();
                		}
                	} else {
                		isNetworkOK = WSUtils.chargerUniversite(mContext);
                	}
                	
                	 while(_active && ((new Date()).getTime()-time  < _splashTime)) {
                         sleep(100);
                     }

                	 // gestion du cas ou les preferences sont enregistrées.        
                    if(	isNetworkOK){
                	 
                	 String universite = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
                     if(WSUtils.KEY_NOT_DEFINE.equals(universite)){
                         Intent intent = new Intent(SplashScreen.this, ChoixRegionActivity.class);
                         startActivity(intent);
                         finish();
                        
                      } else {
                    	//remove SplashScreen from view
                    	
                    	  loadDataGeocampusForAll(mContext);
                    	
                    	  /**/
                    	  String jsonUniv = WSUtils.getJsonUniv(settings);
                          
                          try {
                       	  		JSONObject jsonObject = new JSONObject(jsonUniv);
                 				JSONArray jsonArray = jsonObject.getJSONArray("universities");
                 			    //Loop though my JSONArray
                 				ArrayList<String[]> listUniversite = new ArrayList<String[]>();
                 			    for(Integer i=0; i< jsonArray.length(); i++){
                 			     
                 			            //Get My JSONObject and grab the String Value that I want.
                 			            String title = jsonArray.getJSONObject(i).getString("title");
                 			            String id = jsonArray.getJSONObject(i).getString("id");
                 			            String[] tan = {id,title};
                 			            listUniversite.add(tan);
                 			    }	
                 			    
                 			  WSUtils.updateUnivInDB(listUniversite, mContext);
                          }  catch (JSONException e) {
                				// TODO Auto-generated catch block
                				e.printStackTrace();
                			}
                        	 
                        	
                    	  /**/
                    	  Intent intent =  new Intent(SplashScreen.this, MainActivity.class);
                       	 startActivity(intent);
                       	 finish();
                      }
                    } else {
                    	Log.v(TAG,"erreur reseau recuperée et traitée");
                    	
                    	 Intent intent =  new Intent(SplashScreen.this, MainActivity.class);
                       	 startActivity(intent);
                       	 finish();
                    }
                    
                    
                	
                    
                } catch(InterruptedException e) {
                    // do nothing
                } finally {
                   
                	
                	finish();
//                    startActivity(new Intent(SplashScreen.this, ChoixUnivActivity.class));
                    stopService(getIntent());
                }
            }
        };
        splashTread.start();
        } else {
        	AlertDialog.Builder a = creerAlert(mContext, "Information", "Connexion indisponible, veuillez vérifier votre connexion et réessayer plus tard", "Ok");
        	a.show();
        }
        
    }


    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            _active = false;
        }
        return true;
    }
    

    
    
    private void loadDataGeocampusForAll(Context ctx){
    
    	 new Thread() {
			    @Override public void run() {
					SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
					
					SharedPreferences.Editor editor = settings.edit();
					String flagDownload = settings.getString(WSUtils.FLAG_DOWNLOAD_JSON,
							WSUtils.KEY_NOT_DEFINE);
					Log.v(TAG,"download JSON = "+ flagDownload);
					if(flagDownload.equals(WSUtils.FLAG_KO)){
						
						editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_RUNNING);
						editor.commit();
						// recuperation static du projet vide
						String region = settings.getString(WSUtils.KEY_REGION_URL, WSUtils.KEY_NOT_DEFINE);
						if(region.contains("?")){
                			int index = region.indexOf("?");
                			region =region.substring(0, index);
						}
						String url = region +"/ws/fr/universities/noPoi/pois.json";
						
//						if(region.contains("ueb")){
//							url ="http://univmobile.ueb.eu/ws/fr/universities/rennes1/pois.json";
//						}
		  				
		  				//WSUtils.readAndSaveJsonInDb(url,univId + "_" +WSUtils.FILE_NAME_POIS,mContext, univId, true);
		  				WSUtils.readAndSaveAllExeptPoisInDb(url, "noPoi.json", mContext);
		  				
		  	        	editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_OK);
		  	        	editor.commit();
					}
					Log.v(TAG,"download JSON2 = OK");
				
			    }
			    
		  }.start();
    	
    }
    
    private AlertDialog.Builder creerAlert(Context context, String title, String msg, String buttonOK){
		

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		alertDialog.setTitle(title);
		alertDialog.setMessage(msg);
		alertDialog.setPositiveButton(buttonOK, new DialogInterface.OnClickListener() {			
			public void onClick(DialogInterface dialog, int which) {
				finish();
				
			}
		});
		
		return alertDialog;
	}
	
    
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    // supprime le cache lors de la mise à jour.
    private void checkUpdate(){
    	
    	SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
    	int version = settings.getInt(WSUtils.KEY_PREFS_VERSION, 0);
		Editor editor =settings.edit();
		if (version<WSUtils.VERSION){
			editor.clear();
			editor.putInt(WSUtils.KEY_PREFS_VERSION, WSUtils.VERSION);
    	}   	
    	editor.commit();
    }
}
