package org.unpidf.univmobile.activity;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class ChoixUnivActivity extends Activity {
	

	private static final String TAG = "Univmobile - UnivMobileActivity";	
	 
	
	private Spinner mSpinner;
	private Button mButtonOk;
	//private boolean spinnerLoaded = false;
	
	private Context mContext;
	private String jsonUniv ="";

	//hashmaps pour faire le lien entre id android et id univmobile
    HashMap<String, String> mapIdUniversity;
    HashMap<String, String> mapUniversityId;
    private ProgressDialog mProgressDialog;
    private Thread threadGeocampus;
    
    
    private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);

			 execute();
			 mProgressDialog.dismiss();

		}

	};
    
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.choixuniv);
     
     mContext = this;
	 
     
   	 mProgressDialog = ProgressDialog.show(this, "Patientez svp",
   	          "Chargement des universités...", true);
  
 
  
    runOnUiThread(new Runnable() {
			
			public void run() {
				handler.sendEmptyMessage(0);
			}
		});
    	
    	
        
    }
    
    private void execute(){
    	
    	
    	// suppression des sauvegardes de géolocalisation 
    	if (WSUtils.existFile(WSUtils.FILE_NAME_POIS, this)){
    		deleteFile(WSUtils.FILE_NAME_POIS);
    	}
    	
    	
    	
    	mSpinner = (Spinner) findViewById(R.id.mSpinner);
        
    
    	
    	mButtonOk = (Button) findViewById(R.id.mButtonOk);
         
    //	json = WSUtils.getTextFile("http://univmobile-prod.univ-paris1.fr/iphone/listUniversities.json");
    	SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
    	//jsonUniv = settings.getString(WSUtils.KEY_UNIV_JSON, "");
          
    	
        
          // list a injecter dans le spinner
          ArrayAdapter<CharSequence> adapter = null;
          
        
          
          jsonUniv = settings.getString(WSUtils.KEY_UNIV_JSON, WSUtils.KEY_NOT_DEFINE);
          
           try {
        	   adapter = new ArrayAdapter<CharSequence>(mContext,  android.R.layout.simple_spinner_item);
  				JSONObject jsonObject = new JSONObject(jsonUniv);
  				mapUniversityId = new HashMap<String, String>();
  				mapIdUniversity = new HashMap<String, String>();
          		
          	
  				JSONArray jsonArray = jsonObject.getJSONArray("universities");
  			    //Loop though my JSONArray
  				ArrayList<String[]> listUniversite = new ArrayList<String[]>();
  			    for(Integer i=0; i< jsonArray.length(); i++){
  			     
  			            //Get My JSONObject and grab the String Value that I want.
  			            String title = jsonArray.getJSONObject(i).getString("title");
  			            String id = jsonArray.getJSONObject(i).getString("id");
  			            //Add the string to the list
  			            adapter.add(title);
  			            String[] tan = {id,title};
  			            listUniversite.add(tan);
  			            mapIdUniversity.put(String.valueOf(i), id);
  			            mapUniversityId.put(id, String.valueOf(i));
  			    }	
  			    
  			  WSUtils.storeUnivInDb(listUniversite, mContext);
  			    
          	 }  catch (JSONException e) {
  				// TODO Auto-generated catch block
  				e.printStackTrace();
  			}
          	 
          	
          	
          
          
          adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          
          mSpinner.setAdapter(adapter);
    
         
         // université selectionnée par defaut
         String universitySelected = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
      	
      	  if(!WSUtils.KEY_NOT_DEFINE.equals(universitySelected)){
      		if(mapUniversityId.containsKey(universitySelected)){
      		  mSpinner.setSelection(Integer.parseInt(mapUniversityId.get(universitySelected)));
      		}
      	 }
          
          mButtonOk.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				 
				String spinnerID = String.valueOf(mSpinner.getSelectedItemId());
				String universiteID = mapIdUniversity.get(spinnerID);
				Log.v(TAG, "University selected ="+ universiteID);
	            
				// gestion des préférences
                 SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
                 SharedPreferences.Editor editor = settings.edit();
                 
				 editor.putString(WSUtils.KEY_UNIV, universiteID);
                // Commit the edits!
				 editor.putBoolean(universiteID, true);
	              editor.commit();
	          //    if(WSUtils.getUnivSelected(mContext)==0){
	              
		              WSUtils.updateUnivPref(universiteID, true, getApplicationContext());
		              loadGeocampus();
	           //   }
		        if(!"".equals(universiteID)){
		  				WSUtils.register(getApplicationContext());
		  		}
		          	
		              
		              
	              Intent intent =  new Intent(ChoixUnivActivity.this, MainActivity.class);
                  startActivity(intent);
                  finish();   
	              

				 
			}
          });
        	
  

          
    	
    	
    }

	

    @Override
	public void onBackPressed() {
	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
	builder.setMessage(getResources().getString(R.string.confirm_quit))
	       .setCancelable(false)
	       .setPositiveButton(getResources().getString(R.string.confirm_yes), new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
                	SharedPreferences.Editor editor = settings.edit();
                	editor.remove(WSUtils.KEY_REGION_JSON);
                	editor.remove(WSUtils.KEY_UNIV_JSON);
	        	   //editor.putString(WSUtils.KEY_REGION_JSON, regionjson);
        			editor.commit();
	        	   finish();
	           }
	       })
	       .setNegativeButton(getResources().getString(R.string.confirm_no), new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	                dialog.cancel();
	           }
	       });
	builder.show();
	//AlertDialog alert = builder.create();	
	
	
	}
    
    private void loadGeocampus() {
		// 	  chargement des données géocampus en tache de fond.
    	
    	if(threadGeocampus!=null){
    		threadGeocampus.stop();
    	}
    	
    	threadGeocampus = new Thread() {
			    @Override public void run() {
					SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
					
					SharedPreferences.Editor editor = settings.edit();
					String flagDownload = settings.getString(WSUtils.FLAG_DOWNLOAD_JSON,
							WSUtils.KEY_NOT_DEFINE);
					Log.v(TAG,"download JSON = "+ flagDownload);
					if(flagDownload.equals(WSUtils.FLAG_KO)){
						editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_RUNNING);
						editor.commit();
						String univId = settings.getString(WSUtils.KEY_UNIV, WSUtils.KEY_NOT_DEFINE);
		  				String lang = WSUtils.KEY_LANG;
		  				
		  				String urlRegion =WSUtils.getRegionUrl(mContext);
		  				
		  				//TODO serveur regional
		  				//String url = urlRegion+"/ws/"+lang+"/universities/"+univId+"/"+WSUtils.FILE_NAME_POIS;
		  				String url = WSUtils.constructUrl(urlRegion, univId, lang);
		  	        	WSUtils.readAndSaveJsonInDb(url,univId + "_" +WSUtils.FILE_NAME_POIS,mContext, univId, true);
		  	        	editor.putString(WSUtils.FLAG_DOWNLOAD_JSON, WSUtils.FLAG_OK);
		  	        	editor.commit();
					}
					Log.v(TAG,"download JSON2 = "+ flagDownload);
				
			    }
			    
		  };
		  threadGeocampus.start();
		  
	}
	

}