package org.unpidf.univmobile.activity;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.UnivmobileDBAdapter;
import org.unpidf.univmobile.util.UnivmobileProvider;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * The main activity for the dictionary.
 * Displays search results triggered by the search dialog and handles
 * actions from search suggestions.
 */
public class GeocampusResult extends Activity {
	private static final String TAG = "Univmobile - GeocampusResult";
    private TextView mTextView;
    private ListView mListView;
    private Cursor mCursor;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);
        
        String query = getIntent().getStringExtra(SearchManager.QUERY);
        //Toast.makeText(getBaseContext(), query, Toast.LENGTH_SHORT).show();
        
        
        mTextView = (TextView) findViewById(R.id.text_list);
        mListView = (ListView) findViewById(R.id.list);

        if(getIntent().getStringExtra(WSUtils.KEY_CAT_SELECT)!=null){
        	showResults2(getIntent().getStringExtra(WSUtils.KEY_CAT_SELECT));
        } else {
        	showResults(query);
        }	
    
    }

    
    @Override
    protected void onDestroy() {
    	if(mCursor!=null){
    		mCursor.close();
    	}
    	super.onDestroy();
    }
//    @Override
//    protected void onNewIntent(Intent intent) {
//        // Because this activity has set launchMode="singleTop", the system calls this method
//        // to deliver the intent if this actvity is currently the foreground activity when
//        // invoked again (when the user executes a search from this activity, we don't create
//        // a new instance of this activity, so the system delivers the search intent here)
//        handleIntent(intent);
//    }
//
//    private void handleIntent(Intent intent) {
//        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
//            // handles a click on a search suggestion; launches activity to show word
////            Intent wordIntent = new Intent(this, WordActivity.class);
////            wordIntent.setData(intent.getData());
////            startActivity(wordIntent);
////            finish();
//        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
//            // handles a search query
//            String query = intent.getStringExtra(SearchManager.QUERY);
//            Toast.makeText(this.getApplicationContext(), query, Toast.LENGTH_SHORT).show();
//            showResults(query);
//        }
//    }

    /**
     * Searches the dictionary and displays results for the given query.
     * @param query The search query
     */
    private void showResults(String query) {

    	mCursor = managedQuery(UnivmobileProvider.CONTENT_URI_SEARCH, null, null,
                                new String[] {query}, null);

        if (mCursor == null) {
            // There are no results
            mTextView.setText(getString(R.string.no_results, new Object[] {query}));
        } else {
            // Display the number of results
            int count = mCursor.getCount();
            String countString = getResources().getQuantityString(R.plurals.search_results,
                                    count, new Object[] {count, query});
            mTextView.setText(countString);

            // Specify the columns we want to display in the result
            String[] from = new String[] { BaseColumns._ID,
            								SearchManager.SUGGEST_COLUMN_TEXT_1,
                                           SearchManager.SUGGEST_COLUMN_TEXT_2,
                                           SearchManager.SUGGEST_COLUMN_ICON_1};

//            // Specify the corresponding layout elements where we want the columns to go
            int[] to = new int[] { R.id.id_result,
            						R.id.title_result,
                                   R.id.description_result,
                                   R.id.img_result};

            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter words = new SimpleCursorAdapter(this,
                                          R.layout.result, mCursor, from, to);
            
            mListView.setAdapter(words);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // Build the Intent used to open WordActivity with a specific word Uri
                    Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
                    LinearLayout ll =(LinearLayout)view;
                    LinearLayout ll2 = (LinearLayout)ll.getChildAt(1);
                    TextView tv = (TextView)ll2.getChildAt(0);
                    String value = String.valueOf(tv.getText());
                    Log.v(TAG,"id = "+value);
                    geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
                    SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
                	SharedPreferences.Editor editor = settings.edit();

        	    	// cas categorie       	    	
        	    	if(value.indexOf("_")==-1){
        				editor.remove(WSUtils.KEY_POI_SELECT);
        				editor.putString(WSUtils.KEY_CAT_SELECT, value);
        				Log.v(TAG, "KEY_POI_SELECT removed KEY_CAT_SELECT add : " +value);
        				

        			} else {// cas poi
        				value = value.substring(1);
        				editor.putString(WSUtils.KEY_POI_SELECT, value);
        				Log.v(TAG, "KEY_POI_SELECT add : " +value);
        				
        				
        				
        			}
        			
        	    	
        	    	editor.commit();
        	    	
        	    	startActivity(geocampIntent);
        	    	finish();
                }
            });
            
            
        }
    }
    private void showResults2(String query) {

    	mCursor = managedQuery(UnivmobileProvider.CONTENT_URI_CAT, null, null,
                                new String[] {query}, null);

    	UnivmobileDBAdapter univmobileDBAdapter= new UnivmobileDBAdapter(getApplicationContext());
    	univmobileDBAdapter.open();
    	String cat = univmobileDBAdapter.getCategoryName(query);
    	univmobileDBAdapter.close();
    	
        if (mCursor == null) {
            // There are no results
            mTextView.setText(getString(R.string.no_results, new Object[] {query}));
        } else {
            // Display the number of results
            int count = mCursor.getCount();
            String countString = getResources().getQuantityString(R.plurals.cat_results,
                                    count, new Object[] {count,cat});
            mTextView.setText(countString);

            // Specify the columns we want to display in the result
            String[] from = new String[] { BaseColumns._ID,
            								SearchManager.SUGGEST_COLUMN_TEXT_1,
                                           SearchManager.SUGGEST_COLUMN_TEXT_2,
                                           SearchManager.SUGGEST_COLUMN_ICON_1};

//            // Specify the corresponding layout elements where we want the columns to go
            int[] to = new int[] { R.id.id_result,
            						R.id.title_result,
                                   R.id.description_result,
                                   R.id.img_result};

            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter words = new SimpleCursorAdapter(this,
                                          R.layout.result, mCursor, from, to);
            
            mListView.setAdapter(words);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // Build the Intent used to open WordActivity with a specific word Uri
                    Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
                    LinearLayout ll =(LinearLayout)view;
                    LinearLayout ll2 = (LinearLayout)ll.getChildAt(1);
                    TextView tv = (TextView)ll2.getChildAt(0);
                    String value = String.valueOf(tv.getText());
                    Log.v(TAG,"id = "+value);
                    geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
                    SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
                	SharedPreferences.Editor editor = settings.edit();

        	    	// cas categorie       	    	
        	    	if(value.indexOf("_")==-1){
        				editor.remove(WSUtils.KEY_POI_SELECT);
        				editor.putString(WSUtils.KEY_CAT_SELECT, value);
        				Log.v(TAG, "KEY_POI_SELECT removed KEY_CAT_SELECT add : " +value);
        				

        			} else {// cas poi
        				value = value.substring(1);
        				editor.putString(WSUtils.KEY_POI_SELECT, value);
        				Log.v(TAG, "KEY_POI_SELECT add : " +value);
        				
        				
        				
        			}
        			
        	    	
        	    	editor.commit();
        	    	
        	    	startActivity(geocampIntent);
        	    	finish();
                }
            });
            
            
        }
    }
    @Override
    public void onBackPressed() {
    	 Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
    	  geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
    	  startActivity(geocampIntent);
    	  finish();
    }
    
    @Override
    public boolean onSearchRequested() {
      Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
   	  startActivity(geocampIntent);
   	  finish();
   	  return true;
    }
    
    
    
    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.options_menu, menu);
//
//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
//        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
//        searchView.setIconifiedByDefault(false);
//
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.search:
//                onSearchRequested();
//                return true;
//            default:
//                return false;
//        }
//    }
}