package org.unpidf.univmobile.activity;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class ChoixRegionActivity extends Activity {
	

	private static final String TAG = "Univmobile - ChoixRegionActivity";	

	
	private Spinner mSpinnerRegion;
	private Button mButtonOk;
	//private boolean spinnerLoaded = false;
	
	private Context mContext;
	private String json ="";
	 private ProgressDialog mProgressDialog;
	//hashmaps pour faire le lien entre id android et id univmobile
    HashMap<String, String> mapRegionURL;
    
    
	private Handler handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {

			super.handleMessage(msg);

			 execute();
			 mProgressDialog.dismiss();

		}

	};
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	 
    	super.onCreate(savedInstanceState);
    	 setContentView(R.layout.choixregion);
    	 mContext = this;
    	 mSpinnerRegion = (Spinner) findViewById(R.id.mSpinnerRegion);
    	 
    	 mButtonOk = (Button) findViewById(R.id.mButtonOk);
    	 
         
    	 
    	 mProgressDialog = ProgressDialog.show(this, "Patientez svp",
      	          "Chargement des universités...", true);
    	 
     	 
    	 runOnUiThread(new Runnable() {
 			
 			public void run() {
 				handler.sendEmptyMessage(0);
 			}
 		});
    	
     	
       
       
       
    }

	private void execute() {
		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
		json = settings.getString(WSUtils.KEY_REGION_JSON, "");
         if("".equals(json)){
        	 json = WSUtils.getTextFile(mContext.getText(R.string.server_region).toString() + mContext.getText(R.string.url_region).toString());
         }
         
         
     	
         
         // list a injecter dans le spinner
         ArrayAdapter<CharSequence> adapter = null;
         if(json == null || ("").equals(json)){
        	 //TODO gerer l'erreur pas de réseau
        	 Log.v(TAG, "json=null");
        	 finish();
        	 
         } else {
        	 
        	 adapter = jsonRegionToAdapter();
        	
         }
         
         if(adapter == null){
        	 //TODO gerer l'erreur 
        	 Log.v(TAG, "adapter=null");
        	 finish();
         } else {
        
             adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
             mSpinnerRegion.setAdapter(adapter);
             
         }
         
        
         
       // region selectionnée par defaut
       String regionSelected = settings.getString(WSUtils.KEY_REGION_ID, WSUtils.KEY_NOT_DEFINE);
       if(!WSUtils.KEY_NOT_DEFINE.equals(regionSelected)){
    	   mSpinnerRegion.setSelection(Integer.parseInt(regionSelected));	
 	   }

       
       mButtonOk.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				String regionIdSelected = String.valueOf(mSpinnerRegion.getSelectedItemId());
				goToRegionSelected(regionIdSelected);
				 
			}
       });
	}

	/**
	 * 
	 */
	private ArrayAdapter<CharSequence> jsonRegionToAdapter() {
		ArrayAdapter<CharSequence> adapter = null;
		try {
			JSONObject jsonObject = new JSONObject(json);
			mapRegionURL = new HashMap<String, String>();
			
			adapter = new ArrayAdapter<CharSequence>(mContext,  android.R.layout.simple_spinner_item);
		
			JSONArray jsonArray = jsonObject.getJSONArray("region");
			
			
		    //Loop though my JSONArray
		    for(Integer i=0; i< jsonArray.length(); i++){
		     
		            //Get My JSONObject and grab the String Value that I want.
		            String title = jsonArray.getJSONObject(i).getString("label");
		            String url = jsonArray.getJSONObject(i).getString("url");
		            //Add the string to the list
		            adapter.add(title);
		            mapRegionURL.put(String.valueOf(i),url);
		    }	
		    
		    if(jsonArray.length() == 1){
		    	goToRegionSelected("0");				
			}
		    
		 }  catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return adapter;
	}
    
	
	private void goToRegionSelected(String regionIdSelected){
		
		String regionURL = mapRegionURL.get(regionIdSelected);
		
		
		
		Log.v(TAG, "regionURL ="+ regionURL);
		Log.v(TAG, "regionIdSelected ="+ regionIdSelected);
		// gestion des préférences
		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
      
		editor.putString(WSUtils.KEY_REGION_URL, regionURL);
		editor.putString(WSUtils.KEY_REGION_ID, regionIdSelected);
		 
     
        editor.commit();
     
        mProgressDialog = ProgressDialog.show(this, "Patientez svp",
    	          "Chargement des universités...", true);
        new Thread(new Runnable() {
			
			public void run() {
				 WSUtils.chargerUniversite(mContext);
				 mProgressDialog.dismiss();
				 Intent intent =  new Intent(ChoixRegionActivity.this, ChoixUnivActivity.class);
				  startActivity(intent);
			     finish();   
				
			}
		}).start();
        
          
      
	}
      
	
	
	
	 @Override
		public void onBackPressed() {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage(getResources().getString(R.string.confirm_quit))
		       .setCancelable(false)
		       .setPositiveButton(getResources().getString(R.string.confirm_yes), new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
	                	SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
	                	SharedPreferences.Editor editor = settings.edit();
	                	editor.remove(WSUtils.KEY_REGION_JSON);
		        	   //editor.putString(WSUtils.KEY_REGION_JSON, regionjson);
            			editor.commit();
		        	   
		        	   finish();
		           }
		       })
		       .setNegativeButton(getResources().getString(R.string.confirm_no), new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		builder.show();
	 }
}