package org.unpidf.univmobile.activity;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.geocampus.Marqueur;
import org.unpidf.univmobile.geocampus.MarqueurViewers;
import org.unpidf.univmobile.util.UnivmobileDBAdapter;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MyLocationOverlay;
import com.markupartist.android.widget.ActionBar;
import com.markupartist.android.widget.ActionBar.Action;
import com.markupartist.android.widget.ActionBar.IntentAction;
;

public class GeocampusActivity extends MapActivity {

	public static final String FIRST_USE = "firstuse";
	private static final String GEOLOC = "geoloc";
	public static final String FROMLIST = "fromList";
	UnivmobileDBAdapter adapter;
	private ProgressDialog mProgressDialog;
	final Handler uiThreadCallback = new Handler();
	private static final String TAG = "Univmobile - GeocampusActivity";
	
	private boolean isSearchingPosition = false;
	private MyLocationOverlay myLocationOverlay;
	/**POPUP WINDOW */ 
	private Dialog popupHelp = null;

	
	
	@Override
	public void onNewIntent(Intent newIntent) {
		
	if (Intent.ACTION_SEARCH.equals(newIntent.getAction())) {
	      handleSearch(newIntent);
	}
	isSearchingPosition = false;
	String title = handleView(newIntent);  
	
	
	
	// Get the intent, verify the action and get the query
	

    final ActionBar actionBar = (ActionBar) findViewById(R.id.actionbar);
    actionBar.setTitle(title);
    
    
    MarqueurViewers mv = (MarqueurViewers)findViewById(R.id.map_location_viewer);
    if(myLocationOverlay!=null){
    	mv.getMapView().getOverlays().remove(myLocationOverlay);
    } 
    
    mv.refreshMapMarqueurs();
    mv.invalidate();



	}
	
	
	
	
	
	
	
	
	
	
	public void onCreate(Bundle savedInstanceState) {
		
		 mProgressDialog = ProgressDialog.show(this, "Patientez svp",
   	          "Chargement des cartes", true);
   	  
		
		if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
			 		handleSearch(getIntent());
	            }
		
		isSearchingPosition = false;
     
		 
		
    	
		adapter = new UnivmobileDBAdapter(getApplicationContext());
		findViewById(R.xml.searchable);
		
		
		
		
		String title = handleView(getIntent());
	
	    
	   
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.geocampus);
		
		// ACTION BAR
		
		final ActionBar actionBar = (ActionBar) findViewById(R.id.actionbar);
		final Action searchAction = new IntentAction(this, null, R.drawable.ic_action_listpoint);
		
	    actionBar.setHomeAction(searchAction);
	    actionBar.setTitle(title);
	    RelativeLayout rll = (RelativeLayout) findViewById(R.id.actionbar_home);
	    ImageButton rllbg = (ImageButton) findViewById(R.id.actionbar_home_btn);
	    OnClickListener searchClick = new OnClickListener() {
	   	     
            public void onClick(View v) {
            	onSearchRequested();
            	
            }
        };
       
	    rll.setOnClickListener(searchClick);
	    rllbg.setOnClickListener(searchClick);
        
		final Action settingsAction = new IntentAction(this, createSettingsIntent(getApplicationContext()), R.drawable.ic_action_settings);
	    actionBar.addAction(settingsAction);
	    actionBar.addAction(new Action() {
			public void performAction(View view) {
				Toast.makeText(getApplicationContext(), "En attente d'informations de localisation", Toast.LENGTH_SHORT).show();
				moveToGeoloc();
			}
			public int getDrawable() {
				return R.drawable.ic_action_locate;
			}
		});
	 
	    
//	    ImageButton clicksettings = (ImageButton) findViewById(R.id.actionbar_item);
//	    registerForContextMenu(clicksettings);
//	    clicksettings.setOnClickListener(new OnClickListener() {
//	   	     
//	    	public void onClick(View v) {
//	    		 v.setLongClickable(false);
////	    		 openContextMenu(v);
//	    	}
//	    
//        });
//	    clickLocate.setOnClickListener(new OnClickListener() {
//	   	     
//            public void onClick(View v) {
//            	moveToGeoLocalisationIntent();
//            }
//        });
	    // retour au menu en fermant la webview
//		mButtonRetourUniv = (ImageButton) findViewById(R.id.mButtonRetourUniv);
//		mButtonRetourUniv.setOnClickListener(new View.OnClickListener() {
//			public void onClick(View v) {
//				finish();
//			}
//		});
	    
	    MarqueurViewers mv = (MarqueurViewers)findViewById(R.id.map_location_viewer);
	    if(myLocationOverlay==null){
	    	myLocationOverlay = new MyLocationOverlay(this,
	    			mv.getMapView());
	    	myLocationOverlay.enableMyLocation();
	    	Log.v("myLocation runOnFistFix", "start getMyLocation");
	    	myLocationOverlay.runOnFirstFix(new Runnable() {
				public void run() {
					
					myLocationOverlay.getMyLocation();
					Log.v("myLocation runOnFistFix", "getMyLocation found");
					isSearchingPosition =false;
				}
			});
	    }
	    
	    if (!Intent.ACTION_VIEW.equals(getIntent().getAction())) {
	    	Bundle extras = getIntent().getExtras();
	    	if(extras == null || (!extras.getBoolean(GEOLOC) && !extras.getBoolean(FROMLIST)) ) {
	    	//	onSearchRequested();
	    		//moveToGeoloc();
	    		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
	    		Boolean firstUse = settings.getBoolean(FIRST_USE,true);
	    		if(firstUse){
	    			createPopup();
	    		} else {
	    			onSearchRequested();
	    		}
	    		

	    		
	    	}
	    }
	    mProgressDialog.dismiss();
	    
//		Bundle extras = getIntent().getExtras();
//		if(extras !=null && extras.getBoolean(GEOLOC)) {
//			moveToGeoloc();
//    	} 

	}
//

	
	
	
	
	
	@Override
	protected boolean isRouteDisplayed() {
		
		return false;
	}


	  private Intent createSettingsIntent(Context ctx) {
	        final Intent intent = new Intent(ctx,
					GeocampusPrefsActivity.class);
	        return intent;
//		  return null;
		  
	   }
	
  
	
	  @Override
	  protected void onDestroy() {
	      
		  myLocationOverlay.disableMyLocation();
		  
	      if (adapter != null) {
	    	  adapter.close();
	      }
	      super.onDestroy();
	  }
	 
	  
	  
	  
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
		    MenuInflater inflater = getMenuInflater();
		    inflater.inflate(R.menu.geocampusmenu, menu);
		   
//		    SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
//			String poiSelected = settings.getString(WSUtils.KEY_POI_SELECT, WSUtils.KEY_NOT_DEFINE);
//			if(poiSelected.equals(WSUtils.KEY_NOT_DEFINE)){
//				menu.findItem(R.id.mItemMaps).setEnabled(false);
//			} else {
//				menu.findItem(R.id.mItemMaps).setEnabled(true);
//			}
		    return true;
		}
		
		@Override
		public boolean onPrepareOptionsMenu(Menu menu) {
		    SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
			String poiSelected = settings.getString(WSUtils.KEY_POI_SELECT, WSUtils.KEY_NOT_DEFINE);
			if(poiSelected.equals(WSUtils.KEY_NOT_DEFINE)){
			//	menu.findItem(R.id.mItemMaps).setEnabled(false);
				menu.findItem(R.id.mItemMaps).setIcon(R.drawable.ic_menu_list);
				menu.findItem(R.id.mItemMaps).setTitle(R.string.openList);
			} else {
				//menu.findItem(R.id.mItemMaps).setEnabled(true);
				menu.findItem(R.id.mItemMaps).setIcon(R.drawable.ic_menu_map);
				menu.findItem(R.id.mItemMaps).setTitle(R.string.openMaps);
			}
		    return true;
		}
		
		

//	@Override
//	public void onCreateContextMenu(ContextMenu menu, View v,
//			ContextMenuInfo menuInfo) {
//		// TODO Auto-generated method stub
//		super.onCreateContextMenu(menu, v, menuInfo);
//		menu.setHeaderIcon(R.drawable.ic_action_settings);
//		menu.setHeaderTitle("Configuration des sources");
//		
//		
//	
//		SubMenu univ = menu.addSubMenu("Choix des universités");
//		LinearLayout layout = new LinearLayout(this);
//		TextView textView = new TextView(this);
//		textView.setText("test");
//		layout.addView(textView);
//	
//		
//		univ.setHeaderIcon(R.drawable.ic_menu_university);
//		MenuItem paris1 = univ.add("Paris 1 Panthéon-Sorbonne").setCheckable(true);
//		paris1.setTitleCondensed("test");
//		paris1.setOnMenuItemClickListener(new OnMenuItemClickListener() {
//			
//			public boolean onMenuItemClick(MenuItem menuitem) {
//				// TODO Auto-generated method stub
//				return true;
//			}
//		});
//		univ.add("paris2").setCheckable(true);
//		univ.add("paris3").setCheckable(true);
//		univ.add("paris4").setCheckable(true);
//		
//		SubMenu crous = menu.addSubMenu("Choix des CROUS");
//		SubMenu biblio = menu.addSubMenu("Choix des bibliothèques");
//		
//		
//	}
	
	
	
	
//	private void moveToGeoloc(){
//    //	double latitude = 48.831885002509544;
//    //	double longitude= 2.2857677936553955;
//    	
//    	
//    
//
//    	
//		if(curLocation != null ){
//
//   
//    	//GeoPoint point = new GeoPoint((int)(latitude*1e6),(int)(longitude*1e6));
//    	GeoPoint point = new GeoPoint( 	(int)(curLocation.getLatitude()*1e6), (int)(curLocation.getLongitude()*1e6));
//        MarqueurViewers marqueurViewers = (MarqueurViewers) findViewById(R.id.map_location_viewer);
//       
//        Bitmap bMap = BitmapFactory.decodeResource(getResources(), R.drawable.geoloc);
//        Marqueur pointBleu = new Marqueur(bMap, "", curLocation.getLatitude(), curLocation.getLongitude());
//        marqueurViewers.getMapMarqueur().add(pointBleu);
//       // marqueurViewers.getMapView().getController().zoomInFixing(point.getLatitudeE6(), point.getLongitudeE6());
//
//
//        marqueurViewers.getMapView().getController().animateTo(point);
//        
//      
//     //   marqueurViewers.getMapView().getController().setZoom(MarqueurViewers.ZOOM_POI);
//		} else {
//			Toast.makeText(getApplicationContext(), "Géolocalisation indisponible", Toast.LENGTH_SHORT).show();
//			if (lManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//	        	 lManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,20000, 1, this);
//	        	 
//	         } else {
//	               Toast.makeText(getApplicationContext(), "Activez le gps pour plus de précision", Toast.LENGTH_LONG);
//	               lManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,20000, 1, this);
//	         }
//		}
//	}
		
		
		
		
	
	private void handleSearch(Intent intent){
		  Log.v(TAG, "ACTION_SEARCH");
		    
			// handles a search query
	        Intent resultIntent = new Intent(this, GeocampusResult.class);
	        String query = intent.getStringExtra(SearchManager.QUERY);
	        resultIntent.putExtra(SearchManager.QUERY, query);
	        resultIntent.setData(intent.getData());
	        startActivity(resultIntent);
	        finish();
	}
	
	private String handleView(Intent intent){
		String title =""; 
		SharedPreferences settings = this.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		String cat_selected = settings.getString(WSUtils.KEY_CAT_SELECT, WSUtils.KEY_NOT_DEFINE);
		String poi_selected = settings.getString(WSUtils.KEY_POI_SELECT, WSUtils.KEY_NOT_DEFINE);
		
		if (Intent.ACTION_VIEW.equals(intent.getAction())) {
			SharedPreferences.Editor editor = settings.edit();
			Log.v(TAG, "ACTION_VIEW");
			String value = intent.getData().getFragment();
			Log.v(TAG, value);
			// cas categorie
			if(value.indexOf("_")==-1){
				value = value.substring(1);
				adapter.open();
				title = adapter.getCategoryName(value);
				adapter.close();
				editor.remove(WSUtils.KEY_POI_SELECT);
				editor.putString(WSUtils.KEY_CAT_SELECT, value);
				editor.commit();
				Log.v(TAG, "KEY_POI_SELECT removed KEY_CAT_SELECT add : " +value);
				

			} else {// cas poi
				value = value.substring(2);
				editor.putString(WSUtils.KEY_POI_SELECT, value);
				editor.commit();
				Log.v(TAG, "KEY_POI_SELECT add : " +value);
				adapter.open();
				title = adapter.getPoiName(value);
				adapter.close();
				
				
			}
			

		} else if (!poi_selected.equals(WSUtils.KEY_NOT_DEFINE)) {
			adapter.open();
			title = adapter.getPoiName(poi_selected);
			adapter.close();
		} else if (!cat_selected.equals(WSUtils.KEY_NOT_DEFINE)) {
			adapter.open();
			title = adapter.getCategoryName(cat_selected);
			adapter.close();
		}
		return title;
	}
	
	
	@Override
	protected void finalize() throws Throwable {
		adapter.close();
		super.finalize();
	}


	@Override
	public void onBackPressed() {
		Intent mainIntent = new Intent(this, MainActivity.class);
		startActivity(mainIntent);
		finish();	
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
		switch (item.getItemId()) {
	    case R.id.mItemHome:
	    		Intent mainIntent = new Intent(this, MainActivity.class);
				startActivity(mainIntent);
	    		finish();	
	        return true;
	    case R.id.mItemInfo:
	    	
	    /*	String lien = (String)settings.getString(WSUtils.KEY_HELP,WSUtils.KEY_NOT_DEFINE);
	    //	String lien = "http://maps.google.fr/?ll=47.974295,1.41449&spn=0.246849,0.676346&t=m&z=11&vpsrc=6";
	    	Bundle bundle = new Bundle();
			bundle.putString(WSUtils.KEY_URL, lien);
			bundle.putString(WSUtils.KEY_HELP, lien);
			Intent intent = new Intent(this, UnivMobileWebView.class);
			intent.putExtras(bundle);
			startActivity(intent);*/
	    	
	    	createPopup();
	        return true;
		case R.id.mItemMaps:

			String poiSelected = settings.getString(WSUtils.KEY_POI_SELECT,
					WSUtils.KEY_NOT_DEFINE);
			if (poiSelected.equals(WSUtils.KEY_NOT_DEFINE)) {
				// open liste
				 Intent resultIntent = new Intent(this, GeocampusResult.class);
			
				 String cat_selected = settings.getString(WSUtils.KEY_CAT_SELECT, "1");

				 resultIntent.putExtra(WSUtils.KEY_CAT_SELECT, cat_selected);
				 startActivity(resultIntent);
			     finish();
				
			} else {
				// open maps
				Intent i = new Intent(Intent.ACTION_VIEW);
				String urltmp = getMapsUrl();
				Log.v(TAG, "url de redirection : " + urltmp);
				if (!"".equals(urltmp)) {
					Uri u = Uri.parse(urltmp);
					i.setData(u);
					startActivity(i);
				}
			}

			return true;
		default:
	        return super.onOptionsItemSelected(item);
	    }
	
	}


	private String getMapsUrl(){
		SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
		String poi = settings.getString(WSUtils.KEY_POI_SELECT, WSUtils.KEY_NOT_DEFINE);
		String urltmp ="";
		if (!poi.equals(WSUtils.KEY_NOT_DEFINE)) {
			adapter.open();
			Marqueur m = adapter.getPoi(poi,this);
			adapter.close();
			if(m!=null && m.getPoint()!=null){
				Float lat = Float.valueOf(m.getPoint().getLatitudeE6()) / 1000000f;
				Float lng = Float.valueOf(m.getPoint().getLongitudeE6()) / 1000000f;
				urltmp= "http://maps.google.com/maps?f=q&q="+lat + ","+lng+"%20%28"+m.getTitre()+"%29";
			} else {
				urltmp= "http://maps.google.com/";
			}
			
			
		}
		
		return urltmp;
	}

	private void moveToGeoloc() {
		
		if(!isSearchingPosition){
			
			isSearchingPosition =true;
			MarqueurViewers marqueurViewers = (MarqueurViewers) findViewById(R.id.map_location_viewer);
			if(myLocationOverlay!=null){
				
				marqueurViewers.getMapView().getOverlays().remove(myLocationOverlay);
				myLocationOverlay.getLastFix();
				marqueurViewers.getMapView().getOverlays().add(myLocationOverlay);

				
			} else {
				myLocationOverlay = new MyLocationOverlay(this,
						marqueurViewers.getMapView());
				myLocationOverlay.enableMyLocation();
				marqueurViewers.getMapView().getOverlays().add(myLocationOverlay);
			}
			
		
		
			//myLocationOverlay.enableCompass();
		//	myLocationOverlay.
//			if(myLocationOverlay.getLastFix() !=null){
//				
//				int lat = (int)myLocationOverlay.getLastFix().getLatitude()* 1000000;
//				int lng = (int)myLocationOverlay.getLastFix().getLongitude() * 1000000;
//				GeoPoint lastFix = new GeoPoint(lat, lng);
//				marqueurViewers.getMapView().getController().animateTo(lastFix);
//				marqueurViewers.getMapView().getController().setZoom(16);
//				
//			}
						
			myLocationOverlay.runOnFirstFix(new Runnable() {
				public void run() {
					MarqueurViewers marqueurViewers = (MarqueurViewers) findViewById(R.id.map_location_viewer);
					marqueurViewers.getMapView().getController().animateTo(
							myLocationOverlay.getMyLocation());
					marqueurViewers.getMapView().getController().setZoom(16);
			//		Toast.makeText(mContext "ok", Toast.LENGTH_SHORT).show();
					Log.v("myLocation runOnFistFix", "finish");
					isSearchingPosition =false;
				}
			});
		}
	}

	
	private void createPopup(){
		
		
		  popupHelp = new Dialog(GeocampusActivity.this);
		  popupHelp.setContentView(R.layout.popupgeocampus);
		  popupHelp.setTitle("Bienvenue dans Géocampus");
		 
		  Button button =(Button)popupHelp.findViewById(R.id.button_ok_geocampus_help);
		  button.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				SharedPreferences settings = getSharedPreferences(WSUtils.PREFS_NAME, 0);
				Boolean firstUse = settings.getBoolean(FIRST_USE,true);
				SharedPreferences.Editor editor = settings.edit();
            	editor.putBoolean(FIRST_USE, false);
            	editor.commit();
				popupHelp.dismiss();
				
				if(firstUse){
					onSearchRequested();
				}
			}
		  });
		  
		  popupHelp.show();
	     }

	
	
	
}
