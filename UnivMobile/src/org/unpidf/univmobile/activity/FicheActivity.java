package org.unpidf.univmobile.activity;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.geocampus.MarqueurDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FicheActivity extends Activity {

//	private static final String TAG = "Univmobile - Fiche";
	LinearLayout mFicheLayout;
	MarqueurDetails mMarqueur = null;
	ClipboardManager mClipboard;
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		mClipboard = (ClipboardManager)
		        getSystemService(Context.CLIPBOARD_SERVICE);
		Bundle bundle = getIntent().getExtras();
		mMarqueur = (MarqueurDetails) bundle.get("marqueur");
		setContentView(R.layout.fichegeocampus);

		mFicheLayout = (LinearLayout) findViewById(R.id.fiche);
		mFicheLayout.setBackgroundColor(getResources().getColor(R.color.white));

		TextView titre = (TextView) findViewById(R.id.mTextTitle);
		titre.setText(mMarqueur.getUnivTitle());
		titre.setTextColor(getResources().getColor(R.color.white));

		TextView h1 = (TextView) findViewById(R.id.mH1);
		h1.setText(mMarqueur.getTitre());
		TextView h2 = (TextView) findViewById(R.id.mH2);
		h2.setText(mMarqueur.getSousTitre());

		LinearLayout emplacementLayout = (LinearLayout) findViewById(R.id.ficheEmplacement);
		if (mMarqueur.getEmplacement() != null
				&& !"".equals(mMarqueur.getEmplacement())) {
			TextView emplacement = (TextView) findViewById(R.id.mEmplacementLib);
			emplacement.setText(mMarqueur.getEmplacement());
		} else {

			emplacementLayout.setVisibility(View.GONE);
		}
		emplacementLayout
				.setOnLongClickListener(new View.OnLongClickListener() {

					public boolean onLongClick(View v) {
						
						if (mMarqueur != null) {
							mClipboard.setText(mMarqueur.getEmplacement());
							Toast toast = Toast.makeText(
									getApplicationContext(),
									"\"" + mMarqueur.getEmplacement()
											+ "\" copié", Toast.LENGTH_SHORT);
							toast.show();
							
						}
						return false;
					}
				});

		LinearLayout adressLayout = (LinearLayout) findViewById(R.id.ficheAddress);
		if (mMarqueur.getAddress() != null
				&& !"".equals(mMarqueur.getAddress())) {
			TextView address = (TextView) findViewById(R.id.mAddressLib);
			address.setText(mMarqueur.getAddress());
		} else {
			if (mMarqueur.getAddress2() != null
					&& !"".equals(mMarqueur.getAddress2())) {
				TextView address = (TextView) findViewById(R.id.mAddressLib);
				mMarqueur.setAddress(mMarqueur.getAddress2());
				address.setText(mMarqueur.getAddress2());
			} else {

				adressLayout.setVisibility(View.GONE);
			}
		}

		adressLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					
					mClipboard.setText(mMarqueur.getAddress());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getAddress() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
					
				}
				return false;
			}
		});

		LinearLayout hoursLayout = (LinearLayout) findViewById(R.id.ficheOpenHours);
		if (mMarqueur.getOpenHours() != null
				&& !"".equals(mMarqueur.getOpenHours())) {
			TextView hours = (TextView) findViewById(R.id.mHoursLib);
			hours.setText(mMarqueur.getOpenHours());
		} else {

			hoursLayout.setVisibility(View.GONE);
		}
		hoursLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					mClipboard.setText(mMarqueur.getOpenHours());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getOpenHours() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
				}
				return false;
			}
		});

		LinearLayout phoneLayout = (LinearLayout) findViewById(R.id.ficheTel);
		if (mMarqueur.getPhone() != null && !"".equals(mMarqueur.getPhone())) {
			TextView phone = (TextView) findViewById(R.id.mTelLib);
			phone.setText(mMarqueur.getPhone());
		} else {

			phoneLayout.setVisibility(View.GONE);
		}
		phoneLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					mClipboard.setText(mMarqueur.getPhone());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getPhone() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
				}
				return false;
			}
		});

		
		LinearLayout emailLayout = (LinearLayout) findViewById(R.id.ficheEmail);
		if (mMarqueur.getEmail() != null && !"".equals(mMarqueur.getEmail())) {
			TextView email = (TextView) findViewById(R.id.mEmailLib);
			// email.setText(mMarqueur.getEmail());
			SpannableString content = new SpannableString(mMarqueur.getEmail());
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			email.setText(content);
			email.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					Intent callIntent = new Intent(Intent.ACTION_VIEW);
					callIntent.setData(Uri.parse("mailto:"
							+ mMarqueur.getEmail()));
					startActivity(callIntent);

				}
			});

		} else {
		
			emailLayout.setVisibility(View.GONE);
		}
		emailLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					mClipboard.setText(mMarqueur.getEmail());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getEmail() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
					
					
				}
				return false;
			}
		});

		LinearLayout itineraryLayout = (LinearLayout) findViewById(R.id.ficheAcces);
		if (mMarqueur.getItinerary() != null
				&& !"".equals(mMarqueur.getItinerary())) {
			TextView itinerary = (TextView) findViewById(R.id.mAccesLib);
			itinerary.setText(mMarqueur.getItinerary());
		} else {
			
			itineraryLayout.setVisibility(View.GONE);
		}
		
		itineraryLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					mClipboard.setText(mMarqueur.getItinerary());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getItinerary() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
									
					
				}
				return false;
			}
		});

		LinearLayout urlLayout = (LinearLayout) findViewById(R.id.ficheUrl);

		if (mMarqueur.getUrl() != null && !"".equals(mMarqueur.getUrl())) {
			TextView url = (TextView) findViewById(R.id.mUrlLib);
			SpannableString content = new SpannableString(mMarqueur.getUrl());
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			url.setText(content);
			
			url.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					Intent callIntent = new Intent(Intent.ACTION_VIEW);
					callIntent.setData(Uri.parse(mMarqueur.getUrl()));
					startActivity(callIntent);

				}
			});
			
			
		} else {
			urlLayout.setVisibility(View.GONE);
		}
		
		urlLayout.setOnLongClickListener(new View.OnLongClickListener() {

			public boolean onLongClick(View v) {

				if (mMarqueur != null) {
					mClipboard.setText(mMarqueur.getUrl());
					Toast toast = Toast.makeText(getApplicationContext(), "\""
							+ mMarqueur.getUrl() + "\" copié",
							Toast.LENGTH_SHORT);
					toast.show();
				}
				return false;
			}
		});

		Button button_call = (Button) findViewById(R.id.mCall);
		button_call.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:" + mMarqueur.getPhone()));
				startActivity(callIntent);

			}
		});

		Button button_maps = (Button) findViewById(R.id.mMap);
		button_maps.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Intent callIntent = new Intent(Intent.ACTION_VIEW);
				callIntent.setData(Uri.parse("http://maps.google.fr/maps?q="
						+ mMarqueur.getAddress()));
				startActivity(callIntent);

			}
		});

		Button button_ok = (Button) findViewById(R.id.mButtonOkFiche);
		button_ok.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				finish();

			}
		});
		// titre.setBackgroundColor(getResources().getColor(R.color.white));
		// titlelayout.addView(titre);
	}

}
