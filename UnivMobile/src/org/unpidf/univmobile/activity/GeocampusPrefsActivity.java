package org.unpidf.univmobile.activity;

import java.util.ArrayList;
import java.util.Date;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.UniversiteDTO;
import org.unpidf.univmobile.util.UnivmobileDBAdapter;
import org.unpidf.univmobile.util.WSUtils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.widget.Toast;


public class GeocampusPrefsActivity extends PreferenceActivity {
	private Context mContext;
	UnivmobileDBAdapter adapter;
	PreferenceScreen pref_univ;
	PreferenceScreen pref_crous;
	private ProgressDialog mProgressDialog;
	private boolean hasChanged = false;
	private static final String TAG = "Univmobile - GeocampusPrefsActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		adapter = new UnivmobileDBAdapter(getApplicationContext());
	
		addPreferencesFromResource(R.xml.geocampus_prefs);
		mContext = this;
		 
		
		
		pref_univ = (PreferenceScreen)findPreference("pref_univ");
		pref_crous = (PreferenceScreen)findPreference("pref_crous");
		
		adapter.open();
	
		ArrayList<UniversiteDTO> listUniv = adapter.getUniversityList();
		ArrayList<UniversiteDTO> listCrous = adapter.getCrousList();
	
		
		for (UniversiteDTO universiteDTO : listUniv) {
			CheckBoxPreference univ = createCheckBoxPref(universiteDTO);
			pref_univ.addPreference(univ);
		}
		for (UniversiteDTO universiteDTO : listCrous) {
			CheckBoxPreference univ = createCheckBoxPref(universiteDTO);
			pref_crous.addPreference(univ);
		}
		
		
		pref_univ.setSummary(adapter.getUniversitySummary());
		pref_crous.setSummary(adapter.getCrousSummary());
		if(listCrous.size()==0){
			pref_crous.setSelectable(false);
			pref_crous.setSummary("Données non disponibles");
		}
		
		adapter.close();
		
		Preference update=findPreference("update");
		update.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
								
				chargerPreferences();
				
			return true;
			}

			
		});
		
		
		
//		Preference p=findPreference("save");
//		p.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//			public boolean onPreferenceClick(Preference preference) {
//				
//			//	SharedPreferences prefs = getSharedPreferences();
//				
//				Map<String, ?>  m = prefs.getAll();
//				boolean isChecked = false;
//				for (Iterator iterator = m.values().iterator(); iterator
//						.hasNext();) {
//					try {
//					Boolean s = (Boolean)iterator.next();
//					if(s){
//						isChecked = true;
//						break;
//					}
//					
//					} catch (Exception e) {
//						// TODO: handle exception
//						Log.v("test","test classCast"+e.getMessage());
//					}
//				}
//				
//			
//				if(!isChecked){
////					mContext.
//					//TODO afficher boite de dialogue
//					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
//					builder.setMessage(getResources().getString(R.string.alert_data_geocampus))
//					       .setCancelable(false)
//					       .setPositiveButton(getResources().getString(R.string.bouton_ok), new DialogInterface.OnClickListener() {
//					           public void onClick(DialogInterface dialog, int id) {
//					        	  
//					           }
//					       });
//					builder.show();
//					
//					
//				} else {
//					Intent newIntent = new Intent(mContext, GeocampusActivity.class);
//					startActivity(newIntent);
//					 finish();
//				}
//				return true;
//			}
//		});
		
		
	}

	/**
	 * @param universiteDTO
	 * @return
	 */
	private CheckBoxPreference createCheckBoxPref(UniversiteDTO universiteDTO) {
		CheckBoxPreference univ = new CheckBoxPreference(this);
		univ.setChecked(universiteDTO.isSelected());
		univ.setKey(universiteDTO.getId());
		univ.setTitle(universiteDTO.getTitle());
		univ.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			
			public boolean onPreferenceClick(Preference preference) {
				CheckBoxPreference p = (CheckBoxPreference)preference;
				adapter.open();
				int nbSources = adapter.getNbUnivSelected();
				if(p.isChecked() && nbSources>=WSUtils.NB_SOURCES_MAX){
					Toast.makeText(mContext, "Vous ne pouvez pas sélectionner plus de " + WSUtils.NB_SOURCES_MAX +" sources" , Toast.LENGTH_LONG).show();
					p.setChecked(!p.isChecked());
					
				} else {
					adapter.updateUnivPref(preference.getKey(), p.isChecked());
				}
				adapter.close();
				onContentChanged();
				return false;
			}
		});
		return univ;
	}
	
	  @Override
	    public void onBackPressed() {
		  	if (hasChanged){
		  			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		  			builder.setMessage(getResources().getString(R.string.confirm_reload))
		  		       .setCancelable(false)
		  		       .setPositiveButton(getResources().getString(R.string.confirm_yes), new DialogInterface.OnClickListener() {
		  		           public void onClick(DialogInterface dialog, int id) {
		  		        	 chargerPreferences();
		  		           }
		  		       })
		  		       .setNegativeButton(getResources().getString(R.string.confirm_no), new DialogInterface.OnClickListener() {
		  		           public void onClick(DialogInterface dialog, int id) {
		  		                dialog.cancel();
		  		              Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
		  		            geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
		  		              startActivity(geocampIntent);
		  		              finish();
		  		           }
		  		       });
		  		builder.show();
		  		
		  	}
		  	else{
		  	 Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
	    	 geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
	    	 startActivity(geocampIntent);
	    	 finish();
		  	}
	    	 
	    }
	    
	    @Override
	    public boolean onSearchRequested() {
	      Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
	      geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
	   	  startActivity(geocampIntent);
	   	  finish();
	   	  return true;
	    }
	    
	    @Override
		  protected void onDestroy() {
		      
		      if (adapter != null) {
		    	  adapter.close();
		      }
		      super.onDestroy();
		  }
	    
	    @Override
	    public void onContentChanged() {
	    	
	    	 if (adapter != null) {
	    		 //Toast.makeText(getApplicationContext(), "test onContentChanged", Toast.LENGTH_SHORT).show();
		    	adapter.open();
			//	PreferenceScreen ps = getPreferenceScreen();
			//	Preference p = ps.findPreference("pref_univ");;
				pref_univ.setSummary(adapter.getUniversitySummary());
				pref_crous.setSummary(adapter.getCrousSummary());
				adapter.close();
				hasChanged = true;
	    	 }
	    	super.onContentChanged();
	    }

	    @Override
	    public SharedPreferences getSharedPreferences(String name, int mode) {
			return getSharedPreferences();
		}
	    
	    private SharedPreferences getSharedPreferences() {
			return super.getSharedPreferences(WSUtils.PREFS_NAME, MODE_PRIVATE);
		} 
		
	    private void chargerPreferences() {
			
	    	adapter.open();
			if(adapter.getNbUnivSelected()==0){
				Toast.makeText(mContext, "Veuillez sélectionner au moins une source" , Toast.LENGTH_LONG).show();
			} else {
				
				
			 
						
			
			// gestion de la mise a jour des données en fonction du temps
			long period_max= WSUtils.TIMEOUT_30_DAYS;
			if(getSharedPreferences().contains(WSUtils.KEY_PREFS_SNOOZE)) {
				period_max = WSUtils.TIMEOUT_15_DAYS;
			} 
			
			// maj tout les 30 jours
			long before = getSharedPreferences().getLong(WSUtils.KEY_PREFS_LAST_DATA_REFRESH, 0l);
				Date now = new Date();
				long timeout = now.getTime() - before ;
				if(timeout> period_max && before!=0){
					Log.v(TAG, "timeout = " +timeout +" > period_max = " +period_max);
					
					AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		  			builder.setMessage(getResources().getString(R.string.confirm_timeout))
		  		       .setCancelable(false)
		  		       .setPositiveButton(getResources().getString(R.string.confirm_yes), new DialogInterface.OnClickListener() {
		  		           public void onClick(DialogInterface dialog, int id) {
			  		        	 Log.v(TAG, "suppression des données locales");
			 					// remplace le temps before par le temps t
			 					Editor edit = getSharedPreferences().edit();
			 					Date now = new Date();
			 					edit.putLong(WSUtils.KEY_PREFS_LAST_DATA_REFRESH, now.getTime());
			 					//  suppression les json
			 					ArrayList<UniversiteDTO> listUniv = adapter.getUniversityList();
			 					listUniv.addAll(adapter.getCrousList());
			 					for (UniversiteDTO universiteDTO : listUniv) {
			 						String nameJson = universiteDTO.getId() + "_"+ WSUtils.FILE_NAME_POIS;
			 						WSUtils.remove(nameJson, mContext);
			 					}
			 					edit.remove(WSUtils.KEY_PREFS_SNOOZE);
			 					edit.commit();
			 					chargerPreferenceProcess();
		  		           }
		  		       })
		  		       .setNegativeButton(getResources().getString(R.string.confirm_snooze), new DialogInterface.OnClickListener() {
		  		           public void onClick(DialogInterface dialog, int id) {
		  		                dialog.cancel();
			 					Editor edit = getSharedPreferences().edit();
			 					Date now = new Date();
			 					edit.putLong(WSUtils.KEY_PREFS_LAST_DATA_REFRESH, now.getTime());
			 					edit.putBoolean(WSUtils.KEY_PREFS_SNOOZE, true);
			 					edit.commit();
			 					chargerPreferenceProcess();
		  		                Log.v(TAG, "snooze");
		  		           }
		  		       });
		  			builder.show();
					
					
					
				} else {
					Log.v(TAG, "timeout = " +timeout +" < period_max = " +period_max +" (or before = "+before +"->0)");
					// cas première connexion
					if(before==0){
						Editor edit = getSharedPreferences().edit();
	 					edit.putLong(WSUtils.KEY_PREFS_LAST_DATA_REFRESH, now.getTime());
	 					edit.commit();
					}
					chargerPreferenceProcess();
				}		
			}
		}

		private void chargerPreferenceProcess() {
			mProgressDialog = ProgressDialog.show(GeocampusPrefsActivity.this,
			        "", "Chargement des données, cela peut prendre quelques minutes", true);
			new Thread() {
			    @Override 
			    public void run() {
			    	ArrayList<UniversiteDTO> listTotal = adapter.getUniversityList();
					ArrayList<UniversiteDTO> listCrous = adapter.getCrousList();
					listTotal.addAll(listCrous);
					adapter.dropDataBaseOnlyPOIS();
				
					for (UniversiteDTO universiteDTO : listTotal) {
						if(universiteDTO.isSelected()){
							String urlRegion =WSUtils.getRegionUrl(mContext);
							String url = WSUtils.constructUrl(urlRegion, universiteDTO.getId(), WSUtils.KEY_LANG);
							WSUtils.readAndSaveJsonInDb(url,universiteDTO.getId() + "_"+WSUtils.FILE_NAME_POIS,mContext, universiteDTO.getId(), false);
				
						}
						
					}
					adapter.close();
					mProgressDialog.dismiss();
					Intent geocampIntent = new Intent(getApplicationContext(), GeocampusActivity.class);
				      geocampIntent.putExtra(GeocampusActivity.FROMLIST, true);
				   	  startActivity(geocampIntent);
					finish();	
			    }
			}.start();
		}
}
