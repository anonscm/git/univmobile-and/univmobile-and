package org.unpidf.univmobile.geocampus;

import java.util.Iterator;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.activity.FicheActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.view.GestureDetector;
import android.view.MotionEvent;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class MarqueurOverlay extends Overlay {

	private Paint mInnerPaint, mBorderPaint, mTitrePaint, mSousTitrePaint;
	
	private MarqueurViewers mMarqueurViewers;
	
	private Marqueur mSelectedMarqueur;  
	
	private GestureDetector mGestureDetector = null;
	float scale;
	public MarqueurOverlay(MarqueurViewers mMarqueurViewers) {
		
		this.mMarqueurViewers = mMarqueurViewers;
		UnivGestureDetector detector = new UnivGestureDetector(mMarqueurViewers.getMapView());
		mGestureDetector = new GestureDetector(mMarqueurViewers.getContext(),detector);

		
	}
	@Override
	public void draw(Canvas canvas, MapView	mapView, boolean shadow) {
		
		scale= mMarqueurViewers.getMapView().getContext().getResources().getDisplayMetrics().density;
	   		drawMapMarqueurs(canvas, mapView, shadow);
	   		drawInfoWindow(canvas, mapView, shadow);
	   	
	   		
	 }
	
	
	
	private void drawInfoWindow(Canvas canvas, MapView	mapView, boolean shadow) {
    	
    	if ( mSelectedMarqueur != null) {
    		if ( shadow) {
    			//  Skip painting a shadow
    		} else {
				//  First we need to determine the screen coordinates of the selected MapLocation
				Point selDestinationOffset = new Point();
				mapView.getProjection().toPixels(mSelectedMarqueur.getPoint(), selDestinationOffset);
				
				
				if(!"".equals(mSelectedMarqueur.getTitre())){
				
					Bitmap arrow = BitmapFactory.decodeResource(mMarqueurViewers.getResources(), R.drawable.arrow);
				// recuperation du texte a afficher
				String titre = mSelectedMarqueur.getTitre();
				String soustitre = mSelectedMarqueur.getSousTitre();
				int taille_max=32;
				int INFO_WINDOW_WIDTH = canvas.getWidth() - canvas.getWidth() / 18;
				if(titre.length()>=taille_max){
					titre = titre.substring(0, taille_max -2) + "...";
				}
				if(soustitre.length()>=taille_max+10){
					soustitre= soustitre.substring(0, taille_max +10-2) + "...";
				}
//				titre = titre.substring(0, taille_max -2) + "...";
//				int INFO_WINDOW_WIDTH = (int) (Double.valueOf(canvas.getWidth() )/Double.valueOf(taille_max+1))*(titre.length()+3)  ;
//				if(titre!=null && soustitre!=null){
//					if(titre.length()>soustitre.length()-10){
//						if(titre.length()>=taille_max){
//							titre = titre.substring(0, taille_max -2) + "...";
//							INFO_WINDOW_WIDTH = canvas.getWidth() - canvas.getWidth() / 18;
//						} 
//					} else {
//						INFO_WINDOW_WIDTH = (int) ((Double.valueOf(canvas.getWidth() )/Double.valueOf(taille_max+1))*(soustitre.length()+1)*(19f/22f));
//						if (soustitre.length()>taille_max +10){
//							soustitre= soustitre.substring(0, taille_max +10-2) + "...";
//							INFO_WINDOW_WIDTH = canvas.getWidth() - canvas.getWidth() / 18;
//						}
//					}
//				}
				
				
				
				
		    	//  Setup the info window
				
				
				int INFO_WINDOW_HEIGHT = (int) (50*scale);
				
				
				
				RectF infoWindowRect = new RectF(0,0,INFO_WINDOW_WIDTH,INFO_WINDOW_HEIGHT);				
				int infoWindowOffsetX = selDestinationOffset.x-INFO_WINDOW_WIDTH/2;
				//int infoWindowOffsetY = selDestinationOffset.y-INFO_WINDOW_HEIGHT-mSelectedMarqueur.getImageMarqueur().getHeight();
				int infoWindowOffsetY = selDestinationOffset.y-INFO_WINDOW_HEIGHT-mSelectedMarqueur.getImageMarqueur().getHeight();
				infoWindowRect.offset(infoWindowOffsetX,infoWindowOffsetY);
				
				//  Drawing the inner info window
				canvas.drawRoundRect(infoWindowRect, 5, 5, getInnerPaint());
				
				//  Drawing the border for info window
				canvas.drawRoundRect(infoWindowRect, 5, 5, getBorderPaint());
				//  Draw the MapLocation's name
				
				int TEXT_OFFSET_X = 10;
				int TEXT_OFFSET_Y_title =  (int) (20*scale);
			
				int TEXT_OFFSET_Y_subtitle =(int) (40*scale);
				
				canvas.drawText(titre,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y_title,getTitrePaint());
				
				canvas.drawText(soustitre,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y_subtitle,getSousTitrePaint());
				
				 //Bitmap arrow = BitmapFactory.decodeResource(mMarqueurViewers.getResources(), R.drawable.arrow);
				
				 mSelectedMarqueur.setImageX(infoWindowOffsetX + INFO_WINDOW_WIDTH -arrow.getWidth());
				 mSelectedMarqueur.setImageY(infoWindowOffsetY + INFO_WINDOW_HEIGHT/2 - arrow.getHeight()/2);
				 canvas.drawBitmap(arrow,mSelectedMarqueur.getImageX(),mSelectedMarqueur.getImageY(),getSousTitrePaint());
				
				
				}
			}
    	}
    }
	
	
   private void drawMapMarqueurs(Canvas canvas, MapView	mapView, boolean shadow) {
	  if(mMarqueurViewers.getMapMarqueur() != null){
		  Iterator<Marqueur> iterator = mMarqueurViewers.getMapMarqueur().iterator();
			Point screenCoords = new Point();
	    	while(iterator.hasNext()) {	   
	    		Marqueur marqueur = iterator.next();
	    		if(marqueur!=null){
		    		mapView.getProjection().toPixels(marqueur.getPoint(), screenCoords);
					
			    	if (shadow) {
			    		// Offset the shadow in the y axis as it is angled, so the base is at x=0
			    	//	canvas.drawBitmap(marqueur.getImageMarqueur(), screenCoords.x, screenCoords.y - marqueur.getImageMarqueur().getHeight(),null);
			    	} else {
			    		if(canvas!=null && marqueur!=null && marqueur.getImageMarqueur()!=null){
			    			canvas.drawBitmap(
			    					marqueur.getImageMarqueur(), 
			    					screenCoords.x - marqueur.getImageMarqueur().getWidth()/2, 
			    					screenCoords.y - marqueur.getImageMarqueur().getHeight(),
			    					null);
			    		}
			    	}
	    		}
	    	}
	  }
		
	
    }
	
	@Override
	public boolean onTap(GeoPoint p, MapView mapView)  {
		
		
		/**
		 * Track the popup display
		 */
		boolean isRemovePriorPopup = mSelectedMarqueur != null;  

		/**
		 * Test whether a new popup should display
		 */
		mSelectedMarqueur = getHitMarqueur(mapView,p);
		if ( isRemovePriorPopup || mSelectedMarqueur != null) {
			mapView.invalidate();
		}		
		
		/**
		 *   Return true if we handled this onTap()
		 */
		return mSelectedMarqueur != null;
	}
   
   
   /**
    * Test whether an information balloon should be displayed or a prior balloon hidden.
    */
   private Marqueur getHitMarqueur(MapView	mapView, GeoPoint	tapPoint) {
   	
   	/**
   	 *   Tracking the clicks on MapLocation
   	 */
	   Marqueur hitMarqueur = null;
		
   	RectF hitTestRecr = new RectF();
	Point tapScreenCoords = new Point();
	Point marqueurCoords = new Point();
   	Iterator<Marqueur> iterator = mMarqueurViewers.getMapMarqueur().iterator();
   	while(iterator.hasNext()) {
   		Marqueur marqueurTest = iterator.next();
   		
   		/**
   		 * This is used to translate the map's lat/long coordinates to screen's coordinates
   		 */
   		mapView.getProjection().toPixels(marqueurTest.getPoint(), marqueurCoords);
   		mapView.getProjection().toPixels(tapPoint, tapScreenCoords);
	    	// Create a testing Rectangle with the size and coordinates of our icon
	    	// Set the testing Rectangle with the size and coordinates of our on screen icon
   		if(marqueurTest.getImageMarqueur()!=null){
	   		
   			
   			if(mSelectedMarqueur!=null){
	   			// test si click sur la punaise du POI 
				Bitmap arrow = BitmapFactory.decodeResource(mMarqueurViewers.getResources(), R.drawable.arrow);
				hitTestRecr.set(marqueurTest.getImageX(),marqueurTest.getImageY(),marqueurTest.getImageX()+arrow.getWidth(),marqueurTest.getImageY()+arrow.getHeight());
				if (hitTestRecr.contains(tapScreenCoords.x,tapScreenCoords.y)) {
//	   						Toast toast = Toast.makeText(mapView.getContext(), "lien vers fiche", Toast.LENGTH_SHORT);
//	   						toast.show();
	   						hitMarqueur = mSelectedMarqueur;
	   						createViewFicheGeocampus(mSelectedMarqueur, mapView);
	   						break;
	   			}
   			}
   			
   			//hitTestRecr = new RectF();
   			hitTestRecr.set(-marqueurTest.getImageMarqueur().getWidth()/2,-marqueurTest.getImageMarqueur().getHeight(),marqueurTest.getImageMarqueur().getWidth()/2,0);
   			
	   		hitTestRecr.offset(marqueurCoords.x,marqueurCoords.y);
	   		if (hitTestRecr.contains(tapScreenCoords.x,tapScreenCoords.y)) {
	   			
	   			hitMarqueur = marqueurTest;
	   			
	   			mapView.getController().animateTo(marqueurTest.getPoint());
	   			break;
	   		}
   		}
   	}
   	
   	
   	
   	
   	//  Finally clear the new MouseSelection as its process finished
   	tapPoint = null;
   	
   	return hitMarqueur; 
   }
   
	
    public Paint getInnerPaint() {
        if ( mInnerPaint == null) {
        	mInnerPaint = new Paint();
        	mInnerPaint.setARGB(225, 75, 75, 75); //gray
        	
        	mInnerPaint.setAntiAlias(true);
        }
        return mInnerPaint;
    }

    public Paint getBorderPaint() {
        if ( mBorderPaint == null) {
        	mBorderPaint = new Paint();
        	//mBorderPaint.setARGB(255, 255, 255, 255);
          	mInnerPaint.setARGB(225, 75, 75, 75); //gray
        	mBorderPaint.setAntiAlias(true);
        	mBorderPaint.setStyle(Style.STROKE);
        	mBorderPaint.setStrokeWidth(2);
        }
        return mBorderPaint;
    }

    public Paint getTitrePaint() {
        if ( mTitrePaint == null) {
        	mTitrePaint = new Paint();
        	mTitrePaint.setARGB(255, 255, 255, 255);
        	mTitrePaint.setAntiAlias(true);
        	
        	mTitrePaint.setTextSize(15*scale);
        	
        	
        	Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);
        	mTitrePaint.setTypeface(tf);
        	}
        return mTitrePaint;
    }
    
    public Paint getSousTitrePaint() {
        if ( mSousTitrePaint == null) {
        	mSousTitrePaint = new Paint();
        	mSousTitrePaint.setARGB(255, 255, 255, 255);
        	mSousTitrePaint.setAntiAlias(true);
        	mSousTitrePaint.setTextSize(12*scale);
        	Typeface tf = Typeface.create("Helvetica",Typeface.NORMAL);
        	mSousTitrePaint.setTypeface(tf);
        	}
        return mSousTitrePaint;
    }
    
    public void setSelectedMarqueur(Marqueur m){
    	this.mSelectedMarqueur = m;
    	
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent e, MapView mapView) {
    		return mGestureDetector.onTouchEvent(e);
    }

   
    private void createViewFicheGeocampus(Marqueur m, MapView mapView){
    		Intent intent = new Intent(mapView.getContext(), FicheActivity.class);
    		intent.putExtra("marqueur", m.getDetails());
			mapView.getContext().startActivity(intent);
    	
    
    }
    
}
