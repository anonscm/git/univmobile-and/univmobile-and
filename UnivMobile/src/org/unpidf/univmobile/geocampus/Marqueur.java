package org.unpidf.univmobile.geocampus;


import android.graphics.Bitmap;

import com.google.android.maps.GeoPoint;

public class Marqueur {

	/**
	 * 
	 */

	private Bitmap imageMarqueur;
	private GeoPoint point;
	private int imageX;
	private int imageY;
	private MarqueurDetails details;

//	private Paint innerPaint, borderPaint, textPaint;

//    private boolean afficheInfo = false;  
	public Marqueur(Bitmap imageMarqueur, String texteAAfficher,double latitude, double longitude) {
		this.imageMarqueur = imageMarqueur;
		this.details = new MarqueurDetails(texteAAfficher, (int)(latitude*1e6), (int)(longitude*1e6));
		this.point = new GeoPoint((int)(latitude*1e6),(int)(longitude*1e6));
	}
    
	public Marqueur(Bitmap imageMarqueur, String titre, String sousTitre, String address, GeoPoint point, String univTitle) {
		this.imageMarqueur = imageMarqueur;
		this.point = point;
		this.details = new MarqueurDetails(titre, sousTitre,point, univTitle);

	}
	
	public Marqueur(Bitmap imageMarqueur, GeoPoint point, MarqueurDetails details) {
		this.imageMarqueur = imageMarqueur;
		this.point = point;
		this.details = details;

	}

	public Bitmap getImageMarqueur() {
		return imageMarqueur;
	}


	public String getTitre() {
		return details.getTitre();
	}


	public GeoPoint getPoint() {
		return point;
	}

	
	
	public String getSousTitre() {
		return details.getSousTitre();
	}


	@Override
	public String toString() {
		String result = getTitre() +" : point (" +point.getLatitudeE6()+","+point.getLongitudeE6()+")";
		// TODO Auto-generated method stub
		return result;
	}

	public int getImageX() {
		return imageX;
	}

	public void setImageX(int imageX) {
		this.imageX = imageX;
	}

	public int getImageY() {
		return imageY;
	}

	public void setImageY(int imageY) {
		this.imageY = imageY;
	}

	public MarqueurDetails getDetails() {
		return details;
	}

	public void setDetails(MarqueurDetails details) {
		this.details = details;
	}


	
	
	
	
//	@Override
//	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
//
//		super.draw(canvas, mapView, shadow);
//		
//		Point screenCoords = new Point();
//		mapView.getProjection().toPixels(point, screenCoords);
//		canvas.drawBitmap(imageMarqueur, screenCoords.x, screenCoords.y, getTextPaint());
//		
//	//	canvas.drawPoint(screenCoords.x, screenCoords.y, paint);
//		drawInfoWindow(canvas, mapView, shadow);
//		
//	}

//	@Override
//	public boolean onTap(GeoPoint p, MapView mapView) {
//		Point tapPx = new Point();
//		Point markPx = new Point();
//		//mapView.invalidate();
//		mapView.getProjection().toPixels(p, tapPx);
//		mapView.getProjection().toPixels(point, markPx);
//		
//		if ((tapPx.x > markPx.x - 30) && (tapPx.x < markPx.x + 30)
//				&& (tapPx.y > markPx.y - 30) && (tapPx.y < markPx.y + 30)) {
//			if(afficheInfo == true){
//				afficheInfo = false;
//			} else{
//				afficheInfo = true;				
//			}
//			return true;
//		}else {
//			afficheInfo =false;			
//		}
//			
//			return false;
//	}
//	
	
//	@Override
//	public boolean onTouchEvent(MotionEvent e, MapView mapView) {
//		//// TODO Auto-generated method stub
//	//	Toast.makeText(contexte, texteAAfficher, Toast.LENGTH_LONG).show();		
//		//return true;
//		return super.onTouchEvent(e, mapView);
//	}
	
//	  private void drawInfoWindow(Canvas canvas, MapView  mapView, boolean shadow) {
//		if(afficheInfo) {
//	    		if ( shadow) {
//	    			//  Skip painting a shadow
//	    		} else {
//					//  First we need to determine the screen coordinates of the selected MapLocation
//					Point selDestinationOffset = new Point();
//					mapView.getProjection().toPixels(point, selDestinationOffset);
//			    	
//			    	//  Setup the info window
//					int INFO_WINDOW_WIDTH = 175;
//					int INFO_WINDOW_HEIGHT = 40;
//					RectF infoWindowRect = new RectF(0,0,INFO_WINDOW_WIDTH,INFO_WINDOW_HEIGHT);				
//					int infoWindowOffsetX = selDestinationOffset.x-INFO_WINDOW_WIDTH/2;
//					int infoWindowOffsetY = selDestinationOffset.y-INFO_WINDOW_HEIGHT;
//					infoWindowRect.offset(infoWindowOffsetX,infoWindowOffsetY);
//
//					//  Drawing the inner info window
//					canvas.drawRoundRect(infoWindowRect, 5, 5, getInnerPaint());
//					
//					//  Drawing the border for info window
//					canvas.drawRoundRect(infoWindowRect, 5, 5, getBorderPaint());
//						
//					//  Draw the MapLocation's name
//					int TEXT_OFFSET_X = 10;
//					int TEXT_OFFSET_Y = 15;
//					canvas.drawText(texteAAfficher,infoWindowOffsetX+TEXT_OFFSET_X,infoWindowOffsetY+TEXT_OFFSET_Y,getTextPaint());
//				}
//		}
//	    	
//	    }

	


}
