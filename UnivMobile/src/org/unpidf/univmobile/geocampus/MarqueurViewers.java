package org.unpidf.univmobile.geocampus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.unpidf.univmobile.R;
import org.unpidf.univmobile.util.UnivmobileDBAdapter;
import org.unpidf.univmobile.util.WSUtils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

import com.google.android.maps.MapView;

public class MarqueurViewers extends LinearLayout {

	
	private MarqueurOverlay overlay;
	
    //  Known lat/long coordinates that we'll be using.
    private List<Marqueur> mapMarqueur;
    
    public static MapView mapView;
//	private ProgressDialog mProgressDialog;
    private static String TAG = "Univmobile - MarqueurViewers";
    
    private Context mContext;
    
    private static final int ZOOM_CATEGORY = 14;
    public static final int ZOOM_POI = 16;
    
    private UnivmobileDBAdapter mAdapter;
//    SimpleOnGestureListener mGestureListener;
//    private UnivGestureDetector mGestureDetector;
    
	public MarqueurViewers(Context context, AttributeSet attrs) {
		super(context, attrs);
		mContext = context;
		
		init();
	}

	public MarqueurViewers(Context context, List<Marqueur> pMapMarqueur) {
		super(context);
		this.mapMarqueur= pMapMarqueur;
	
		
		init();
	}
	
	public void init() {		
//		mGestureDetector = new UnivGestureDetector(); 
//		mGestureListener = new SimpleOnGestureListener();
		
		if(mAdapter == null){
			mAdapter= new UnivmobileDBAdapter(mContext);
		}
		setOrientation(VERTICAL);
		setLayoutParams(new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT,android.view.ViewGroup.LayoutParams.FILL_PARENT));
		String key = getResources().getString(R.string.google_key);
		mapView =  new MapView(getContext(),key);
		mapView.setEnabled(true);
		mapView.setClickable(true);
		addView(mapView);
		overlay = new MarqueurOverlay(this);
		mapView.getOverlays().add(overlay);
    	mapView.getController().setZoom(ZOOM_CATEGORY);
    	mapView.setBuiltInZoomControls(true);
    	if(getMapMarqueur().size()>0){
    		if(getMapMarqueur().get(0) != null){
    		mapView.getController().setCenter( getMapMarqueur().get(0).getPoint());
    		}
    	}
//    	mapView.setOnTouchListener(new  OnTouchListener() {
//			
//			public boolean onTouch(View v, MotionEvent event) {
//				Log.v(TAG, "touch");
//				
//				if(mGestureListener.onDoubleTapEvent(event)){
//					Log.v(TAG, "double tap");
//				} else {
//					Log.v(TAG, "no double tap");
//				}
//				return true;
//			}
//		});
		
	}


	public List<Marqueur> getMapMarqueur(){
		if(mapMarqueur==null){
			mapMarqueur = getListMarqueur();
		}
		return mapMarqueur;
	}

	public MapView getMapView() {
		
		return mapView;
	}
	
	public List<Marqueur> getListMarqueur(){
		
		SharedPreferences settings = getContext().getSharedPreferences(WSUtils.PREFS_NAME, 0);
		String poiSelected = settings.getString(WSUtils.KEY_POI_SELECT, WSUtils.KEY_NOT_DEFINE);
		
		 
		if(poiSelected.equals(WSUtils.KEY_NOT_DEFINE)){
			return getListMarqueurCat();
		} else {
			return getListMarqueurPoi(poiSelected);
		}
	
	}

	public List<Marqueur> getListMarqueurPoi(String id){
		List<Marqueur> mapMarquer = new ArrayList<Marqueur>();
		mAdapter.open();
		Marqueur m = mAdapter.getPoi(id, mContext);
		mapMarquer.add(m);
		mAdapter.close();
		
		if(m!=null){
			mapView.getController().setCenter( m.getPoint());
			mapView.getController().setZoom(ZOOM_POI);
			Log.v(TAG,m.toString());
			overlay.setSelectedMarqueur(m);
		}
		
		return mapMarquer;
	}
	
	
	public List<Marqueur> getListMarqueurCat() {
		Date time1 = new Date();
		
		
//		MapView mapView = (MapView) findViewById(R.id.map);
		//List<Marqueur> mapOverlays = mapView.getOverlays();
		
		SharedPreferences settings = getContext().getSharedPreferences(WSUtils.PREFS_NAME, 0);
	
		
		String catSelected = settings.getString(WSUtils.KEY_CAT_SELECT, "0");
		
		
		if("0".equals(catSelected)){
			return new ArrayList<Marqueur>();
		} 
		mAdapter.open();
		List<Marqueur> mapMarquer = mAdapter.getListPoiFromCategory(catSelected, mContext);
		mAdapter.close();
		
		Marqueur span = mapMarquer.get(mapMarquer.size()-2);
		mapView.getController().zoomToSpan(span.getPoint().getLongitudeE6(),span.getPoint().getLatitudeE6());
		Marqueur position = mapMarquer.get(mapMarquer.size()-1);
		mapView.getController().setCenter( position.getPoint());
		
		if(mapView.getZoomLevel()>ZOOM_POI){
			mapView.getController().setZoom(ZOOM_POI);
		}
		
		//mapView.getController().setZoom(ZOOM_CATEGORY);
		
		Log.v("","Durée du traitement des "+mapMarquer.size()+" pois : " + (new Date().getTime() - time1.getTime()) + "ms");
		overlay.setSelectedMarqueur(null);
		
		return mapMarquer;

	}
	
	public void refreshMapMarqueurs(){
		mapMarqueur = getListMarqueur() ;
		
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		setOrientation(VERTICAL);
		setLayoutParams(new LinearLayout.LayoutParams(android.view.ViewGroup.LayoutParams.FILL_PARENT,android.view.ViewGroup.LayoutParams.FILL_PARENT));
		String key = getResources().getString(R.string.google_key);
		mapView =  new MapView(getContext(),key);
		mapView.setEnabled(true);
		mapView.setClickable(true);
		addView(mapView);
		overlay = new MarqueurOverlay(this);
		mapView.getOverlays().add(overlay);
    	mapView.getController().setZoom(14);
    	if(getMapMarqueur().size()>0){
    		mapView.getController().setCenter( getMapMarqueur().get(0).getPoint());
    	}
    
    	
	}
	
	
	
	
}
