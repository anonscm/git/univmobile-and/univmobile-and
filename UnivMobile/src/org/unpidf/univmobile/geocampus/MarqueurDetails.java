package org.unpidf.univmobile.geocampus;


import java.io.Serializable;

import com.google.android.maps.GeoPoint;

public class MarqueurDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String titre;
	private String sousTitre;
	private String univTitle;
	private String emplacement;
	private String address;
	private String address2;
	private String phone;
	private String fax;
	private String email;
	private String url;
	private String openHours;
	private String itinerary;
//	private Paint innerPaint, borderPaint, textPaint;

	public MarqueurDetails(){}
	
	
//    private boolean afficheInfo = false;  
	public MarqueurDetails(String texteAAfficher,double latitude, double longitude) {
		this.titre = texteAAfficher;
		
	}
    
	public MarqueurDetails(String titre, String sousTitre, GeoPoint point, String universiteTitle) {
		this.titre = titre;
		this.sousTitre = sousTitre;
		this.univTitle = universiteTitle;

	}



	public String getTitre() {
		return titre;
	}

	
	public String getUnivTitle() {
		return univTitle;
	}

	public void setUnivTitle(String univTitle) {
		this.univTitle = univTitle;
	}

	public String getSousTitre() {
		return sousTitre;
	}


	public String getEmplacement() {
		return emplacement;
	}


	public void setEmplacement(String emplacement) {
		this.emplacement = emplacement;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}


	public void setSousTitre(String sousTitre) {
		this.sousTitre = sousTitre;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getOpenHours() {
		return openHours;
	}


	public void setOpenHours(String openHours) {
		this.openHours = openHours;
	}


	public String getItinerary() {
		return itinerary;
	}


	public void setItinerary(String itinerary) {
		this.itinerary = itinerary;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getFax() {
		return fax;
	}


	public void setFax(String fax) {
		this.fax = fax;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	

	


}
