package org.unpidf.univmobile.geocampus;

import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;

import com.google.android.maps.MapView;


public class UnivGestureDetector extends SimpleOnGestureListener {
	private MapView mapView = null;

	/**
	 * Constructor.
	 * @param mapView reference to the map view.
	 */
	public UnivGestureDetector(MapView mapView) {
		this.mapView = mapView;
	}
	/**
	 * {@inheritDoc}
	 */
	public boolean onDoubleTap(MotionEvent e) {
		// Zoom in.
		mapView.getController().zoomIn();

		return true;
	}

}
