package org.unpidf.univmobile;

import org.unpidf.univmobile.util.WSUtils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class UnivMobileWebView extends Activity {

	private WebView mWebView;
//	private ImageButton mButtonRetourUniv;
	//private ProgressDialog mProgressDialog;
	//private Context mContext;
	private static final String TAG = "Univmobile - UnivMobileWebView";
	private String urlStart = "";
	private String titre;
	// Instance of WebChromeClient for handling all chrome functions.
	//private volatile WebChromeClient mWebChromeClient;

	
    final Activity activity = this;
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
      
		Bundle bundle = this.getIntent().getExtras();
		String url = bundle.getString(WSUtils.KEY_URL);
		titre = bundle.getString(WSUtils.KEY_TITLE);
		urlStart = url;
    	super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.webview);
        mWebView = (WebView) findViewById(R.id.mWebView);
        
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress)
            {
                activity.setTitle("Chargement...");
                activity.setProgress(progress * 100);
 
                if(progress == 100)
                	
                    activity.setTitle(titre);
            }
        });
 
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl)
            {
                // Handle the error
            	Log.v(TAG,"description : " + description);
            }
//            @Override
//            public void onReceivedSslError(WebView view,
//            		SslErrorHandler handler, SslError error) {
//            	
//            	if(handler!=null){
//            		handler.proceed();
//            	}
//
//            }
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url)
            {
                view.loadUrl(url);
                return true;
            }
        });
 
        mWebView.loadUrl(url);
        
    }
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.webviewmenu, menu);
	    return true;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
		 
		switch (item.getItemId()) {
	    case R.id.mItemBack:
	    	if(mWebView.canGoBack()){
	    		mWebView.goBack();
	    	} else {
	    		finish();	
	    	}
	        return true;
	    case R.id.mItemHome:
	    	finish();	
	    	return true;
	    case R.id.mItemInfo:
	    	
	    	
	    	Bundle bundle = this.getIntent().getExtras();
			String url = bundle.getString(WSUtils.KEY_HELP);
			mWebView.loadUrl(url);
	    	return true;
	    case R.id.mItemChrome:
	  
	    	Intent i = new Intent(Intent.ACTION_VIEW);
	    	
	    	String urltmp = urlStart;
	    	if (urlStart!=null && urlStart.contains("iphone=true")){
	    		urltmp = urltmp.replace("iphone=true", "iphone=false");
	    	}
	    	Log.v(TAG,"url de redirection : "+urltmp);
	    	Uri u = Uri.parse(urltmp);
	    	i.setData(u);
			startActivity(i);
	    	return true;
	    	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	
//	
//	
//
//	/**
//	 * Set the WebChromeClient.
//	 * @param client An implementation of WebChromeClient.
//	 */
//	public void setWebChromeClient(WebChromeClient client) {
//	    mWebChromeClient = client;
//	}
    
	

	
}
}
