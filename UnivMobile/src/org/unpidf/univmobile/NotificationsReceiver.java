package org.unpidf.univmobile;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.unpidf.univmobile.activity.MainActivity;
import org.unpidf.univmobile.util.WSUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.c2dm.C2DMBroadcastReceiver;

public class NotificationsReceiver extends C2DMBroadcastReceiver {
	
	private String m_registrationId;
	private Context m_context;
	@Override
	protected void onError(Context context, String error) {
		Log.v("NotificationsReceiver",error);
		// traitement de l'erreur
	}

	@Override
	protected void onRegistration(Context context, String registrationId) {
		
		m_registrationId = registrationId;
		m_context = context;
		SharedPreferences settings =  context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(WSUtils.KEY_REGISTRATION_ID, registrationId);
		editor.commit();
		
		Thread registerThread = new Thread() {
			@Override
			public void run() {
				// String registration =
				// intent.getStringExtra("registration_id");
				TelephonyManager tm = (TelephonyManager) m_context
						.getSystemService(Context.TELEPHONY_SERVICE);
				String udid = tm.getDeviceId();
				SharedPreferences settings =  m_context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
				String key_univ = settings.getString(WSUtils.KEY_UNIV, "");
				
				if(!"".equals(key_univ)){
					
					String uri = m_context.getText(R.string.url_push)
							+ "?udid="+ udid 
							+ "&token=" + m_registrationId 
							+ "&key=" +m_context.getText(R.string.push_key) 
							+ "&univ_id=" + key_univ;
					
					
					
					Log.v("NotificationsReceiver",
							"envoi de la requete pour service push :" + m_context.getText(R.string.url_push));
					
					Log.v("NotificationsReceiver",
							"udid :" + udid);
					Log.v("NotificationsReceiver",
							"token :" + m_registrationId);
				
					Log.v("NotificationsReceiver",
							"univ_id :" + key_univ);
				
					
					
					
					
					// FIX : crash android 3.0 et +
					//int SDK_INT = android.os.Build.VERSION.SDK_INT;
					if(Build.VERSION.SDK_INT >10) {
						StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
						StrictMode.setThreadPolicy(policy);						
					}
					

					HttpGet httpGet = new HttpGet(uri);
					HttpClient httpclient = new DefaultHttpClient();
					try {
						httpclient.execute(httpGet);
						Log.v("onReceive", "registrationId :" + m_registrationId
								+ " udid " + udid
								+ "  transmise "); 
					} catch (ClientProtocolException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block

						e.printStackTrace();

					}
				}
				
				// L'enregistrement a correctement été effectué.
				// Transmettez ici l'identifiant d'enregistrement au serveur Web
				// qui transmettra les messages.
				
				
			}
		};
		registerThread.run();
         	
	}

	@Override
	protected void onUnregistration(Context context) {
		// traitement du désabonnement
	}

	@Override
	protected void onMessageReceived(Context context, Intent intent) {
		String message = intent.getStringExtra("alert"); 
		String callback_url = intent.getStringExtra("u");
		String callback_title = intent.getStringExtra("t");
		
		
		
		//title dynamique
		String title = "UnivMobile :";
		int iconId = R.drawable.icon;

		// création de la notification :
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(iconId, "Notification UnivMobile", System.currentTimeMillis()); 
		
		
		SharedPreferences settings = context.getSharedPreferences(WSUtils.PREFS_NAME, 0);
		
		if(settings.getBoolean(WSUtils.PUSH_PREF, true)){
			// active les vibrations
			notification.vibrate = new long[] {0,200,100,200,100,0};
			// active les sonneries
			notification.defaults |= Notification.DEFAULT_SOUND;
		}
		//active la notifications par LED
		notification.defaults |= Notification.DEFAULT_LIGHTS;
		
		Bundle bundle = new Bundle(); 
		//bundle.putString(WSUtils.KEY_URL, lien);
		bundle.putString(WSUtils.KEY_PUSH, message);
		bundle.putString(WSUtils.KEY_PUSH_URL, callback_url);
		bundle.putString(WSUtils.KEY_PUSH_TITLE, callback_title);
		
		
		
		
		// création de l'activité à démarrer lors du clic :
		Intent notifIntent = new Intent(context.getApplicationContext(), MainActivity.class);
		notifIntent.putExtras(bundle);
		
		
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notifIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		// affichage de la notification dans le menu déroulant :
		notification.setLatestEventInfo(context, title, message, contentIntent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL; // la notification disparaitra une fois cliquée

		// lancement de la notification :
		notificationManager.notify(1, notification);
	}
	
		
	
}
