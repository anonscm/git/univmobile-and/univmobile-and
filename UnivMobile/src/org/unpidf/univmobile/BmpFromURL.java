package org.unpidf.univmobile;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class BmpFromURL {

	private Bitmap myBitmap;

	public BmpFromURL(String imageURL) {
		URL myImageURL = null;

		try {

			myImageURL = new URL(imageURL);

		} catch (MalformedURLException error) {
			Log.v("BmpFromURL", error.getMessage());
			error.printStackTrace();

		}

		try {

			HttpURLConnection connection = (HttpURLConnection) myImageURL
					.openConnection();

			connection.setDoInput(true);

			connection.connect();

			InputStream input = connection.getInputStream();

			myBitmap = BitmapFactory.decodeStream(input);

		} catch (IOException e) {

			e.printStackTrace();

		}

	}
	
	public Bitmap getMyBitmap() {

		return myBitmap;

		}

}
